# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: play-through of "rauc install" and "rauc status mark-bad"
# This test doesn't run in LXD as RAUC cannot  mount a squashfs due to the
# sandbox blocking that. It would only work if RAUC would be able to detect
# and handle that situation and switch to FUSE. This test works fine in qemu.
backends: [-lxd]
details: |
    Using a custom boot backend, not related to system ota, record what "rauc
    install", followed by "rauc mark-bad" is doing on the system.
environment:
    AFTER_REBOOT_SLOT/kernel_not_booted: A
    AFTER_REBOOT_SLOT/kernel_booted: B
prepare: |
    mkdir bundle-dir
    mkdir system-dir

    # Create a fake system image
    mkdir system-dir/etc
    printf "ID=asos\nID_VERSION=1\nID_VARIANT=asos-fake\n" > system-dir/etc/os-release

    mksquashfs system-dir bundle-dir/system.img -comp zstd

    # Create a fake rauc bundle with that system image
    cat <<RAUC_MANIFEST >bundle-dir/manifest.raucm
    [update]
    compatible=SystemOTA Test Environment
    version=1

    [image.system]
    filename=system.img
    RAUC_MANIFEST

    # Modify the fake rauc configuration file to set a custom bootloader handler.
    cat <<RAUC_SYSTEM_CONF >>/etc/rauc/system.conf
    [handlers]
    bootloader-custom-backend=$(pwd)/backend.sh
    RAUC_SYSTEM_CONF

    rauc bundle --cert="$TEST_RAUC_KEYS_DIR/cert.pem" --key="$TEST_RAUC_KEYS_DIR/key.pem" bundle-dir bundle.img

    # Store /proc/cmdline and allow us to fake /proc/cmdline easily in the execute phase below.
    cp /proc/cmdline cmdline.orig
    cp cmdline.orig cmdline
    mount --bind cmdline /proc/cmdline

    # Stop rauc and grab a cursor for the journal log
    systemctl stop rauc.service || true
    journalctl -u sysotad.service --cursor-file=cursor >/dev/null || true

    # Truncate the backend log and reset backend state
    truncate --size=0 /var/backend.log
    echo 0 >/tmp/backend.state

execute: |
    printf "%s rauc.slot=A\n" "$(cat cmdline.orig)" > cmdline
    rauc install bundle.img

    # The backend was called as we expect.
    diff -u /tmp/backend.log expected-install.log

    # Truncate the log for another command.
    truncate --size=0 /tmp/backend.log

    # The slot contains the image we prescribed.
    cmp "$TEST_RAUC_FAKE_SYSTEM_DIR/slot-b" bundle-dir/system.img

    # On a real system, we would reboot here.

    # Here we stop rauc.service to discard any state, rig /proc/cmdline to
    # pretend we've booted the B slot and use mark-bad to indicate that the
    # update has not worked fine.
    systemctl stop rauc.service
    # Note that the slot we pretend to be in now is a test variant, so we check
    # both cases. One case simulates that the new (updated) kernel and userspace
    # boot fine but decide to revert. The other case simulates that the new
    # kernel does not boot and we reboot back to the previous slot.
    printf "%s rauc.slot=%s\n" "$(cat cmdline.orig)" "$AFTER_REBOOT_SLOT" > cmdline
    case "$AFTER_REBOOT_SLOT" in
        A)
            # If we've booted into slot A then we implicitly mark the other slot
            # (B) as bad. Here we simulate a broken kernel that never lets the
            # new userspace to run.
            rauc status mark-bad other
            ;;
        B)
            # If we've booted into slot B we actively chose to mark it as bad.
            # Here we simulate some sort of incompatibility where the new image
            # boots but decides, perhaps by asking some other parts of the system,
            # to roll-back.
            rauc status mark-bad
            ;;
    esac

    # The backend was called as we expected.
    diff -u /tmp/backend.log expected-status-mark-bad.log

restore: |
    umount /proc/cmdline || true

    rm -rf system-dir bundle-dir
    rm -f *.img
    rm -f *.pem
    rm -f /tmp/backend.{log,state}
    rm -f cursor
    rm -f cmdline{,.orig}

debug: |
    cat /proc/cmdline
    test -f /tmp/backend.log && cat /tmp/backend.log
    test -f /tmp/backend.state && cat /tmp/backend.state
    journalctl --cursor-file=cursor -u rauc.service
