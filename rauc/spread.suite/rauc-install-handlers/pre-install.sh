#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
set -e

# Log command line arguments.
echo "pre-install: $*" >> /tmp/pre-install.log

# Log all the RAUC environment variables.
env | grep ^RAUC | sort >> /tmp/pre-install.log

# Look at what's in RAUC_BUNDLE_MOUNT_POINT.
ls -l "$RAUC_BUNDLE_MOUNT_POINT" >> /tmp/pre-install.log
