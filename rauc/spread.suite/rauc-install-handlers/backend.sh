#!/bin/sh -e
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

# Log the request to a fixed file as well as to stderr (for debugging)
echo "custom: $*" >> /tmp/backend.log
echo "custom: $*" >&2

# Using /tmp/backend.state as the state variable, implement a state machine
# which handles the expected request and gives a canned response.
#
# Everything written to stdout is a part of the custom boot backend:
# https://rauc.readthedocs.io/en/latest/integration.html#custom-bootloader-backend-interface
state="$(cat /tmp/backend.state)"
case "${state}" in
    # This is what happens after: rauc install bundle.img
    0)
        test "$*" = "set-state B bad" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo 1 >/tmp/backend.state
        ;;
    1)
        test "$*" = "set-primary B" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo 2 >/tmp/backend.state
        ;;
    *)
        echo "unexpected state: $state" >&2
        exit 1
        ;;
esac
