// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package installdbus_test

import (
	"encoding/xml"
	"errors"
	"os"
	"testing"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/cmd/sysotad/raucinstallhosted"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installdbus"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installtest"
)

func Test(t *testing.T) { TestingT(t) }

var errBoom = errors.New("boom")

type clientSuite struct {
	dbustest.Suite
	conn        *dbus.Conn
	serviceHost *dbusutil.ServiceHost
	handler     installtest.Handler
}

var _ = Suite(&clientSuite{})

func (s *clientSuite) SetUpTest(_ *C) {
	s.handler.ResetCallbacks()
}

func (s *clientSuite) EnsureConn(c *C) *dbus.Conn {
	if s.conn == nil {
		var err error
		s.conn, err = dbusutil.SystemBus()
		c.Assert(err, IsNil)
	}

	return s.conn
}

func (s *clientSuite) EnsureService(c *C) {
	if s.serviceHost == nil {
		conn := s.EnsureConn(c)

		s.serviceHost = dbusutil.NewServiceHost(conn)
		s.serviceHost.AddHostedService(raucinstallhosted.New(&s.handler))

		err := s.serviceHost.Export()
		c.Assert(err, IsNil)

		reply, err := conn.RequestName(svcspec.BusName, dbus.NameFlagDoNotQueue)
		c.Assert(err, IsNil)
		c.Assert(reply, Equals, dbus.RequestNameReplyPrimaryOwner)
	}
}

func (s *clientSuite) TearDownTest(c *C) {
	if s.serviceHost != nil {
		err := s.serviceHost.Unexport()
		c.Check(err, IsNil)

		s.serviceHost = nil
	}

	if s.conn != nil {
		err := s.conn.Close()
		c.Check(err, IsNil)

		s.conn = nil
	}
}

func (s *clientSuite) TestServiceMissing(c *C) {
	client := installdbus.NewClient(s.EnsureConn(c))

	err := client.PreInstall(&installhandler.Environment{})
	c.Assert(err, ErrorMatches, `The name org.oniroproject.sysota1 was not provided by any .service files`)
}

func (s *clientSuite) TestPreInstallSuccess(c *C) {
	// Both client and service are happy.
	s.EnsureService(c)
	client := installdbus.NewClient(s.EnsureConn(c))
	env := &installhandler.Environment{
		SystemConfig:      "foo",
		InitialHandlerPID: os.Getpid(), // we self-send the D-Bus message.
	}
	called := false

	s.handler.MockPreInstallFn(func(env2 *installhandler.Environment) error {
		c.Check(env, DeepEquals, env2)

		called = true

		return nil
	})

	err := client.PreInstall(env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *clientSuite) TestPreInstallBadInput(c *C) {
	// Client provides faulty data, service rejects it.
	s.EnsureService(c)
	client := installdbus.NewClient(s.EnsureConn(c))
	err := client.PreInstall(&installhandler.Environment{SlotIDs: []string{"potato"}})
	c.Assert(err, ErrorMatches, `cannot create handler environment: variable RAUC_SLOTS refers to undefined slot potato`)
}

func (s *clientSuite) TestPreInstallServiceFails(c *C) {
	// Service fails, client reports the error.
	s.EnsureService(c)
	client := installdbus.NewClient(s.EnsureConn(c))

	s.handler.MockPreInstallFn(func(env *installhandler.Environment) error {
		return errBoom
	})

	err := client.PreInstall(&installhandler.Environment{})
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *clientSuite) TestPostInstallSuccess(c *C) {
	// Both client and service are happy.
	s.EnsureService(c)
	client := installdbus.NewClient(s.EnsureConn(c))
	env := &installhandler.Environment{
		SystemConfig:      "foo",
		InitialHandlerPID: os.Getpid(), // we self-send the D-Bus message.
	}
	called := false

	s.handler.MockPostInstallFn(func(env2 *installhandler.Environment) error {
		c.Check(env, DeepEquals, env2)

		called = true

		return nil
	})

	err := client.PostInstall(env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *clientSuite) TestPostInstallBadInput(c *C) {
	// Client provides faulty data, service rejects it.
	s.EnsureService(c)
	client := installdbus.NewClient(s.EnsureConn(c))
	err := client.PostInstall(&installhandler.Environment{SlotIDs: []string{"potato"}})
	c.Assert(err, ErrorMatches, `cannot create handler environment: variable RAUC_SLOTS refers to undefined slot potato`)
}

func (s *clientSuite) TestPostInstallServiceFails(c *C) {
	// Service fails, client reports the error.
	s.EnsureService(c)
	client := installdbus.NewClient(s.EnsureConn(c))

	s.handler.MockPostInstallFn(func(env *installhandler.Environment) error {
		return errBoom
	})

	err := client.PostInstall(&installhandler.Environment{})
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *clientSuite) TestIntrospection(c *C) {
	s.EnsureService(c)

	// Introspection data is published and looks correct.
	obj := s.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	node, err := introspect.Call(obj)
	c.Assert(err, IsNil)
	c.Check(node, DeepEquals, &introspect.Node{
		XMLName: xml.Name{Local: "node"},
		Name:    "/org/oniroproject/sysota1/Service",
		Interfaces: []introspect.Interface{{
			Name: "org.oniroproject.sysota1.RAUC.InstallHandler",
			Methods: []introspect.Method{{
				Name: "PreInstall",
				Args: []introspect.Arg{
					{Name: "environ", Type: "as", Direction: "in"},
				},
			}, {
				Name: "PostInstall",
				Args: []introspect.Arg{
					{Name: "environ", Type: "as", Direction: "in"},
				},
			}},
		}, {
			Name: "org.freedesktop.DBus.Introspectable",
			Methods: []introspect.Method{{
				Name: "Introspect",
				Args: []introspect.Arg{
					{Name: "out", Type: "s", Direction: "out"}},
			}},
		}},
	})
}
