// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package spec defines the shared specification between D-Bus client and
// service implementing RAUC custom boot handler and install handler.
package spec

const (
	// InterfaceName is the name of the RAUC interface provided by SystemOTA.
	InterfaceName = "org.oniroproject.sysota1.RAUC.InstallHandler"

	// Methods of the install handler

	// PreInstall is the name of the RAUC handler invoked before installing a bundle.
	PreInstall = "PreInstall"
	// PostInstall is the name of the RAUC handler invoked after installing a bundle.
	PostInstall = "PostInstall"
)
