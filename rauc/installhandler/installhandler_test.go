// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package installhandler_test

import (
	"errors"
	"testing"

	"gitlab.com/zygoon/sysota/rauc/installhandler"
	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type installHandlerSuite struct{}

var _ = Suite(&installHandlerSuite{})

var sampleEnv = []string{
	"STUFF_NOT_RELATED_TO_RAUC=potato",
	"MALFORMED_ENTRY",
	// Data obtained from tests/rauc/spread.suite/rauc-install-handlers/task.yaml.
	"RAUC_BUNDLE_MOUNT_POINT=/run/rauc/bundle",
	"RAUC_CURRENT_BOOTNAME=A",
	"RAUC_IMAGE_CLASS_1=system",
	"RAUC_IMAGE_DIGEST_1=24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
	"RAUC_IMAGE_NAME_1=system.img",
	"RAUC_MOUNT_PREFIX=/run/rauc",
	"RAUC_SLOT_BOOTNAME_1=B",
	"RAUC_SLOT_BOOTNAME_2=A",
	"RAUC_SLOT_CLASS_1=system",
	"RAUC_SLOT_CLASS_2=system",
	"RAUC_SLOT_DEVICE_1=/tmp/rauc-fake-system/slot-b",
	"RAUC_SLOT_DEVICE_2=/tmp/rauc-fake-system/slot-a",
	"RAUC_SLOT_NAME_1=system.1",
	"RAUC_SLOT_NAME_2=system.0",
	"RAUC_SLOT_PARENT_1=",
	"RAUC_SLOT_PARENT_2=",
	"RAUC_SLOTS=1 2 ",
	"RAUC_SLOT_TYPE_1=raw",
	"RAUC_SLOT_TYPE_2=raw",
	"RAUC_SYSTEM_CONFIG=/etc/rauc/system.conf",
	"RAUC_TARGET_SLOTS=1 ",
	"RAUC_UPDATE_SOURCE=/run/rauc/bundle",
}

func (s *installHandlerSuite) TestNewInstallEnvironment(c *C) {
	env, err := installhandler.NewEnvironment(sampleEnv)
	c.Assert(err, IsNil)
	c.Check(env, DeepEquals, &installhandler.Environment{
		MountPrefix:      "/run/rauc",
		BundleMountPoint: "/run/rauc/bundle",
		CurrentBootName:  "A",
		UpdateSource:     "/run/rauc/bundle",
		SystemConfig:     "/etc/rauc/system.conf",
		TargetSlotIDs:    []string{"1"},
		SlotIDs:          []string{"1", "2"},
		Slots: map[string]*installhandler.Slot{
			"1": {
				ID:         "1",
				Name:       "system.1",
				Class:      "system",
				Type:       "raw",
				BootName:   "B",
				DevicePath: "/tmp/rauc-fake-system/slot-b",
			},
			"2": {
				ID:         "2",
				Name:       "system.0",
				Class:      "system",
				Type:       "raw",
				BootName:   "A",
				DevicePath: "/tmp/rauc-fake-system/slot-a",
			},
		},
		ImageIDs: []string{"1"},
		Images: map[string]*installhandler.Image{
			"1": {
				ID:     "1",
				Name:   "system.img",
				Class:  "system",
				Digest: "24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
			},
		},
	})
}

func (s *installHandlerSuite) TestNewInstallHandlerEnvironmentWithEmptyBlock(c *C) {
	env, err := installhandler.NewEnvironment(nil)
	c.Assert(err, IsNil)
	c.Check(env.SlotIDs, HasLen, 0)
	c.Check(env.TargetSlotIDs, HasLen, 0)
	c.Check(env.ImageIDs, HasLen, 0)
}

func (s *installHandlerSuite) TestEnviron(c *C) {
	env1, err := installhandler.NewEnvironment(sampleEnv)
	c.Assert(err, IsNil)
	env2, err := installhandler.NewEnvironment(env1.Environ())
	c.Assert(err, IsNil)

	c.Check(env1, DeepEquals, env2)
}

func (s *installHandlerSuite) TestEmptyEnviron(c *C) {
	env1, err := installhandler.NewEnvironment(nil)
	c.Assert(err, IsNil)

	environ := env1.Environ()
	c.Check(environ, DeepEquals, []string{
		"RAUC_BUNDLE_MOUNT_POINT=",
		"RAUC_CURRENT_BOOTNAME=",
		"RAUC_MOUNT_PREFIX=",
		"RAUC_SLOTS=",
		"RAUC_SYSTEM_CONFIG=",
		"RAUC_TARGET_SLOTS=",
		"RAUC_UPDATE_SOURCE=",
	})

	env2, err := installhandler.NewEnvironment(environ)
	c.Assert(err, IsNil)

	c.Check(env1, DeepEquals, env2)
}

func (s *installHandlerSuite) TestNewEnvironmentErrors(c *C) {
	// Empty environment is valid but produces entirely empty environment.
	env, err := installhandler.NewEnvironment(nil)
	c.Assert(err, IsNil)
	c.Check(env, DeepEquals, &installhandler.Environment{})

	// All variables in the RAUC_ namespace have to be recognized.
	_, err = installhandler.NewEnvironment([]string{"RAUC_POTATO=1"})
	c.Assert(err, ErrorMatches, `cannot create handler environment: unknown environment variable RAUC_POTATO=1`)
	c.Check(errors.Unwrap(err), ErrorMatches, `unknown environment variable .*`)

	// All slots referenced in RAUC_SLOTS must be defined.
	_, err = installhandler.NewEnvironment([]string{"RAUC_SLOTS=1"})
	c.Assert(err, ErrorMatches, `cannot create handler environment: variable RAUC_SLOTS refers to undefined slot 1`)
	c.Check(errors.Unwrap(err), ErrorMatches, `variable .* refers to .*`)

	// All slots referenced in RAUC_TARGET_SLOTS must be defined.
	_, err = installhandler.NewEnvironment([]string{"RAUC_TARGET_SLOTS=1"})
	c.Assert(err, ErrorMatches, `cannot create handler environment: variable RAUC_TARGET_SLOTS refers to undefined slot 1`)

	// All slots referenced in RAUC_SLOT_PARENT_$id must be defined.
	_, err = installhandler.NewEnvironment([]string{"RAUC_SLOT_PARENT_1=2"})
	c.Assert(err, ErrorMatches, `cannot create handler environment: variable RAUC_SLOT_PARENT_1 refers to undefined slot 2`)
}

func (s *installHandlerSuite) TestImageWithClass(c *C) {
	env, err := installhandler.NewEnvironment([]string{
		"RAUC_IMAGE_CLASS_1=system",
		"RAUC_IMAGE_DIGEST_1=24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
		"RAUC_IMAGE_NAME_1=system.img",
	})
	c.Assert(err, IsNil)

	c.Check(env.ImageWithClass("system"), DeepEquals, &installhandler.Image{
		ID:     "1",
		Name:   "system.img",
		Class:  "system",
		Digest: "24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
	})

	c.Check(env.ImageWithClass("potato"), IsNil)
}

func (s *installHandlerSuite) TestTargetSlotWithClass(c *C) {
	env, err := installhandler.NewEnvironment([]string{
		"RAUC_SLOT_BOOTNAME_1=B",
		"RAUC_SLOT_BOOTNAME_2=A",
		"RAUC_SLOT_CLASS_1=system",
		"RAUC_SLOT_CLASS_2=system",
		"RAUC_SLOT_DEVICE_1=/tmp/rauc-fake-system/slot-b",
		"RAUC_SLOT_DEVICE_2=/tmp/rauc-fake-system/slot-a",
		"RAUC_SLOT_NAME_1=system.1",
		"RAUC_SLOT_NAME_2=system.0",
		"RAUC_SLOT_PARENT_1=",
		"RAUC_SLOT_PARENT_2=",
		"RAUC_SLOTS=1 2 ",
		"RAUC_SLOT_TYPE_1=raw",
		"RAUC_SLOT_TYPE_2=raw",
		"RAUC_TARGET_SLOTS=1 ",
	})
	c.Assert(err, IsNil)

	c.Check(env.TargetSlotWithClass("system"), DeepEquals, &installhandler.Slot{
		ID:         "1",
		Name:       "system.1",
		Class:      "system",
		Type:       "raw",
		BootName:   "B",
		DevicePath: "/tmp/rauc-fake-system/slot-b",
	})

	c.Check(env.TargetSlotWithClass("potato"), IsNil)

	// We are really looking at RAUC_TARGET_SLOTS.
	env, err = installhandler.NewEnvironment([]string{
		"RAUC_SLOT_BOOTNAME_1=B",
		"RAUC_SLOT_BOOTNAME_2=A",
		"RAUC_SLOT_CLASS_1=system",
		"RAUC_SLOT_CLASS_2=system",
		"RAUC_SLOT_DEVICE_1=/tmp/rauc-fake-system/slot-b",
		"RAUC_SLOT_DEVICE_2=/tmp/rauc-fake-system/slot-a",
		"RAUC_SLOT_NAME_1=system.1",
		"RAUC_SLOT_NAME_2=system.0",
		"RAUC_SLOT_PARENT_1=",
		"RAUC_SLOT_PARENT_2=",
		"RAUC_SLOTS=1 2 ",
		"RAUC_SLOT_TYPE_1=raw",
		"RAUC_SLOT_TYPE_2=raw",
		// NOTE: this is now empty.
		"RAUC_TARGET_SLOTS=",
	})
	c.Assert(err, IsNil)

	c.Check(env.TargetSlotWithClass("system"), IsNil)
}
