// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package rauc provides APIs for working with RAUC.
//
// Specific functionality is present in sub-packages like boothandler and installhandler.
package rauc

const (
	// BusName is the D-Bus bus name claimed by the RAUC service.
	BusName = "de.pengutronix.rauc"
	// Interface is the D-Bus interface name of the RAUC service.
	Interface = "de.pengutronix.rauc.Installer"
	// ObjectPath is the D-Bus object path of the RAUC service's installer object.
	ObjectPath = "/"
	// InstallBundleMethod is the name of the D-Bus InstallBundle method.
	InstallBundleMethod = "InstallBundle"
	// LastErrorProperty is the name of the D-Bus LastError property.
	LastErrorProperty = "LastError"
	// ProgressProperty is the name of the D-Bus Progress property.
	ProgressProperty = "Progress"
	// CompletedSignal is the name of the D-Bus Completed signal
	CompletedSignal = "Completed"
)
