// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package bootdbus provides D-Bus client to RAUC custom boot handler.
package bootdbus

import (
	"github.com/godbus/dbus/v5"

	"gitlab.com/zygoon/sysota/boot"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/rauc/boothandler/bootdbus/spec"
)

// Client uses the RAUC interface offered by SystemOTA over D-Bus.
type Client struct {
	conn *dbus.Conn
}

// NewClient returns new Client instance using given bus connection.
func NewClient(conn *dbus.Conn) *Client {
	return &Client{conn: conn}
}

// PrimarySlot calls the org.oniroproject.sysota1.RAUC.GetPrimary D-Bus method.
func (client *Client) PrimarySlot() (slot boot.Slot, err error) {
	var slotName string

	serviceObj := client.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	if err := serviceObj.Call(spec.InterfaceName+"."+spec.GetPrimary, 0).Store(&slotName); err != nil {
		return boot.InvalidSlot, err
	}

	err = slot.UnmarshalText([]byte(slotName))

	return slot, err
}

// SetPrimarySlot calls the org.oniroproject.sysota1.RAUC.SetPrimary D-Bus method.
func (client *Client) SetPrimarySlot(slot boot.Slot) error {
	serviceObj := client.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	return serviceObj.Call(spec.InterfaceName+"."+spec.SetPrimary, 0, slot.String()).Store()
}

// SlotState calls the org.oniroproject.sysota1.RAUC.GetState D-Bus method.
func (client *Client) SlotState(slot boot.Slot) (state boot.SlotState, err error) {
	var stateName string

	serviceObj := client.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	if err := serviceObj.Call(spec.InterfaceName+"."+spec.GetState, 0, slot.String()).Store(&stateName); err != nil {
		return boot.InvalidSlotState, err
	}

	err = state.UnmarshalText([]byte(stateName))

	return state, err
}

// SetSlotState calls the org.oniroproject.sysota1.RAUC.SetState D-Bus method.
func (client *Client) SetSlotState(slot boot.Slot, state boot.SlotState) error {
	serviceObj := client.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	return serviceObj.Call(spec.InterfaceName+"."+spec.SetState, 0, slot.String(), state.String()).Store()
}
