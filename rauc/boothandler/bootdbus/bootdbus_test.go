// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package bootdbus_test

import (
	"encoding/xml"
	"errors"
	"testing"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/cmd/sysotad/raucboothosted"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
	"gitlab.com/zygoon/sysota/rauc/boothandler/bootdbus"
	"gitlab.com/zygoon/sysota/rauc/boothandler/boottest"
)

func Test(t *testing.T) { TestingT(t) }

var errBoom = errors.New("boom")

type clientSuite struct {
	dbustest.Suite
	conn        *dbus.Conn
	serviceHost *dbusutil.ServiceHost
	handler     boottest.Handler
}

var _ = Suite(&clientSuite{})

func (s *clientSuite) SetUpTest(c *C) {
	s.handler.ResetCallbacks()
}

func (s *clientSuite) EnsureConn(c *C) *dbus.Conn {
	if s.conn == nil {
		var err error
		s.conn, err = dbusutil.SystemBus()
		c.Assert(err, IsNil)
	}

	return s.conn
}

func (s *clientSuite) EnsureService(c *C) {
	if s.serviceHost == nil {
		conn := s.EnsureConn(c)

		s.serviceHost = dbusutil.NewServiceHost(conn)
		s.serviceHost.AddHostedService(raucboothosted.New(&s.handler))

		err := s.serviceHost.Export()
		c.Assert(err, IsNil)

		reply, err := conn.RequestName(svcspec.BusName, dbus.NameFlagDoNotQueue)
		c.Assert(err, IsNil)
		c.Assert(reply, Equals, dbus.RequestNameReplyPrimaryOwner)
	}
}

func (s *clientSuite) TearDownTest(c *C) {
	if s.serviceHost != nil {
		err := s.serviceHost.Unexport()
		c.Check(err, IsNil)

		s.serviceHost = nil
	}

	if s.conn != nil {
		err := s.conn.Close()
		c.Check(err, IsNil)

		s.conn = nil
	}
}

func (s *clientSuite) TestServiceMissing(c *C) {
	client := bootdbus.NewClient(s.EnsureConn(c))

	_, err := client.PrimarySlot()
	c.Assert(err, ErrorMatches, `The name org.oniroproject.sysota1 was not provided by any .service files`)
}

func (s *clientSuite) TestPrimarySlot(c *C) {
	s.EnsureService(c)
	client := bootdbus.NewClient(s.EnsureConn(c))

	called := false

	s.handler.MockPrimarySlotFn(func() (boot.Slot, error) {
		called = true
		return boot.SlotA, nil
	})

	// Both client and service are happy.
	slot, err := client.PrimarySlot()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotA)
	c.Check(called, Equals, true)

	// Service returns invalid slot, client is unhappy.
	called = false

	s.handler.MockPrimarySlotFn(func() (boot.Slot, error) {
		called = true

		return boot.InvalidSlot, nil
	})

	_, err = client.PrimarySlot()
	c.Assert(err, ErrorMatches, `invalid boot slot`)
	c.Check(called, Equals, true)

	// Service fails, client reports the error.
	called = false

	s.handler.MockPrimarySlotFn(func() (boot.Slot, error) {
		called = true

		return boot.InvalidSlot, errBoom
	})

	_, err = client.PrimarySlot()
	c.Assert(err, ErrorMatches, `boom`)
	c.Check(called, Equals, true)
}

func (s *clientSuite) TestSetPrimarySlot(c *C) {
	s.EnsureService(c)
	client := bootdbus.NewClient(s.EnsureConn(c))

	// Both client and service are happy.
	called := false

	s.handler.MockSetPrimarySlotFn(func(slot boot.Slot) error {
		c.Check(slot, Equals, boot.SlotA)

		called = true

		return nil
	})

	err := client.SetPrimarySlot(boot.SlotA)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	// Service fails, client reports the error.
	called = false

	s.handler.MockSetPrimarySlotFn(func(slot boot.Slot) error {
		c.Assert(slot, Equals, boot.SlotA)

		called = true

		return errBoom
	})

	err = client.SetPrimarySlot(boot.SlotA)
	c.Assert(err, ErrorMatches, `boom`)
	c.Check(called, Equals, true)

	// Client tries to set invalid slot, service doesn't see the request.
	s.handler.MockSetPrimarySlotFn(func(slot boot.Slot) error {
		panic("service should have not been called")
	})

	err = client.SetPrimarySlot(boot.InvalidSlot)
	c.Assert(err, ErrorMatches, `invalid boot slot`)
}

func (s *clientSuite) TestSlotState(c *C) {
	s.EnsureService(c)
	client := bootdbus.NewClient(s.EnsureConn(c))

	// Both client and service are happy.
	called := false

	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		c.Check(slot, Equals, boot.SlotA)

		called = true

		return boot.GoodSlot, nil
	})

	state, err := client.SlotState(boot.SlotA)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.GoodSlot)
	c.Check(called, Equals, true)

	// Service fails, client reports the error.
	called = false

	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		c.Check(slot, Equals, boot.SlotB)

		called = true

		return boot.InvalidSlotState, errBoom
	})

	_, err = client.SlotState(boot.SlotB)
	c.Assert(err, ErrorMatches, `boom`)

	// Client asks about invalid slot, service does not see the request.
	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		panic("service should have not been called")
	})

	state, err = client.SlotState(boot.InvalidSlot)
	c.Assert(err, ErrorMatches, `invalid boot slot`)
	c.Check(state, Equals, boot.InvalidSlotState)

	// Service returns invalid state, client is unhappy.
	called = false

	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		c.Check(slot, Equals, boot.SlotB)

		called = true

		return boot.InvalidSlotState, nil
	})

	_, err = client.SlotState(boot.SlotB)
	c.Assert(err, ErrorMatches, `invalid slot state`)
	c.Check(called, Equals, true)
}

func (s *clientSuite) TestSetSlotState(c *C) {
	s.EnsureService(c)
	client := bootdbus.NewClient(s.EnsureConn(c))

	// Both client and service are happy.
	called := false

	s.handler.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error {
		c.Check(slot, Equals, boot.SlotA)
		c.Check(state, Equals, boot.GoodSlot)

		called = true

		return nil
	})

	err := client.SetSlotState(boot.SlotA, boot.GoodSlot)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	// Service fails, client reports the error.
	called = false

	s.handler.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error {
		c.Check(slot, Equals, boot.SlotA)
		c.Check(state, Equals, boot.GoodSlot)

		called = true

		return errBoom
	})

	err = client.SetSlotState(boot.SlotA, boot.GoodSlot)
	c.Assert(err, ErrorMatches, `boom`)
	c.Check(called, Equals, true)

	// Client sets the state of an invalid slot, service does not see the request.
	s.handler.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error {
		panic("service should have not been called")
	})

	err = client.SetSlotState(boot.InvalidSlot, boot.GoodSlot)
	c.Assert(err, ErrorMatches, `invalid boot slot`)

	// Client sets an invalid slot state, service does not see the request.
	s.handler.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error {
		panic("service should have not been called")
	})

	err = client.SetSlotState(boot.SlotA, boot.InvalidSlotState)
	c.Assert(err, ErrorMatches, `invalid slot state`)
}

func (s *clientSuite) TestIntrospection(c *C) {
	s.EnsureService(c)

	// Introspection data is published and looks correct.
	obj := s.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	node, err := introspect.Call(obj)
	c.Assert(err, IsNil)
	c.Check(node, DeepEquals, &introspect.Node{
		XMLName: xml.Name{Local: "node"},
		Name:    "/org/oniroproject/sysota1/Service",
		Interfaces: []introspect.Interface{{
			Name: "org.oniroproject.sysota1.RAUC.BootHandler",
			Methods: []introspect.Method{{
				Name: "GetPrimary",
				Args: []introspect.Arg{
					{Name: "slot", Type: "s", Direction: "out"},
				},
			}, {
				Name: "SetPrimary",
				Args: []introspect.Arg{
					{Name: "slot", Type: "s", Direction: "in"},
				},
			}, {
				Name: "GetState",
				Args: []introspect.Arg{
					{Name: "slot", Type: "s", Direction: "in"},
					{Name: "state", Type: "s", Direction: "out"}},
			}, {
				Name: "SetState",
				Args: []introspect.Arg{
					{Name: "slot", Type: "s", Direction: "in"},
					{Name: "state", Type: "s", Direction: "in"},
				},
			}},
		}, {
			Name: "org.freedesktop.DBus.Introspectable",
			Methods: []introspect.Method{{
				Name: "Introspect",
				Args: []introspect.Arg{
					{Name: "out", Type: "s", Direction: "out"}},
			}},
		}},
	})
}
