// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boottest

import (
	"gitlab.com/zygoon/sysota/boot"
)

// BootState implements rauc.BootModeHolder for the BootProtocolAdapter
type BootState struct {
	bootMode boot.Mode

	// BootModeChanged can be used to observe or verify changes to boot mode.
	BootModeChanged func(boot.Mode, bool) error
}

// BootMode returns the current boot mode.
func (state *BootState) BootMode() boot.Mode {
	return state.bootMode
}

// SetBootMode sets the new boot mode.
//
// Changes to boot mode are propagated to BootModeChanged callback before
// becoming effective, enabling both notifications and validation use-cases.
func (state *BootState) SetBootMode(bootMode boot.Mode, isRollback bool) error {
	if state.bootMode == bootMode {
		return nil
	}

	if state.BootModeChanged != nil {
		if err := state.BootModeChanged(bootMode, isRollback); err != nil {
			return err
		}
	}

	state.bootMode = bootMode

	return nil
}
