<!--
SPDX-License-Identifier: CC-BY-4.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# Comparison of SysOTA and OpenHarmony OTA

The SysOTA project was started after an investigation of the update stack of
the OpenHarmony project. OpenHarmony code is ever-shifting, but at the time of
this writing can be found at https://gitee.com/openharmony. SysOTA code can be
found at https://gitlab.com/zygoon/sysota.

## Overview

OpenHarmony code has a similar conceptual architecture, with A/B slots and
updates across reboot. Where they differ is in implementation technology, means
of interaction with the rest of the OS, dependencies and completeness.

OpenHarmony is predominantly implemented in C and C++, and uses a particular
arrangement of frameworks, kits and HALs that are compatible with linking in
everything as a library. This is also true of the `update_ota_lite` repository,
which cannot be easily used without the rest of the OpenHarmony stack.

In contrast, SysOTA is implemented in Go, and designed to run as a set of
cooperating system services. There are no C APIs involved and all inter-process
communication is handled with D-Bus.

## APIs

APIs are important for constructing systems. This is why SysOTA picked separate
services with D-Bus over in-process library model.

Using D-Bus has several advantages.

First of all it allows individual processes to have locked down permissions. An
unprivileged process may be allowed to query for the basic information or to
check if an update is available. Inside SysOTA itself, this architecture allows
certain elements to be confined from writing to critical areas, for example to
re-configure the boot loader.

The second advantage is that D-Bus hides the implementation language of the
service, allowing individual pieces to be implemented in widely different
toolkits, such as C (legacy services), Go or Rust (modern services) or Dart
(user interfaces).

D-Bus also acts as permission boundary, as the message server both enforces
communication rules (who can talk to who) and validates the formats of the
messages (no corrupt data).

In contrast, the in-memory model often requires all the elements to be
implemented in one language. Today this is very often the memory-unsafe C or
C++ programming language duo. Any mistake in this code can have catastrophic
consequences or may introduce a security vulnerability. Memory-safe languages
like Go avoid this problem entirely, making it much harder to craft a
successful attack apart from providing a much more productive environment to
begin with.

In the future we may re-write parts of SysOTA or its dependencies from C to Go
or Rust and the entire stack will retain API compatibility with existing binary
applications.

## Dependencies and interactions

OpenHarmony components are tightly related and difficult to use in isolation.
Huawei Open Source Technology Center (OSTC) has been trying to integrate
OpenHarmony components into a modern but traditional, non-Android, Linux
distribution since 2021 with limited success.

In contrast, *all* of SysOTA can be cross-compiled for a given CPU architecture
and OS with literally one invocation of the Go compiler. While it was not
designed to be portable across operating system Kernels (nor should it, since
doing so would compromise on the design), building and deploying SysOTA onto
virtually any board is relatively trivial.

SysOTA aims to innovate where it can but does not shy of adding dependencies
where it is useful. As such, as an essential dependency, SysOTA uses the RAUC
project for several essential features. RAUC is an established project, with
countless deployments and rich feature set. RAUC is very flexible and even
though it is technically a service, it can be configured to act and do nearly
anything, using the flexible and open-ended handler and hook system. SysOTA
relies on RAUC to perform cryptographic verification of update packages,
installation of the update payload into one of the available slots and
management of slot space. 

Interestingly RAUC has one additional advantage. Since it is a part of the
embedded Linux ecosystem for so many years, it is widely supported by other
tools. RAUC offers D-Bus APIs and existing good programs and services use those
APIs.

Leveraging RAUC allows SysOTA to offer basic integration with eclipse HawkBit
without writing any line of code. The Yocto project contains high-quality,
maintained packaging recipes for RAUC and for producing update packages (also
known as bundles).

There are ongoing development projects exploring novel techniques for storing
RAUC bundles, so that delta retrieval is both efficient and automatic, such as
the casync project.

Eclipse HawkBit can be used to manage *any* Oniro device, with SysOTA, RAUC and
rauc-hawkbit-updater to form a capable fleet management system.

All of this represents network effects of being in the open Yocto community,
where ideas are explored, commercialized or abandoned, and where successful
ideas may stick and gain traction.

In contrast using the OpenHarmony stack, one has to not only figure out how to
build the update library, but also how to build even the most basic product
around it, with system service, command line tools, APIs for other parts of the
system, server-side fleet management or even basic update servers.

## Robustness

At the core of SysOTA lies a transactional update protocol, with A and B slots
being atomically _switched_ during the update transaction and then either
committed after being deemed correct and healthy, or automatically rolled back
in case of unexpected failure, power loss or health-check failure.

This inner core lets us put the OTA stack at the foundation of the system,
where any devices in the field can eventually and safely upgrade to a version
they will accept and keep, allowing further updates to come in later.

It is actually challenging to provide this guarantee without having a way to
re-design hardware or firmware interfaces. SysOTA is implemented very carefully
to avoid any critical sections, where unexpected failure, such as a power loss,
would result in the wrong thing happening.

In contrast there is no such guarantee in the OpenHarmony APIs. In the case of
failure it may be possible to use the recovery mode to attempt to recover the
system, but in general it is not something that can happen unattended. Given
that the update framework is not a complete implementation, and that there is
no public source code using this functionality, it is _possible_ that a more
complete and safer system exists in private. For an open project, one would
have to re-implement it from scratch.

## Practicality

SysOTA is designed to be compatible with a wide range of popular hardware.
Together with RAUC it can support nearly any x86, ARM (both server-like
non-server-like) or RISC device, with a wide range of possible boot loaders to
choose from.

While SysOTA does choose to implement boot loader handling internally, it can
also use RAUC's existing features. Apart from introducing a whole new boot
loader into the mix, the work needed to configure SysOTA for a given device can
be measured in hours. 

In contrast, OpenHarmony does not provide this element at all. Essential
interactions are hidden behind the HAL (hardware abstraction layer) and no
implementation for popular community or industrial devices is provided.

## Compatibility

In theory it is possible to create a C library offering OpenHarmony APIs, as
defined by `update_ota_lite` while internally calling SysOTA and RAUC D-Bus
APIs. Such library would allow bridging the gap between the two solutions.

At this time this has not been attempted as we are not aware of any program
integrating with the OpenHarmony APIs that could be used as a driving factor.

## Summary

SysOTA and OpenHarmony update framework represent similar ideas in widely
different implementations. Both projects use the classic A/B approach instead
of in-place updates. SysOTA relies on Linux services and D-Bus while
OpenHarmony offers in-process library strictly tied to the rest of the
OpenHarmony C/C++ interfaces. SysOTA can be easily deployed on new hardware,
with support for popular boot loaders and, thanks to RAUC, clients to cloud
management solutions like Eclipse HawkBit. OpenHarmony updater offers no such
functionality.
