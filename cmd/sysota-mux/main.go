// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package main implements the sysota-mux multiplexer.
//
// The executable avoid having to build multiple small executables, each
// statically linking to the Go standard library. This saves tremendous amounts
// of disk space at the cost of mapping some extra memory for long-running
// programs.
//
// During development, the fact that all of SysOTA compiles down to one static
// binary is convenient, as it allows easier iteration on top of read-only disk
// images.
package main

import (
	"context"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"

	rauccustomboothandler "gitlab.com/zygoon/sysota/cmd/rauc-custom-boot-handler"
	raucpostinstallhandler "gitlab.com/zygoon/sysota/cmd/rauc-post-install-handler"
	raucpreinstallhandler "gitlab.com/zygoon/sysota/cmd/rauc-pre-install-handler"
	"gitlab.com/zygoon/sysota/cmd/sysotactl"
	"gitlab.com/zygoon/sysota/cmd/sysotad"
)

// Cmd implements the sysota-mux command multiplexer.
type Cmd struct {
	preserveArgs0 bool
}

// Run routes execution to the appropriate command.
func (cmd Cmd) Run(ctx context.Context, args []string) error {
	r := router.Router{
		Name: "sysota-mux",
		Commands: map[string]cmdr.Cmd{
			sysotactl.Name:              sysotactl.Cmd{},
			sysotad.Name:                &sysotad.Cmd{Opts: sysotad.DefaultOptions()},
			raucpreinstallhandler.Name:  raucpreinstallhandler.Cmd{},
			raucpostinstallhandler.Name: raucpostinstallhandler.Cmd{},
			rauccustomboothandler.Name:  rauccustomboothandler.Cmd{},
		},
		UseArgv0: cmd.preserveArgs0,
	}

	if err := r.Run(ctx, args); err != nil {
		return err
	}

	return nil
}

// PreserveArgs0 instructs RunMain to preserve args[0].
func (cmd Cmd) PreserveArgs0() bool {
	return cmd.preserveArgs0
}

func main() {
	if len(os.Args) > 0 && strings.HasSuffix(os.Args[0], ".test") {
		return
	}

	cmd := Cmd{
		preserveArgs0: len(os.Args) > 0 && filepath.Base(os.Args[0]) != "sysota-mux",
	}

	cmdr.RunMain(cmd, os.Args,
		cmdr.WithSignals(os.Interrupt, syscall.SIGTERM),
		cmdr.WithPanicBehavior(cmdr.RecoverTraceExit),
	)
}
