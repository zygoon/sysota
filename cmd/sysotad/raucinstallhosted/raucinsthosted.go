// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package raucinstallhosted implements D-Bus interface exposing RAUC install
// handler provided as installhandler.Interface.
package raucinstallhosted

import (
	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"

	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installdbus/spec"
)

const (
	environArgName = "environ"
)

// HostedService exposes RAUC custom boot handler over D-Bus.
type HostedService struct {
	iface installhandler.Interface
	conn  *dbus.Conn
}

// New returns a hosted service with the given install handler.
func New(iface installhandler.Interface) *HostedService {
	return &HostedService{iface: iface}
}

// JoinServiceHost integrates with dbusutil.ServiceHost.
func (hs *HostedService) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	hs.conn = reg.DBusConn()

	ifaceReg := reg.Object(svcspec.ObjectPath).Interface(spec.InterfaceName)

	ifaceReg.SetMethods(map[string]interface{}{
		spec.PreInstall:  hs.PreInstall,
		spec.PostInstall: hs.PostInstall,
	})

	ifaceReg.SetIntrospection(&introspect.Interface{
		Name: spec.InterfaceName,
		Methods: []introspect.Method{
			{
				Name: spec.PreInstall,
				Args: []introspect.Arg{
					// Pass environment block as input. This allows the protocol
					// to evolve without changing the API.
					{Name: environArgName, Type: "as", Direction: dbusutil.In},
				},
			},
			{
				Name: spec.PostInstall,
				Args: []introspect.Arg{
					{Name: environArgName, Type: "as", Direction: dbusutil.In},
				},
			},
		},
	})

	return nil
}

func (hs *HostedService) populateHandlerPID(env *installhandler.Environment, sender dbus.Sender) error {
	// Find the pid of the sender and store it int the handler context.
	// See https://dbus.freedesktop.org/doc/dbus-specification.html
	var pid uint32

	if err := hs.conn.BusObject().Call("org.freedesktop.DBus.GetConnectionUnixProcessID", 0, sender).Store(&pid); err != nil {
		return err
	}

	env.InitialHandlerPID = int(pid)

	return nil
}

// PreInstall maps installhandler.Install.PreInstall to D-Bus.
func (hs *HostedService) PreInstall(sender dbus.Sender, environ []string) (dbusErr *dbus.Error) {
	env, err := installhandler.NewEnvironment(environ)
	if err != nil {
		return dbus.MakeFailedError(err)
	}

	if err := hs.populateHandlerPID(env, sender); err != nil {
		return dbus.MakeFailedError(err)
	}

	if err := hs.iface.PreInstall(env); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// PostInstall maps installhandler.Interface.PostInstall to D-Bus.
func (hs *HostedService) PostInstall(sender dbus.Sender, environ []string) (dbusErr *dbus.Error) {
	env, err := installhandler.NewEnvironment(environ)
	if err != nil {
		return dbus.MakeFailedError(err)
	}

	if err := hs.populateHandlerPID(env, sender); err != nil {
		return dbus.MakeFailedError(err)
	}

	if err := hs.iface.PostInstall(env); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}
