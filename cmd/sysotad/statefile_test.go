// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
package sysotad_test

import (
	"errors"
	"os"
	"path/filepath"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/cmd/sysotad"
	"gitlab.com/zygoon/sysota/ota"

	. "gopkg.in/check.v1"
)

type stateFileSuite struct{}

var _ = Suite(&stateFileSuite{})

func (s *stateFileSuite) TestLoad(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "state.ini")
	sf := sysotad.StateFile(name)

	err := os.WriteFile(name, []byte("[System]\nBootMode=try"), 0600)
	c.Assert(err, IsNil)

	st, err := sf.LoadState()
	c.Assert(err, IsNil)
	c.Check(st, DeepEquals, &ota.SystemState{BootMode: boot.Try})
}

func (s *stateFileSuite) TestSave(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "state.ini")
	sf := sysotad.StateFile(name)

	err := sf.SaveState(&ota.SystemState{BootMode: boot.Try})
	c.Assert(err, IsNil)

	data, err := os.ReadFile(name)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "[System]\nBootMode=try\n")
}

func (s *stateFileSuite) TestSaveNoBaseDir(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "subdir", "state.ini")
	sf := sysotad.StateFile(name)

	// When the base directory does not exist the error is silently ignored.
	err := sf.SaveState(&ota.SystemState{BootMode: boot.Try})
	c.Assert(err, IsNil)

	_, err = os.ReadFile(name)
	c.Assert(errors.Is(err, os.ErrNotExist), Equals, true)
}
