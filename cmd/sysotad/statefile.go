// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotad

import (
	"errors"
	"os"

	"gitlab.com/zygoon/sysota/ota"
)

// StateFile implements ota.StateFile and is used as I/O interface for the service.
type StateFile string

// LoadState loads system state.
func (sf StateFile) LoadState() (*ota.SystemState, error) {
	return ota.LoadState(string(sf))
}

// SaveState saves system state.
//
// Historically ENOENT-style errors are ignored. This can happen if the
// directory which is supposed to hold the state file does not exist.
func (sf StateFile) SaveState(st *ota.SystemState) error {
	err := ota.SaveState(st, string(sf))

	// Ignore save errors if the state directory does not exist.
	// TODO(zyga): this code should go away, it was created early on and does
	// not make much sense anymore.
	var pathErr *os.PathError
	if errors.As(err, &pathErr) && os.IsNotExist(pathErr) {
		return nil
	}

	// Did we save the state successfully?
	if err == nil {
		// TODO(zyga): pass context and allow this place to log reliably.
		// _, _ = fmt.Fprintf(Stdout, "System state saved to %s.\n", string(sf))
		return nil
	}

	return err
}
