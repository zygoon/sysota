// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package raucadapter_test

import (
	"errors"
	"testing"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/boot/testboot"
	"gitlab.com/zygoon/sysota/cmd/sysotad/raucadapter"
	"gitlab.com/zygoon/sysota/rauc/boothandler/boottest"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installtest"
)

func Test(t *testing.T) { TestingT(t) }

var errBoom = errors.New("boom")

type adapterSuite struct {
	proto     testboot.Protocol
	bootState boottest.BootState
	adapter   *raucadapter.Adapter
	called    int

	// variant holds the description of the test suite variant.
	variant CommentInterface
	// fromSlot is the slot the system is updating from.
	fromSlot boot.Slot
	// toSlot is the slot the system is updating to.
	toSlot boot.Slot
	// pristine causes QueryActive and QueryInactive to fail with
	// boot.ErrPristineBootConfig error.
	pristine bool
}

func (s *adapterSuite) SetUpSuite(c *C) {
	s.adapter = raucadapter.New(&s.proto, &s.bootState)
}

func (s *adapterSuite) SetUpTest(c *C) {
	s.cleanSlate(c)
	// This is only set in the per-test setup so that individual tests
	// can call cleanSlate and still observe the changes to the boot mode.
	c.Assert(s.bootState.SetBootMode(boot.Normal, false), IsNil)
}

func (s *adapterSuite) cleanSlate(c *C) {
	s.proto.ResetCallbacks()
	s.bootState.BootModeChanged = nil
	s.called = 0
}

// This suite represents the state of the system before the first update (factory image).
var _ = Suite(&adapterSuite{
	variant:  Commentf("scenario: factory image -> B"),
	fromSlot: boot.SlotA,
	toSlot:   boot.SlotB,
	pristine: true,
})

// This suite represents the state of the system after the first, third, fifth (and so on) update.
var _ = Suite(&adapterSuite{
	variant:  Commentf("scenario: B -> A"),
	fromSlot: boot.SlotB,
	toSlot:   boot.SlotA,
})

// This suite represents the state of the system after the second, fourth, sixth (and so on) update.
var _ = Suite(&adapterSuite{
	variant:  Commentf("scenario: A -> B"),
	fromSlot: boot.SlotA,
	toSlot:   boot.SlotB,
})

func (s *adapterSuite) TestInstallGoodPlayThrough(c *C) {
	// This test corresponds to the spread test rauc/tests/rauc-install-good and
	// shows how RAUC operations are translated to boot.Protocol operations.
	// It has been generalized to cover three possible variants as conveyed by
	// the three adapterSuite instances defined above.
	//
	// custom: set-state $toSlot bad
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetSlotState(s.toSlot, boot.BadSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)

	// custom: set-primary $toSlot
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		// NOTE: Throughout the process, the bootloader has $fromSlot as the
		// active slot. On a real implementation of boot.Protocol this doesn't
		// change until CommitSwitch is called.
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.TrySwitchFn = func(slot boot.Slot) error {
		s.called++

		c.Assert(slot, Equals, s.toSlot)

		return nil
	}
	err = s.adapter.SetPrimarySlot(s.toSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 2, s.variant)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// TODO(zyga): Conceptually, we would reboot with the special flag here.
	// This is not tested yet, as it requires cooperation between RAUC
	// bootloader handler and RAUC install handler.

	// custom: get-primary
	s.cleanSlate(c)

	s.proto.QueryInactiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.toSlot, nil
	}

	slot, err := s.adapter.PrimarySlot()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, s.toSlot)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// custom: get-state $toSlot
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	state, err := s.adapter.SlotState(s.toSlot)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.BadSlot)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// custom: get-state $fromSlot
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	state, err = s.adapter.SlotState(s.fromSlot)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.GoodSlot, s.variant)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// custom: set-state $toSlot good
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.CommitSwitchFn = func() error {
		s.called++

		return nil
	}
	err = s.adapter.SetSlotState(s.toSlot, boot.GoodSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 2)
	// Note that boot.Mode switches to Normal.
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

func (s *adapterSuite) TestInstallBadPlayThrough(c *C) {
	// This test corresponds to the spread test rauc/tests/rauc-install-bad and
	// shows how RAUC operations are translated to boot.Protocol operations.
	// It has been generalized to cover three possible variants as conveyed by
	// the three adapterSuite instances defined above.
	//
	// custom: set-state $toSlot bad.
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetSlotState(s.toSlot, boot.BadSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)

	// custom: set-primary $toSlot
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		// NOTE: Throughout the process, the bootloader has SlotA as the active
		// slot. On a real implementation of boot.Protocol this doesn't change
		// until CommitSwitch is called.
		s.called++
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.TrySwitchFn = func(slot boot.Slot) error {
		s.called++

		c.Assert(slot, Equals, s.toSlot)

		return nil
	}
	err = s.adapter.SetPrimarySlot(s.toSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 2, s.variant)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// TODO(zyga): Conceptually, we would reboot with the special flag here.
	// This is not tested yet, as it requires cooperation between RAUC
	// bootloader handler and RAUC install handler.

	// custom: get-primary
	s.cleanSlate(c)

	s.proto.QueryInactiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.toSlot, nil
	}
	slot, err := s.adapter.PrimarySlot()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, s.toSlot)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// custom: get-state $toSlot
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	state, err := s.adapter.SlotState(s.toSlot)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.BadSlot)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// custom: get-state $fromSlot
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	state, err = s.adapter.SlotState(s.fromSlot)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.GoodSlot)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)

	// custom: set-state $toSlot bad
	s.cleanSlate(c)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.CancelSwitchFn = func() error {
		s.called++

		return nil
	}
	err = s.adapter.SetSlotState(s.toSlot, boot.BadSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 2)
	// Note that boot.Mode switches to Normal.
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

// Tests for BootProtocolAdapter.PrimarySlot.

func (s *adapterSuite) TestPrimarySlotInNormalMode(c *C) {
	c.Assert(s.bootState.SetBootMode(boot.Normal, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	slot, err := s.adapter.PrimarySlot()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, s.fromSlot)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

func (s *adapterSuite) TestPrimarySlotInTryMode(c *C) {
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryInactiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.toSlot, nil
	}
	slot, err := s.adapter.PrimarySlot()
	c.Assert(err, IsNil, s.variant)
	c.Check(slot, Equals, s.toSlot)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)
}

func (s *adapterSuite) TestPrimarySlotInInvalidMode(c *C) {
	c.Assert(s.bootState.SetBootMode(boot.InvalidBootMode, false), IsNil)
	_, err := s.adapter.PrimarySlot()
	c.Assert(err, ErrorMatches, `invalid boot mode`)
}

// Tests for BootProtocolAdapter.SetPrimarySlot.

func (s *adapterSuite) TestSetPrimarySlotToActiveInNormalMode(c *C) {
	c.Assert(s.bootState.SetBootMode(boot.Normal, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetPrimarySlot(s.fromSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

func (s *adapterSuite) TestSetPrimaryToInactiveInNormalMode(c *C) {
	// Setting the primary slot to the inactive slot while in normal boot mode
	// commences an update transaction.
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.TrySwitchFn = func(slot boot.Slot) error {
		return nil
	}
	err := s.adapter.SetPrimarySlot(s.toSlot)
	c.Assert(err, IsNil)
	c.Check(s.bootState.BootMode(), Equals, boot.Try, s.variant)
}

func (s *adapterSuite) TestSetPrimarySlotToActiveInTryMode(c *C) {
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}

	// Even though $fromSlot is active, because we are in try-boot transaction,
	// this operation is invalid and must be rejected. If it were allowed, there
	// would be a discrepancy with the following call to PrimarySlot, which
	// would not return the same slot.
	err := s.adapter.SetPrimarySlot(s.fromSlot)
	c.Assert(err, ErrorMatches, `violation of protocol between SystemOTA and RAUC`, s.variant)
}

func (s *adapterSuite) TestSetPrimarySlotToInactiveInTryMode(c *C) {
	activeSlot := s.fromSlot
	inactiveSlot := boot.SynthesizeInactiveSlot(activeSlot)

	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetPrimarySlot(inactiveSlot)
	c.Assert(err, IsNil, s.variant)
	c.Assert(s.called, Equals, 1)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)
}

func (s *adapterSuite) TestSetPrimarySlotToPrimarySlot(c *C) {
	// Invariant, setting primary slot to itself is a safe no-op.
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.QueryInactiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.toSlot, nil
	}

	for _, bootMode := range []boot.Mode{boot.Normal, boot.Try} {
		comment := Commentf("%s, boot mode: %s", s.variant.CheckCommentString(), bootMode)
		err := s.bootState.SetBootMode(bootMode, false)
		c.Assert(err, IsNil, comment)

		primarySlot, err := s.adapter.PrimarySlot()
		c.Assert(err, IsNil, comment)
		err = s.adapter.SetPrimarySlot(primarySlot)
		c.Assert(err, IsNil, comment)

		c.Check(s.bootState.BootMode(), Equals, bootMode, comment)
	}
}

func (s *adapterSuite) TestSetPrimarySlotToInvalidSlot(c *C) {
	err := s.adapter.SetPrimarySlot(boot.InvalidSlot)
	c.Assert(err, ErrorMatches, `invalid boot slot`)
}

func (s *adapterSuite) TestSetPrimarySlotButQueryActiveFailed(c *C) {
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		// Ignore s.pristine since this is about general failure of QueryActive.
		return boot.InvalidSlot, errBoom
	}
	err := s.adapter.SetPrimarySlot(s.fromSlot)
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *adapterSuite) TestSetPrimaryToInactiveButTrySwitchFailed(c *C) {
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.TrySwitchFn = func(slot boot.Slot) error {
		return errBoom
	}
	err := s.adapter.SetPrimarySlot(s.toSlot)
	c.Assert(err, ErrorMatches, `boom`, s.variant)

	// Note that a transaction was not started.
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

func (s *adapterSuite) TestSetPrimaryButBootModeIsInvalid(c *C) {
	err := s.bootState.SetBootMode(boot.InvalidBootMode, false)
	c.Check(err, IsNil)

	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}

	err = s.adapter.SetPrimarySlot(s.fromSlot)
	c.Assert(err, ErrorMatches, `invalid boot mode`, s.variant)
}

// Tests for BootProtocolAdapter.SlotState.

func (s *adapterSuite) TestSlotStateInvalidSlot(c *C) {
	_, err := s.adapter.SlotState(boot.InvalidSlot)
	c.Assert(err, ErrorMatches, `invalid boot slot`)
}

func (s *adapterSuite) TestSlotStateButQueryActiveFailed(c *C) {
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		// Ignore s.pristine since this is about general failure of QueryActive.
		return boot.InvalidSlot, errBoom
	}

	_, err := s.adapter.SlotState(s.fromSlot)
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *adapterSuite) TestSlotStateOfActiveSlot(c *C) {
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}

	state, err := s.adapter.SlotState(s.fromSlot)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.GoodSlot)
}

func (s *adapterSuite) TestSlotStateOfInactiveSlot(c *C) {
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}

	state, err := s.adapter.SlotState(s.toSlot)
	c.Assert(err, IsNil)
	c.Check(state, Equals, boot.BadSlot)
}

// Tests for BootProtocolAdapter.SetSlotState.

func (s *adapterSuite) TestSetSlotStateOfActiveSlotInNormalModeToGood(c *C) {
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetSlotState(s.fromSlot, boot.GoodSlot)
	c.Assert(err, IsNil)
}

func (s *adapterSuite) TestSetSlotStateOfActiveSlotInNormalModeToBad(c *C) {
	// RAUC never sets the state of the active slot to bad.
	//
	// For SystemOTA this operation is meaningless, as we cannot abort a
	// transaction that we don't have in normal mode. If this happens we are out
	// of luck as there is nothing to do.
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetSlotState(s.fromSlot, boot.BadSlot)
	c.Assert(err, ErrorMatches, `violation of protocol between SystemOTA and RAUC`, s.variant)
}

func (s *adapterSuite) TestSetSlotStateOfInactiveSlotInNormalModeToGood(c *C) {
	// For SystemOTA this operation is meaningless, as the inactive slot can
	// only be qualified as good when we are in an update transaction and wish
	// to commit it. In normal mode the active slot is always primary. In try
	// mode the inactive slot is always primary.
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetSlotState(s.toSlot, boot.GoodSlot)
	c.Assert(err, ErrorMatches, `violation of protocol between SystemOTA and RAUC`, s.variant)
}

func (s *adapterSuite) TestSetSlotStateOfActiveSlotInTryModeToBad(c *C) {
	// For SystemOTA this operation is meaningless because the active slot
	// cannot be marked as bad by definition, because the active slot must and
	// is always considered good.
	//
	// Note that this terminology is confusing.
	//
	// Active slot is a SystemOTA term for the slot is what we use for booting
	// outside of the special case where the inactive slot is booted. Primary
	// slot is the RAUC term for the slot that is used for booting in general.
	//
	// RAUC model is stateful, system OTA model is transactional, in the sense
	// that rauc reads and writes the state and any combination can be
	// expressed. SystemOTA synthesizes the state that RAUC expects based on the
	// boot.Mode alone (that is, one bit of information).
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetSlotState(s.fromSlot, boot.BadSlot)
	c.Assert(err, ErrorMatches, `violation of protocol between SystemOTA and RAUC`, s.variant)
}

func (s *adapterSuite) TestSetSlotStateOfActiveSlotInTryModeToGood(c *C) {
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	err := s.adapter.SetSlotState(s.fromSlot, boot.GoodSlot)
	c.Assert(err, IsNil, s.variant)
}

func (s *adapterSuite) TestSetSlotStateOfInactiveSlotInTryModeToBad(c *C) {
	// While in try-boot mode, setting the state of the inactive slot
	// to bad cancels the update transaction.
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		// Note that $fromSlot is active.
		return s.fromSlot, nil
	}
	s.proto.CancelSwitchFn = func() error {
		s.called++
		return nil
	}
	// Note that we are setting the state of $toSlot, which is the "other" slot
	// compared to what was returned by QueryActive.
	err := s.adapter.SetSlotState(s.toSlot, boot.BadSlot)
	c.Assert(err, IsNil, s.variant)
	c.Assert(s.called, Equals, 2)
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

func (s *adapterSuite) TestSetSlotStateOfInactiveSlotInTryModeToGood(c *C) {
	// While in try-boot mode, setting the state of the inactive slot
	// to good commits the update transaction.
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++
		// Note that $fromSlot is active.
		return s.fromSlot, nil
	}
	s.proto.CommitSwitchFn = func() error {
		s.called++
		return nil
	}
	// Note that we are setting the state of $toSlot, which is the "other" slot
	// compared to what was returned by QueryActive.
	err := s.adapter.SetSlotState(s.toSlot, boot.GoodSlot)
	c.Assert(err, IsNil)
	c.Assert(s.called, Equals, 2)
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

func (s *adapterSuite) TestSetSlotStateOfInvalidSlot(c *C) {
	err := s.adapter.SetSlotState(boot.InvalidSlot, boot.GoodSlot)
	c.Assert(err, ErrorMatches, `invalid boot slot`)
}

func (s *adapterSuite) TestSetSlotStateToInvalidState(c *C) {
	err := s.adapter.SetSlotState(s.fromSlot, boot.InvalidSlotState)
	c.Assert(err, ErrorMatches, `invalid slot state`)
}

func (s *adapterSuite) TestSetSlotStateButQueryActiveFailed(c *C) {
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		// Ignore s.pristine since this is about general failure of QueryActive.
		return boot.InvalidSlot, errBoom
	}
	err := s.adapter.SetSlotState(s.fromSlot, boot.GoodSlot)
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *adapterSuite) TestSetSlotStateOfInactiveSlotInTryModeToBadButSetBootModeFailed(c *C) {
	// We are trying to cancel the transaction but we failed to set the new boot mode.
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++
		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.bootState.BootModeChanged = func(bootMode boot.Mode, isRollback bool) error {
		c.Assert(bootMode, Equals, boot.Normal)
		s.called++

		return errBoom
	}
	err := s.adapter.SetSlotState(s.toSlot, boot.BadSlot)
	c.Assert(err, ErrorMatches, `boom`, s.variant)
	c.Assert(s.called, Equals, 2)
	c.Check(s.bootState.BootMode(), Equals, boot.Try)
}

func (s *adapterSuite) TestSetSlotStateOfInactiveSlotInTryModeToBadButCancelSwitchFailed(c *C) {
	// We are trying to cancel the transaction but that operation fails.
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}

		return s.fromSlot, nil
	}
	s.proto.CancelSwitchFn = func() error {
		s.called++
		return errBoom
	}
	err := s.adapter.SetSlotState(s.toSlot, boot.BadSlot)
	c.Assert(err, ErrorMatches, `boom`, s.variant)
	c.Assert(s.called, Equals, 2)
	// Note that even though cancelling the transaction has failed, the boot
	// mode is back to normal. Canceling the transaction cannot be required for
	// correctness as we may never reach this code under adverse circumstances.
	// The entire idea is based on the ability to, in conjunction with the boot
	// loader, to implement a boot-only-once semantics. In the case of failure
	// we are always back to good configuration.
	//
	// CancelSwitch exists only to (perhaps) remove temporary files that were
	// required for the duration of TrySwitch lifetime.
	c.Check(s.bootState.BootMode(), Equals, boot.Normal)
}

func (s *adapterSuite) TestSetSlotStateOfInactiveSlotInTryModeToGoodButCommitSwitchFailed(c *C) {
	// While in try-boot mode, setting the state of the inactive slot
	// to good commits the update transaction.
	c.Assert(s.bootState.SetBootMode(boot.Try, false), IsNil)
	s.proto.QueryActiveFn = func() (boot.Slot, error) {
		s.called++

		if s.pristine {
			return boot.InvalidSlot, boot.ErrPristineBootConfig
		}
		// Note that $fromSlot is active.
		return s.fromSlot, nil
	}
	s.proto.CommitSwitchFn = func() error {
		s.called++
		return errBoom
	}
	// Note that we are setting the state of $toSlot, which is the "other" slot
	// compared to what was returned by QueryActive.
	err := s.adapter.SetSlotState(s.toSlot, boot.GoodSlot)
	c.Assert(err, ErrorMatches, `boom`, s.variant)
	c.Assert(s.called, Equals, 2)

	// Note that because cannot commit the switch we are still, technically, in
	// try mode.
	//
	// This mode requires special cleanup from the SystemOTA logic outside of
	// the adapter. Since the active boot slot never changed, we are going to
	// boot a good known image. We should revert the boot mode to normal and
	// reboot.
	//
	// XXX(zyga): This may warrant a special error type to handle properly.
	c.Check(s.bootState.BootMode(), Equals, boot.Try)
}

func (s *adapterSuite) TestPreInstallWithUnawareBootProtocol(c *C) {
	err := s.adapter.PreInstall(&installhandler.Environment{})
	c.Assert(err, IsNil)
}

func (s *adapterSuite) TestPreInstallWithAwareBootProtocol(c *C) {
	called := false
	env := &installhandler.Environment{}
	proto := &testboot.InstallAwareProtocol{
		Handler: installtest.Handler{},
	}

	proto.Handler.MockPreInstallFn(func(env2 *installhandler.Environment) error {
		c.Assert(env2, Equals, env)

		called = true

		return nil
	})

	adapter := raucadapter.New(proto, &s.bootState)

	err := adapter.PreInstall(env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}

func (s *adapterSuite) TestPostInstallHandlerWithUnawareBootProtocol(c *C) {
	called := false
	env := &installhandler.Environment{}
	proto := &testboot.InstallAwareProtocol{
		Handler: installtest.Handler{},
	}

	proto.Handler.MockPostInstallFn(func(env2 *installhandler.Environment) error {
		c.Assert(env2, Equals, env)

		called = true

		return nil
	})

	adapter := raucadapter.New(proto, &s.bootState)

	err := adapter.PostInstall(env)
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
}
