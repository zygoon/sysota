// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotad_test

import (
	"errors"

	"github.com/godbus/dbus/v5"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/boot/testboot"
	"gitlab.com/zygoon/sysota/cmd/sysotad"
	"gitlab.com/zygoon/sysota/ota"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
)

var errBoom = errors.New("boom")

type serviceSuite struct {
	dbustest.Suite

	config ota.Config
	state  ota.SystemState

	svc  *sysotad.Service
	conn *dbus.Conn
}

var _ = Suite(&serviceSuite{})

func (s *serviceSuite) EnsureConn(c *C) *dbus.Conn {
	var err error

	if s.conn == nil {
		s.conn, err = dbusutil.SystemBus()
		c.Assert(err, IsNil)
	}

	return s.conn
}

func (s *serviceSuite) EnsureService(c *C) {
	var err error

	if s.svc == nil {
		s.EnsureConn(c)

		s.svc, err = sysotad.NewService(s.conn,
			sysotad.WithConfigFiles(&ota.MemoryConfigFiles{Config: &s.config}),
			sysotad.WithStateFile(&ota.MemoryStateFile{SystemState: &s.state}))
		c.Assert(err, IsNil)

		err := s.svc.Export()
		c.Assert(err, IsNil)

		err = sysotad.RequestBusName(s.conn)
		c.Assert(err, IsNil)
	}
}

func (s *serviceSuite) SetUpTest(_ *C) {
	s.config = ota.Config{}
	s.state = ota.SystemState{}
}

func (s *serviceSuite) TearDownTest(c *C) {
	if s.svc != nil {
		err := s.svc.Unexport()
		c.Assert(err, IsNil)

		s.svc = nil
	}

	if s.conn != nil {
		err := s.conn.Close()
		c.Assert(err, IsNil)

		s.conn = nil
	}
}

func (s *serviceSuite) TestSmokeAccessWithoutBootProtocolAcrossDBus(c *C) {
	s.config.DebugBootAPI = true
	s.EnsureService(c)

	svcObject := s.conn.Object("org.oniroproject.sysota1", "/org/oniroproject/sysota1/Service")

	var slotName string

	err := svcObject.Call("org.oniroproject.sysota1.BootLoader.QueryActive", 0).Store(&slotName)
	c.Assert(err, ErrorMatches, `boot protocol is not set`)
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.QueryInactive", 0).Store(&slotName)
	c.Assert(err, ErrorMatches, `boot protocol is not set`)
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.TrySwitch", 0, "B").Store(&slotName)
	c.Assert(err, ErrorMatches, `boot protocol is not set`)
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.CommitSwitch", 0).Store(&slotName)
	c.Assert(err, ErrorMatches, `boot protocol is not set`)
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.CancelSwitch", 0).Store(&slotName)
	c.Assert(err, ErrorMatches, `boot protocol is not set`)
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.Reboot", 0, 0).Store(&slotName)
	c.Assert(err, ErrorMatches, `boot protocol is not set`)
}

func (s *serviceSuite) TestSmokeAccessWithBootProtocolAcrossDBus(c *C) {
	var proto testboot.Protocol

	s.config.BootLoaderType = ota.TestBoot
	s.config.DebugBootAPI = true

	sysotad.SetTestBootProtocol(&proto)

	s.EnsureService(c)

	svcObject := s.conn.Object("org.oniroproject.sysota1", "/org/oniroproject/sysota1/Service")

	var slotName string

	proto.QueryActiveFn = func() (boot.Slot, error) { return boot.SlotA, nil }
	err := svcObject.Call("org.oniroproject.sysota1.BootLoader.QueryActive", 0).Store(&slotName)
	c.Assert(err, IsNil)
	c.Check(slotName, Equals, "A")

	proto.QueryActiveFn = func() (boot.Slot, error) { return boot.InvalidSlot, errBoom }
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.QueryActive", 0).Store(&slotName)
	c.Assert(err, ErrorMatches, `boom`)

	proto.QueryInactiveFn = func() (boot.Slot, error) { return boot.SlotB, nil }
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.QueryInactive", 0).Store(&slotName)
	c.Assert(err, IsNil)
	c.Check(slotName, Equals, "B")

	proto.QueryInactiveFn = func() (boot.Slot, error) { return boot.InvalidSlot, errBoom }
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.QueryInactive", 0).Store(&slotName)
	c.Assert(err, ErrorMatches, `boom`)

	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.TrySwitch", 0, "potato").Store()
	c.Assert(err, ErrorMatches, `invalid boot slot`)

	called := false
	proto.TrySwitchFn = func(slot boot.Slot) error {
		called = true

		c.Check(slot, Equals, boot.SlotB)

		return nil
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.TrySwitch", 0, "B").Store()
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	proto.TrySwitchFn = func(_ boot.Slot) error {
		return errBoom
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.TrySwitch", 0, "B").Store()
	c.Assert(err, ErrorMatches, `boom`)

	called = false
	proto.CommitSwitchFn = func() error {
		called = true

		return nil
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.CommitSwitch", 0).Store()
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	proto.CommitSwitchFn = func() error {
		return errBoom
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.CommitSwitch", 0).Store()
	c.Assert(err, ErrorMatches, `boom`)

	called = false
	proto.CancelSwitchFn = func() error {
		called = true

		return nil
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.CancelSwitch", 0).Store()
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	proto.CancelSwitchFn = func() error {
		return errBoom
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.CancelSwitch", 0).Store()
	c.Assert(err, ErrorMatches, `boom`)

	called = false
	proto.RebootFn = func(flags boot.RebootFlags) error {
		called = true

		c.Check(flags, Equals, boot.RebootTryBoot)

		return nil
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.Reboot", 0, boot.RebootTryBoot).Store()
	c.Assert(err, IsNil)
	c.Check(called, Equals, true)

	proto.RebootFn = func(flags boot.RebootFlags) error {
		return errBoom
	}
	err = svcObject.Call("org.oniroproject.sysota1.BootLoader.Reboot", 0, 0).Store()
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *serviceSuite) TestAccessPropertiesAcrossDBus(c *C) {
	s.config.DeviceMaker = "Mr Potato"
	s.config.DeviceModel = "DevKit 9000"

	s.config.UpdateStream = "latest/nightly"
	s.config.UpdateSystemImagePackage = "potato-os"
	s.config.UpdateServerURL = "https://updates.example.org/"

	s.state.InstalledPackageName = "potato-os"
	s.state.InstalledPackageVersion = "1.0"
	s.state.InstalledSourceRevision = "git-hash"

	s.EnsureService(c)

	svcObject := s.conn.Object("org.oniroproject.sysota1", "/org/oniroproject/sysota1/Service")

	var propValue dbus.Variant

	// Read access.

	err := svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "Maker").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("Mr Potato"))

	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "Model").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("DevKit 9000"))

	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "ServerURL").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("https://updates.example.org/"))

	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "SystemImagePackage").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("potato-os"))

	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "Stream").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("latest/nightly"))

	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "Potato").Store(&propValue)
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.PropertyNotFound`)

	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Potato", "Property").Store(&propValue)
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.InterfaceNotFound`)

	var propMap map[string]dbus.Variant

	err = svcObject.Call("org.freedesktop.DBus.Properties.GetAll", 0, "org.oniroproject.sysota1.Service").Store(&propMap)
	c.Assert(err, IsNil)
	c.Check(propMap, DeepEquals, map[string]dbus.Variant{
		"Maker":                   dbus.MakeVariant("Mr Potato"),
		"Model":                   dbus.MakeVariant("DevKit 9000"),
		"ServerURL":               dbus.MakeVariant("https://updates.example.org/"),
		"SystemImagePackage":      dbus.MakeVariant("potato-os"),
		"Stream":                  dbus.MakeVariant("latest/nightly"),
		"InstalledPackageName":    dbus.MakeVariant("potato-os"),
		"InstalledPackageVersion": dbus.MakeVariant("1.0"),
		"InstalledSourceRevision": dbus.MakeVariant("git-hash"),
	})

	err = svcObject.Call("org.freedesktop.DBus.Properties.GetAll", 0, "org.oniroproject.sysota1.Potato").Store(&propMap)
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.InterfaceNotFound`)

	// Read only properties.

	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Service", "Maker", "potato").Store()
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.ReadOnly`)

	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Service", "Model", "potato").Store()
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.ReadOnly`)

	// Write access.
	//
	// TODO(zyga): observe the property changed signal.
	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Service", "ServerURL", "https://netota.example.org/").Store()
	c.Assert(err, IsNil)
	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "ServerURL").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("https://netota.example.org/"))

	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Service", "SystemImagePackage", "french-fries-os").Store()
	c.Assert(err, IsNil)
	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "SystemImagePackage").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("french-fries-os"))

	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Service", "Stream", "latest/stable").Store()
	c.Assert(err, IsNil)
	err = svcObject.Call("org.freedesktop.DBus.Properties.Get", 0, "org.oniroproject.sysota1.Service", "Stream").Store(&propValue)
	c.Assert(err, IsNil)
	c.Check(propValue, Equals, dbus.MakeVariant("latest/stable"))

	// Invalid access.

	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Service", "Stream", 42).Store()
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.InvalidArg`)

	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Service", "Potato", "value").Store()
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.PropertyNotFound`)

	err = svcObject.Call("org.freedesktop.DBus.Properties.Set", 0, "org.oniroproject.sysota1.Potato", "Property", "value").Store()
	c.Assert(err, ErrorMatches, `org.freedesktop.DBus.Properties.Error.InterfaceNotFound`)
}

func (s *serviceSuite) TestIsIdle(c *C) {
	s.EnsureService(c)

	// Service without operations is idle.
	c.Check(s.svc.IsIdle(), Equals, true)
}

func (s *serviceSuite) TestGetManagedObjects(c *C) {
	s.EnsureService(c)

	svcObject := s.conn.Object("org.oniroproject.sysota1", "/org/oniroproject/sysota1/Service")

	var objs map[dbus.ObjectPath]map[string]map[string]dbus.Variant

	err := svcObject.Call("org.freedesktop.DBus.ObjectManager.GetManagedObjects", 0).Store(&objs)
	c.Assert(err, IsNil)
	c.Check(objs, DeepEquals, map[dbus.ObjectPath]map[string]map[string]dbus.Variant{})
}

func (s *serviceSuite) TestBootMode(c *C) {
	s.EnsureService(c)

	c.Check(s.svc.BootMode(), Equals, boot.Normal)
	// TODO(zyga): check that PropertiesChanged signal was *not* sent.
	c.Check(s.svc.SetBootMode(boot.Normal, false), IsNil)
	// TODO(zyga): check that PropertiesChanged signal was sent.
	c.Check(s.svc.SetBootMode(boot.Try, false), IsNil)
	c.Check(s.svc.BootMode(), Equals, boot.Try)
}
