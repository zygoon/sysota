// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package raucboothosted implements D-Bus interface exposing RAUC custom boot
// handler provided as boothandler.Interface.
package raucboothosted

import (
	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"

	"gitlab.com/zygoon/sysota/boot"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/rauc/boothandler"
	"gitlab.com/zygoon/sysota/rauc/boothandler/bootdbus/spec"
)

const (
	slotArgName  = "slot"
	stateArgName = "state"
)

// HostedService exposes RAUC install handler over D-Bus.
type HostedService struct {
	iface boothandler.Interface
}

// New returns a hosted service with the given boot handler.
func New(iface boothandler.Interface) *HostedService {
	return &HostedService{iface: iface}
}

// JoinServiceHost integrates with dbusutil.ServiceHost.
func (hs *HostedService) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	ifaceReg := reg.Object(svcspec.ObjectPath).Interface(spec.InterfaceName)

	ifaceReg.SetMethods(map[string]interface{}{
		spec.GetPrimary: hs.GetPrimary,
		spec.SetPrimary: hs.SetPrimary,
		spec.GetState:   hs.GetState,
		spec.SetState:   hs.SetState,
	})

	ifaceReg.SetIntrospection(&introspect.Interface{
		Name: spec.InterfaceName,
		Methods: []introspect.Method{
			{
				Name: spec.GetPrimary,
				Args: []introspect.Arg{
					{Name: slotArgName, Type: "s", Direction: dbusutil.Out},
				},
			},
			{
				Name: spec.SetPrimary,
				Args: []introspect.Arg{
					{Name: slotArgName, Type: "s", Direction: dbusutil.In},
				},
			},
			{
				Name: spec.GetState,
				Args: []introspect.Arg{
					{Name: slotArgName, Type: "s", Direction: dbusutil.In},
					{Name: stateArgName, Type: "s", Direction: dbusutil.Out},
				},
			},
			{
				Name: spec.SetState,
				Args: []introspect.Arg{
					{Name: slotArgName, Type: "s", Direction: dbusutil.In},
					{Name: stateArgName, Type: "s", Direction: dbusutil.In},
				},
			},
		},
	})

	return nil
}

// GetPrimary maps boothandler.Interface.PrimarySlot to D-Bus.
func (hs *HostedService) GetPrimary() (slotName string, dbusErr *dbus.Error) {
	slot, err := hs.iface.PrimarySlot()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	return slot.String(), nil
}

// SetPrimary maps boothandler.Interface.SetPrimarySlot to D-Bus.
func (hs *HostedService) SetPrimary(slotName string) (dbusErr *dbus.Error) {
	var slot boot.Slot

	if err := slot.UnmarshalText([]byte(slotName)); err != nil {
		return dbus.MakeFailedError(err)
	}

	if err := hs.iface.SetPrimarySlot(slot); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// GetState maps boothandler.Interface.SlotState to D-Bus.
func (hs *HostedService) GetState(slotName string) (bootState string, dbusErr *dbus.Error) {
	var slot boot.Slot

	if err := slot.UnmarshalText([]byte(slotName)); err != nil {
		return "", dbus.MakeFailedError(err)
	}

	state, err := hs.iface.SlotState(slot)
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	return state.String(), nil
}

// SetState maps boothandler.Interface.SetSlotState to D-Bus.
func (hs *HostedService) SetState(slotName string, stateName string) (dbusErr *dbus.Error) {
	var slot boot.Slot

	if err := slot.UnmarshalText([]byte(slotName)); err != nil {
		return dbus.MakeFailedError(err)
	}

	var state boot.SlotState

	if err := state.UnmarshalText([]byte(stateName)); err != nil {
		return dbus.MakeFailedError(err)
	}

	if err := hs.iface.SetSlotState(slot, state); err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}
