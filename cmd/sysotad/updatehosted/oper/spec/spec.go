// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package spec

import (
	"fmt"
)

const (
	// Interface is the name of the Operation D-Bus interface.
	Interface = "org.oniroproject.sysota1.Operation"

	// KindProperty is the name of the Kind D-Bus property.
	KindProperty = "Kind"
	// StateProperty is the name of the State D-Bus property.
	StateProperty = "State"
	// ProgressProperty is the name of the Progress D-Bus property.
	ProgressProperty = "Progress"
	// ErrorProperty is the name of the Error D-Bus property.
	ErrorProperty = "Error"
)

// Kind denotes the kind operation.
type Kind string

const (
	// UnknownOperation identifies operation kind not know to the client.
	UnknownOperation Kind = "unknown"
	// SystemUpdate identifies system image update operations.
	SystemUpdate Kind = "system-update"
)

// String returns the textual representation of the operation kind.
func (kind Kind) String() string {
	return string(kind)
}

// Clamp returns a known operation type or UnknownOperation.
func (kind Kind) Clamp() Kind {
	switch kind {
	case UnknownOperation, SystemUpdate:
		return kind
	default:
		return UnknownOperation
	}
}

// State denotes the state of the operation.
type State int

const (
	// UnknownState identifies operations in unknown state.
	UnknownState State = iota
	// Pending identifies operations which are not yet running.
	Pending
	// Running identifies operations which are actively executing.
	Running
	// Done identifies operations which have finished executing.
	Done
)

// String returns the textual representation of the operation state.
func (st State) String() string {
	switch st {
	case Pending:
		return "pending"
	case Running:
		return "running"
	case Done:
		return "done"
	default:
		return "unknown"
	}
}

// Clamp returns a known operation state or UnknownOperation.
func (st State) Clamp() State {
	switch st {
	case UnknownState, Pending, Running, Done:
		return st
	default:
		return UnknownState
	}
}

// Progress denotes the progress percentage of the operation.
type Progress float64

// String formats progress as a percentage.
func (p Progress) String() string {
	return fmt.Sprintf("%.0f%%", float64(p))
}

// Clamp returns the sensible value of progress or the nearest limit.
func (p Progress) Clamp() Progress {
	switch {
	case p < 0:
		return 0
	case p > 100:
		return 100
	default:
		return p
	}
}
