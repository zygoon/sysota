// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package updatehosted_test

import (
	"context"
	"encoding/xml"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	"gitlab.com/zygoon/netota"
	"gitlab.com/zygoon/netota/pkg/server"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/cmd/sysotad"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted"
	"gitlab.com/zygoon/sysota/ota"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
)

func Test(t *testing.T) { TestingT(t) }

type hostedServiceSuite struct {
	dbustest.Suite

	sh dbusutil.ServiceHost
	hs updatehosted.HostedService

	cfg *ota.Config
	st  *ota.SystemState

	cg        ota.ConfigGuard
	sg        ota.StateGuard
	conn      *dbus.Conn
	svcObject dbus.BusObject
}

var _ = Suite(&hostedServiceSuite{})

func (s *hostedServiceSuite) SetUpSuite(c *C) {
	s.Suite.SetUpSuite(c)

	conn, err := dbusutil.SystemBus()
	c.Assert(err, IsNil)

	s.svcObject = conn.Object("org.oniroproject.sysota1", "/org/oniroproject/sysota1/Service")

	s.conn = conn

	s.cfg = &ota.Config{}
	s.st = &ota.SystemState{}

	c.Assert(s.cg.BindAndLoad(s), IsNil)
	c.Assert(s.sg.BindAndLoad(s), IsNil)

	s.hs.Init(&s.cg, &s.sg)
	s.sh.Init(conn)
	s.sh.AddHostedService(&s.hs)

	err = s.sh.Export()
	c.Assert(err, IsNil)

	err = sysotad.RequestBusName(conn)
	c.Assert(err, IsNil)
}

func (s *hostedServiceSuite) SetUpTest(c *C) {
	err := s.sg.SetState(&ota.SystemState{})
	c.Assert(err, IsNil)

	err = s.cg.SetConfig(&ota.Config{})
	c.Assert(err, IsNil)

	s.cfg = nil
	s.st = nil
}

func (s *hostedServiceSuite) LoadConfig() (*ota.Config, error) {
	if s.cfg == nil {
		panic("s.cfg is nil")
	}

	return s.cfg, nil
}

func (s *hostedServiceSuite) SaveConfig(cfg *ota.Config) error {
	s.cfg = cfg

	return nil
}

func (s *hostedServiceSuite) LoadState() (*ota.SystemState, error) {
	if s.st == nil {
		panic("s.st is nil")
	}

	return s.st, nil
}

func (s *hostedServiceSuite) SaveState(st *ota.SystemState) error {
	s.st = st

	return nil
}

// queryOption influences how Query family of functions behave.
type queryOption func(*query)

// QueryOption is a dummy method identifying query option types.
func (queryOption) QueryOption() {}

type query struct {
	pkgName    string
	streamName string
}

func newQuery(opts []netota.QueryOption) *query {
	q := &query{}

	for _, opt := range opts {
		opt.(queryOption)(q)
	}

	return q
}

func (q *query) visitPackages(repo *TestRepo, v func(pkg *netota.Package)) {
	if q.pkgName != "" {
		if pkg, ok := repo.pkgs[q.pkgName]; ok {
			v(pkg)
		}

		return
	}

	for _, pkg := range repo.pkgs {
		v(pkg)
	}
}

func (q *query) visitStreams(repo *TestRepo, v func(pkg *netota.Package, st *netota.Stream)) {
	q.visitPackages(repo, func(pkg *netota.Package) {
		if q.streamName != "" {
			if st, ok := pkg.Streams[q.streamName]; ok {
				v(pkg, st)
			}

			return
		}

		for _, st := range pkg.Streams {
			v(pkg, st)
		}
	})
}

func (q *query) visitArchives(repo *TestRepo, v func(pkg *netota.Package, st *netota.Stream, ar *netota.Archive)) {
	q.visitStreams(repo, func(pkg *netota.Package, st *netota.Stream) {
		for _, ar := range st.Archives {
			v(pkg, st, ar)
		}
	})
}

// PackageName causes Query to select only a particular package.
func (r *TestRepo) PackageName(name string) netota.QueryOption {
	return queryOption(func(q *query) {
		q.pkgName = name
	})
}

// StreamName causes Query to select only a particular stream.
func (r *TestRepo) StreamName(name string) netota.QueryOption {
	return queryOption(func(q *query) {
		q.streamName = name
	})
}

type TestRepo struct {
	pkgs map[string]*netota.Package
}

func NewTestRepo(pkgs map[string]*netota.Package) *TestRepo {
	r := &TestRepo{pkgs: pkgs}
	r.fixup()

	return r
}

// fixup ensures that packages and streams have names matching their map keys.
func (r *TestRepo) fixup() {
	for pkgName, pkg := range r.pkgs {
		pkg.Name = pkgName

		for streamName, st := range pkg.Streams {
			st.Name = streamName
			st.PackageName = pkgName

			if st.UpdatedOnSec == 0 {
				st.UpdatedOnSec = time.Date(1, time.January, 1, 0, 0, 0, 0, time.UTC).Unix()
			}
		}
	}
}

// QueryPackages returns a subset of matching packages.
//
// Use PackageName query option to select a single package. Packages do not
// carry stream information, perform a separate QueryStreams query if required.
func (r *TestRepo) QueryPackages(ctx context.Context, opts ...netota.QueryOption) (result []*netota.Package) {
	newQuery(opts).visitPackages(r, func(pkg *netota.Package) {
		pkgCopy := new(netota.Package)
		*pkgCopy = *pkg
		pkgCopy.Streams = nil

		result = append(result, pkgCopy)
	})

	return result
}

// QueryStreams returns a subset of matching streams.
//
// Use PackageName and StreamName query option to select a single package or a
// single stream. Streams do not carry archive information, perform a separate
// QueryArchives query if required.
func (r *TestRepo) QueryStreams(ctx context.Context, opts ...netota.QueryOption) (result []*netota.Stream) {
	newQuery(opts).visitStreams(r, func(pkg *netota.Package, st *netota.Stream) {
		stCopy := new(netota.Stream)
		*stCopy = *st
		stCopy.Archives = nil

		result = append(result, stCopy)
	})

	return result
}

// QueryArchives returns a subset of matching archives.
//
// Use PackageName and StreamName query option to select a single package or a
// single stream.
func (r *TestRepo) QueryArchives(ctx context.Context, opts ...netota.QueryOption) (result []*netota.Archive) {
	newQuery(opts).visitArchives(r, func(pkg *netota.Package, st *netota.Stream, ar *netota.Archive) {
		arCopy := new(netota.Archive)
		*arCopy = *ar

		result = append(result, arCopy)
	})

	return result
}

func (s *hostedServiceSuite) TestGetStreamsHappy(c *C) {
	srv := httptest.NewServer(server.NewHandler(NewTestRepo(map[string]*netota.Package{
		"potato-os": {
			Streams: map[string]*netota.Stream{
				"latest/stable": {
					PackageVersion: "1.0.1",
					SourceRevision: "sha-foo",
					SourceAliases:  []string{"v1.0.1"},
					UpdatedOnSec:   time.Date(2022, time.January, 5, 0, 0, 0, 0, time.UTC).Unix(),
				},
				"latest/nightly": {
					PackageVersion: "1.1.0~gitxxx", // cspell:disable-line
					SourceRevision: "sha-bar",
					UpdatedOnSec:   time.Date(2022, time.January, 12, 0, 0, 0, 0, time.UTC).Unix(),
				},
				"old/stable": {
					IsClosed:   true,
					ReplacedBy: "latest/stable",
				},
			},
		},
	})))
	defer srv.Close()

	c.Assert(s.svcObject.SetProperty("org.oniroproject.sysota1.Service.ServerURL", srv.URL), IsNil)
	c.Assert(s.svcObject.SetProperty("org.oniroproject.sysota1.Service.SystemImagePackage", "potato-os"), IsNil)

	var streams map[string]netota.Stream
	err := s.svcObject.Call("org.oniroproject.sysota1.Service.GetStreams", 0).Store(&streams)
	c.Assert(err, IsNil)

	c.Check(streams, DeepEquals, map[string]netota.Stream{
		"latest/nightly": {
			Name:           "latest/nightly",
			PackageName:    "potato-os",
			PackageVersion: "1.1.0~gitxxx", // cspell:disable-line
			SourceRevision: "sha-bar",
			SourceAliases:  []string{},
			UpdatedOnSec:   time.Date(2022, time.January, 12, 0, 0, 0, 0, time.UTC).Unix(),
		},
		"latest/stable": {
			Name:           "latest/stable",
			PackageName:    "potato-os",
			PackageVersion: "1.0.1",
			SourceRevision: "sha-foo",
			SourceAliases:  []string{"v1.0.1"},
			UpdatedOnSec:   time.Date(2022, time.January, 5, 0, 0, 0, 0, time.UTC).Unix(),
		},
		"old/stable": {
			Name:          "old/stable",
			PackageName:   "potato-os",
			IsClosed:      true,
			ReplacedBy:    "latest/stable",
			SourceAliases: []string{},
			UpdatedOnSec:  time.Date(1, time.January, 1, 0, 0, 0, 0, time.UTC).Unix(),
		},
	})
}

func (s *hostedServiceSuite) TestGetStreamsNoSuchPackage(c *C) {
	srv := httptest.NewServer(server.NewHandler(NewTestRepo(nil)))
	defer srv.Close()

	c.Assert(s.svcObject.SetProperty("org.oniroproject.sysota1.Service.ServerURL", srv.URL), IsNil)
	c.Assert(s.svcObject.SetProperty("org.oniroproject.sysota1.Service.SystemImagePackage", "potato-os"), IsNil)

	var streams map[string]netota.Stream
	err := s.svcObject.Call("org.oniroproject.sysota1.Service.GetStreams", 0).Store(&streams)
	c.Assert(err, DeepEquals, dbus.Error{
		Name: "org.freedesktop.DBus.Error.Failed",
		Body: []interface{}{"cannot retrieve update streams: cannot query package: unexpected status code: 404"},
	})
}

func (s *hostedServiceSuite) TestIntrospection(c *C) {
	obj := s.conn.Object(svcspec.BusName, svcspec.ObjectPath)
	node, err := introspect.Call(obj)
	c.Assert(err, IsNil)
	c.Check(node, DeepEquals, &introspect.Node{
		XMLName: xml.Name{Space: "", Local: "node"},
		Name:    "/org/oniroproject/sysota1/Service",
		Interfaces: []introspect.Interface{{
			// ...
			Name: "org.oniroproject.sysota1.Service",
			Methods: []introspect.Method{{
				Name: "GetStreams",
				Args: []introspect.Arg{
					{Name: "streams", Type: "a{s(sbsxsssas)}", Direction: "out"}}}, {
				Name: "Update",
				Args: []introspect.Arg{
					{Name: "hints", Type: "a{sv}", Direction: "in"},
					{Name: "operation", Type: "o", Direction: "out"}}}},
			Properties: []introspect.Property{{
				Name:   "InstalledPackageName",
				Type:   "s",
				Access: "read",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "true"}}}, {
				Name:   "InstalledPackageVersion",
				Type:   "s",
				Access: "read",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "true"}}}, {
				Name:   "InstalledSourceRevision",
				Type:   "s",
				Access: "read",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "true"}}}, {

				Name:   "Maker",
				Type:   "s",
				Access: "read",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "const"}}}, {
				Name:   "Model",
				Type:   "s",
				Access: "read",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "const"}}}, {

				Name:   "ServerURL",
				Type:   "s",
				Access: "readwrite",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "true"}}}, {
				Name:   "Stream",
				Type:   "s",
				Access: "readwrite",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "true"}}}, {
				Name:   "SystemImagePackage",
				Type:   "s",
				Access: "readwrite",
				Annotations: []introspect.Annotation{
					{Name: "org.freedesktop.DBus.Property.EmitsChangedSignal", Value: "true"}}}}}, {
			// ...
			Name: "org.freedesktop.DBus.ObjectManager",
			Methods: []introspect.Method{{
				Name: "GetManagedObjects",
				Args: []introspect.Arg{
					{Name: "objpath_interfaces_and_properties", Type: "a{oa{sa{sv}}}", Direction: "out"}}}},
			Signals: []introspect.Signal{
				{Name: "InterfacesAdded", Args: []introspect.Arg{
					{Name: "object_path", Type: "o", Direction: "out"},
					{Name: "interfaces_and_properties", Type: "a{sa{sv}}", Direction: "out"}}}, {
					Name: "InterfacesRemoved",
					Args: []introspect.Arg{
						{Name: "object_path", Type: "o", Direction: "out"},
						{Name: "interfaces", Type: "as", Direction: "out"}}}}}, {
			// ...
			Name: "org.freedesktop.DBus.Properties",
			Methods: []introspect.Method{{
				Name: "Get",
				Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "in"},
					{Name: "property", Type: "s", Direction: "in"},
					{Name: "value", Type: "v", Direction: "out"}}}, {
				Name: "GetAll",
				Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "in"},
					{Name: "props", Type: "a{sv}", Direction: "out"}}}, {
				Name: "Set",
				Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "in"},
					{Name: "property", Type: "s", Direction: "in"},
					{Name: "value", Type: "v", Direction: "in"}}}},
			Signals: []introspect.Signal{{
				Name: "PropertiesChanged",
				Args: []introspect.Arg{
					{Name: "interface", Type: "s", Direction: "out"},
					{Name: "changed_properties", Type: "a{sv}", Direction: "out"},
					{Name: "invalidates_properties", Type: "as", Direction: "out"}}}}}, {
			// ...
			Name: "org.freedesktop.DBus.Introspectable",
			Methods: []introspect.Method{{
				Name: "Introspect",
				Args: []introspect.Arg{
					{Name: "out", Type: "s", Direction: "out"}}}}}}})
}
