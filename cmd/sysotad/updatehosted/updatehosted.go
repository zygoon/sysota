// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package updatehosted implements the online update component of sysotad.
package updatehosted

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"hash"
	"io"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"runtime"
	"sort"
	"sync"
	"sync/atomic"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	"github.com/godbus/dbus/v5/prop"
	"gitlab.com/zygoon/netota"

	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted/oper"
	opspec "gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted/oper/spec"
	"gitlab.com/zygoon/sysota/ota"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/objmgr"
	"gitlab.com/zygoon/sysota/rauc"
)

const (
	// InterfaceName is the name of the SystemOTA Service interface.
	// TODO(zyga): re-think the name.
	InterfaceName = "org.oniroproject.sysota1.Service"

	httpScheme  = "http"
	httpsScheme = "https"
)

// IntrospectData is the D-Bus introspection data for Service interface of the service object.
//
// It is provided explicitly without using reflection for a bit more control
// over the provided data, given that we implement the properties interface
// ourselves.
var IntrospectData = introspect.Interface{
	Name: InterfaceName,
	Methods: []introspect.Method{
		{
			Name: svcspec.GetStreamsMethod,
			Args: []introspect.Arg{
				{
					Name:      "streams",
					Type:      dbus.SignatureOfType(reflect.TypeOf(map[string]netota.Stream{})).String(),
					Direction: dbusutil.Out,
				},
			},
		},
		{
			Name: svcspec.UpdateMethod,
			Args: []introspect.Arg{
				{
					Name:      "hints",
					Type:      "a{sv}",
					Direction: dbusutil.In,
				},
				{
					Name:      "operation",
					Type:      "o",
					Direction: dbusutil.Out,
				},
			},
		},
		// Property introspection data is generated automatically.
	},
}

// HostedService provides functions for downloading and applying updates.
//
// Maker is a near-arbitrary string that identifier the device maker.
// Model is a near-arbitrary string that is selected by the device maker.
// Stream is a path-like string that identifiers which of perhaps several versions,
// branches or stability levels to use.
//
// Available updates are specific to a given (maker,model,stream) tuple.
type HostedService struct {
	m     sync.RWMutex
	conn  *dbus.Conn
	cg    *ota.ConfigGuard
	sg    *ota.StateGuard
	props *dbusutil.InterfaceProperties
	ops   []*oper.Operation

	lastOperationID int
}

func (hs *HostedService) Init(cg *ota.ConfigGuard, sg *ota.StateGuard) {
	hs.cg = cg
	hs.sg = sg
}

type stateNotifierKey struct{}

// JoinServiceHost integrates with dbusutil.ServiceHost.
func (hs *HostedService) JoinServiceHost(reg dbusutil.ServiceRegistration) error {
	// Remember connection to send signals.
	hs.conn = reg.DBusConn()

	ifaceReg := reg.Object(svcspec.ObjectPath).Interface(InterfaceName)
	ifaceReg.SetMethods(map[string]interface{}{
		svcspec.GetStreamsMethod: hs.GetStreams,
		svcspec.UpdateMethod:     hs.Update,
	})

	// Snapshot of the config and state.
	cfgSnapshot := hs.cg.Config()
	stSnapshot := hs.sg.State()

	// Remember properties to have access later.
	hs.props = ifaceReg.SetProperties(map[string]*prop.Prop{
		svcspec.MakerProperty: {
			Value: cfgSnapshot.DeviceMaker,
			Emit:  prop.EmitConst,
		},
		svcspec.ModelProperty: {
			Value: cfgSnapshot.DeviceModel,
			Emit:  prop.EmitConst,
		},

		svcspec.StreamProperty: {
			Value:    cfgSnapshot.UpdateStream,
			Writable: true,
			Emit:     prop.EmitTrue,
			Callback: func(ch *prop.Change) *dbus.Error {
				streamName, ok := ch.Value.(string)
				if !ok {
					return &dbus.ErrMsgInvalidArg
				}

				// TODO: validation.

				err := hs.cg.AlterConfig(func(cfg *ota.Config) (bool, error) {
					if cfg.UpdateStream != streamName {
						cfg.UpdateStream = streamName
						return true, nil
					}

					return false, nil
				})

				if err != nil {
					return dbus.MakeFailedError(err)
				}

				return nil
			},
		},
		svcspec.ServerURLProperty: {
			Value:    cfgSnapshot.UpdateServerURL,
			Writable: true,
			Emit:     prop.EmitTrue,
			Callback: func(ch *prop.Change) *dbus.Error {
				serverURL, ok := ch.Value.(string)
				if !ok {
					return &dbus.ErrMsgInvalidArg
				}

				// TODO: validation.

				err := hs.cg.AlterConfig(func(cfg *ota.Config) (bool, error) {
					if cfg.UpdateServerURL != serverURL {
						cfg.UpdateServerURL = serverURL
						return true, nil
					}

					return false, nil
				})

				if err != nil {
					return dbus.MakeFailedError(err)
				}

				return nil
			},
		},
		svcspec.SystemImagePackageProperty: {
			Value:    cfgSnapshot.UpdateSystemImagePackage,
			Writable: true,
			Emit:     prop.EmitTrue,
			Callback: func(ch *prop.Change) *dbus.Error {
				pkgName, ok := ch.Value.(string)
				if !ok {
					return &dbus.ErrMsgInvalidArg
				}

				// TODO: validation.

				err := hs.cg.AlterConfig(func(cfg *ota.Config) (bool, error) {
					if cfg.UpdateSystemImagePackage != pkgName {
						cfg.UpdateSystemImagePackage = pkgName
						return true, nil
					}

					return false, nil
				})

				if err != nil {
					return dbus.MakeFailedError(err)
				}

				return nil
			},
		},

		svcspec.InstalledPackageNameProperty: {
			Value: stSnapshot.InstalledPackageName,
			Emit:  prop.EmitTrue,
		},
		svcspec.InstalledPackageVersionProperty: {
			Value: stSnapshot.InstalledPackageVersion,
			Emit:  prop.EmitTrue,
		},
		svcspec.InstalledSourceRevisionProperty: {
			Value: stSnapshot.InstalledSourceRevision,
			Emit:  prop.EmitTrue,
		},
	})

	// Register observer to fire prop changes for state changes.
	hs.sg.RegisterObserver(stateNotifierKey{}, func(st *ota.SystemState) error {
		_ = hs.props.Set(svcspec.InstalledPackageNameProperty, dbus.MakeVariant(st.InstalledPackageName))
		_ = hs.props.Set(svcspec.InstalledPackageVersionProperty, dbus.MakeVariant(st.InstalledPackageVersion))
		_ = hs.props.Set(svcspec.InstalledSourceRevisionProperty, dbus.MakeVariant(st.InstalledSourceRevision))

		return nil
	})

	ifaceReg.SetIntrospection(&IntrospectData)

	ifaceReg = reg.Object(svcspec.ObjectPath).Interface(objmgr.InterfaceName)
	ifaceReg.SetMethods(map[string]interface{}{
		"GetManagedObjects": hs.GetManagedObjects,
	})
	ifaceReg.SetIntrospection(&objmgr.IntrospectData)

	return nil
}

// IsIdle returns true if the OTA service is idle and can be shut down.
func (hs *HostedService) IsIdle() bool {
	hs.m.RLock()
	defer hs.m.RUnlock()

	// TODO: idle checker should ignore operations which are completed or failed.
	return len(hs.ops) == 0
}

// GetStreams asks the update server available streams.
func (hs *HostedService) GetStreams() (map[string]netota.Stream, *dbus.Error) {
	hs.m.Lock()
	defer hs.m.Unlock()

	cli, err := hs.client()
	if err != nil {
		return nil, dbus.MakeFailedError(err)
	}

	pkgName, err := hs.nonEmptySystemImagePackageName()
	if err != nil {
		return nil, dbus.MakeFailedError(err)
	}

	pkg, err := cli.Package(hs.conn.Context(), pkgName)
	if err != nil {
		return nil, dbus.MakeFailedError(fmt.Errorf("cannot retrieve update streams: %w", err))
	}

	// D-Bus return types cannot handle map-to-pointer types.
	// Translate map[string]*netota.Stream to map[string]netota.Stream
	streams := make(map[string]netota.Stream, len(pkg.Streams))
	for _, stream := range pkg.Streams {
		streams[stream.Name] = *stream
	}

	return streams, nil
}

// Update starts and asynchronous update operation.
//
// The OTA service checks the configured update server for available updates
// and returns an object which can be used to track the progress and result.
//
// TODO: Implement this for real by calling thing from the OTA backend.
func (hs *HostedService) Update(hints map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error) {
	hs.m.Lock()
	defer hs.m.Unlock()

	// Don't attempt to update if we have a pending package name. This implies
	// that another update is in progress. On rollback this is re-set
	// automatically.
	if st := hs.sg.State(); st.PendingPackageName != "" {
		return "", dbus.MakeFailedError(errors.New("update operation in progress"))
	}

	// Get a NetOTA client to talk to the server side.
	cli, err := hs.client()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	pkgName, err := hs.nonEmptySystemImagePackageName()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	streamName, err := hs.nonEmptyStreamName()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	// Check what's published in our update stream.
	// TODO: pass constraints to avoid sending archives we would filter out.
	stream, err := cli.Stream(hs.conn.Context(), pkgName, streamName)
	if err != nil {
		return "", dbus.MakeFailedError(fmt.Errorf("cannot retrieve update archives: %w", err))
	}

	if stream.IsClosed {
		// TODO: provide a dedicated D-Bus error for this.
		return "", dbus.MakeFailedError(fmt.Errorf("cannot update from a closed stream"))
	}

	// Check if there's something to update to.
	// Note: we are never comparing versions, just package name and source revision.
	if st := hs.sg.State(); st.InstalledPackageName == stream.PackageName && st.InstalledSourceRevision == stream.SourceRevision {
		// TODO: provide a dedicated D-Bus error for this.
		return "", dbus.MakeFailedError(errors.New("system is up-to-date"))
	}

	// Pick a compatible archive.
	ar, err := hs.pickArchive(stream.Archives)
	if err != nil {
		return "", dbus.MakeFailedError(fmt.Errorf("cannot pick update archive: %w", err))
	}

	fmt.Printf("Selected update archive: %#v\n", ar)

	// Pick the best download offer.
	offer, err := hs.pickDownloadOffer(ar)
	if err != nil {
		return "", dbus.MakeFailedError(fmt.Errorf("cannot pick download offer: %w", err))
	}

	fmt.Printf("Selected download offer: %#v\n", offer)

	// Create an update operation so that clients can track changes from other
	// processes by observing the properties of this D-Bus object.
	op, err := hs.makeOp()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	// Remember the operation. We never remove those. They just go away when we
	// are done and the system reboots.
	hs.ops = append(hs.ops, op)

	// Allow the update progress to last up to one hour.
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Hour))

	op.Go(opspec.SystemUpdate, func() error {
		defer cancel()

		// Download the file and verify checksums on the fly.
		name, err := hs.downloadFile(ctx, op, ar, offer)
		if err != nil {
			return fmt.Errorf("cannot download archive: %w", err)
		}

		// Remove the update bundle when the operation finishes. RAUC mounts and
		// copies the system image during the installBundle call below. Once
		// RAUC indicates that everything is done the file is no longer
		// necessary.
		defer func() {
			_ = os.Remove(name)
		}()

		// Remember the name, version and revision of the package we are
		// attempting to install. On rollback those are all reset to empty. On
		// commit the pending values are copied over to the current values and
		// then re-set to empty.
		//
		// This is how we track what is installed.
		err = hs.sg.AlterState(func(st *ota.SystemState) (changed bool, err error) {
			_, _ = fmt.Printf("TRY: update to %s (%s), pending source revision set to %s\n",
				stream.PackageName, stream.PackageVersion, stream.SourceRevision)

			changed = changed || st.PendingPackageName != stream.PackageName
			st.PendingPackageName = stream.PackageName
			changed = changed || st.PendingPackageVersion != stream.PackageVersion
			st.PendingPackageVersion = stream.PackageVersion
			changed = changed || st.PendingSourceRevision != stream.SourceRevision
			st.PendingSourceRevision = stream.SourceRevision

			return changed, nil
		})
		if err != nil {
			return fmt.Errorf("cannot set pending update properties: %w", err)
		}

		// Ask RAUC to install the bundle. This blocks until the operation is
		// completed, successfully or otherwise.
		return hs.installBundle(ctx, op, name)
	})

	return op.ObjectPath(), nil
}

// ByteCounter counts the number of bytes written to it.
type ByteCounter struct {
	total int64
	c     chan int64
}

// NewByteCounter returns a new byte counter.
func NewByteCounter() *ByteCounter {
	return &ByteCounter{c: make(chan int64)}
}

// Totals returns an un-buffered channel for reading the total number of bytes.
func (w *ByteCounter) Totals() <-chan int64 {
	return w.c
}

// Write increments the counter by the length of the slice.
//
// If TotalCh has a pending receiver, the updated total is sent. If no receiver
// is pending the change is ignored. This is meant to allow slow receivers (e.g.
// a user interface thread) to not stall the write operations.
func (w *ByteCounter) Write(p []byte) (n int, err error) {
	total := atomic.AddInt64(&w.total, int64(len(p)))

	select {
	case w.c <- total:
	default:
		// Nobody is receiving, ignore.
		break
	}

	return len(p), nil
}

// Total returns the total number of bytes written.
func (w *ByteCounter) Total() int64 {
	return atomic.LoadInt64(&w.total)
}

// Close closes the totals channel.
//
// No more writes are possible.
func (w *ByteCounter) Close() error {
	close(w.c)

	return nil
}

func (hs *HostedService) downloadFile(ctx context.Context, op *oper.Operation, ar *netota.Archive, offer netota.DownloadOffer) (name string, err error) {
	// Prepare a GET request with a context.
	// TODO: set the User Agent once sysotad knows its version.
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, offer.URL, nil)
	if err != nil {
		return "", fmt.Errorf("cannot prepare download request: %w", err)
	}

	// GET the download URL.
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("cannot perform download request: %w", err)
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("bad http status: %s", resp.Status)
	}
	// XXX: should we check content length before reading?

	// We will be streaming the content to a number of writers. At least one for
	// the temporary file, one for progress observer and then one for each
	// checksum we want to compute.
	writers := make([]io.Writer, 0, len(ar.ArchiveChecksums)+2)

	// Make a temporary file.
	f, err := os.CreateTemp("", ar.ArchiveName)
	if err != nil {
		return "", fmt.Errorf("cannot create temporary file: %w", err)
	}

	defer func() {
		_ = f.Close()

		// If something failed then get rid of the file.
		if err != nil {
			_ = os.Remove(f.Name())
		}
	}()

	writers = append(writers, f)

	// Make a progress writer for tracking how much was downloaded.
	p := NewByteCounter()

	defer func() {
		_ = p.Close()
	}()

	writers = append(writers, p)

	// As data is being written to the progress writer it will send the new
	// total to the channel. Pick them up and set progress percentage for
	// clients to see.
	go func() {
		last := time.Now()

		// Send one final update after everything is over. The loop below skips
		// some updates and the progress writer also skips some updates if the
		// loop below is busy. This ensures that the final percentage will not
		// be stuck an unexpected value.
		defer func() {
			op.SetProgress(opspec.Progress(100.0 * p.Total() / ar.ArchiveSize))
		}()

		for {
			select {
			case <-ctx.Done():
				return
			case total := <-p.Totals():
				// Rate-limit updates to one per second.
				if time.Now().After(last.Add(1 * time.Second)) {
					op.SetProgress(opspec.Progress(100.0 * total / ar.ArchiveSize))

					last = time.Now()
				}
			}
		}
	}()

	// Create a hasher for each checksum we know about.
	hashers := make(map[netota.ChecksumType]hash.Hash, len(ar.ArchiveChecksums))

	for hashType := range ar.ArchiveChecksums {
		switch hashType {
		case netota.Sha256:
			hasher := sha256.New()
			hashers[hashType] = hasher
			writers = append(writers, hasher)
		}
	}

	// Copy the response body to both the hasher and the temporary file.
	n, err := io.Copy(io.MultiWriter(writers...), resp.Body)
	if err != nil {
		return "", fmt.Errorf("cannot save: %w", err)
	}

	if n != ar.ArchiveSize {
		return "", fmt.Errorf("archive size mismatch: expected %d, got %d", ar.ArchiveSize, n)
	}

	// Verify checksums of each type.
	for hashType, expected := range ar.ArchiveChecksums {
		actual := hex.EncodeToString(hashers[hashType].Sum(nil))
		if actual != expected {
			return "", fmt.Errorf("%s checksum mismatch, expected %s, actual: %s", hashType, expected, actual)
		}
	}

	return f.Name(), nil
}

func (hs *HostedService) installBundle(ctx context.Context, op *oper.Operation, bundlePath string) error {
	raucObj := hs.conn.Object(rauc.BusName, rauc.ObjectPath)
	matchOpts := []dbus.MatchOption{
		dbus.WithMatchSender(rauc.BusName),
	}

	// Observe all signals sent by RAUC.
	if err := hs.conn.AddMatchSignal(matchOpts...); err != nil {
		return err
	}

	defer func() {
		_ = hs.conn.RemoveMatchSignal(matchOpts...)
	}()

	// TODO: watch for NameOwnerLost to know if RAUC dies.

	// Relay D-Bus signals to this channel.
	sigCh := make(chan *dbus.Signal)
	defer close(sigCh)

	hs.conn.Signal(sigCh)
	defer hs.conn.RemoveSignal(sigCh)

	// Call the InstallBundle method. This doesn't block. The process continues asynchronously.
	if err := raucObj.Call(rauc.Interface+"."+rauc.InstallBundleMethod, 0, bundlePath, map[string]dbus.Variant{}).Store(); err != nil {
		return err
	}

	// Keep watching signals from RAUC until updates arrive.
	if exitCode := hs.observeUpdate(ctx, op, sigCh); exitCode != 0 {
		// If rauc provided non-zero exit code then check last error property.
		errMsg, err := raucObj.GetProperty(rauc.Interface + "." + rauc.LastErrorProperty)
		if err != nil {
			return err
		}

		// If last error message is non-empty then RAUC has failed.
		if msg := errMsg.Value().(string); msg != "" {
			return fmt.Errorf("rauc error: %s", msg)
		}
	}

	return nil
}

func (hs *HostedService) observeUpdate(ctx context.Context, op *oper.Operation, sigCh <-chan *dbus.Signal) (exitCode int) {
	for {
		select {
		case <-ctx.Done():
			return
		case sig := <-sigCh:
			if sig == nil {
				break
			}

			switch sig.Name {
			// TODO: once we observe NameOwnerLost react to RAUC quitting.
			//
			// Pay attention to how we communicate the exit code as a non-zero
			// return from this function causes a RAUC property read in the
			// caller, which may re-start RAUC after a crash.
			case rauc.Interface + "." + rauc.CompletedSignal:
				// Decode the Completed signal and convey the exit code.
				// TODO: Move this to the RAUC package.
				// https://rauc.readthedocs.io/en/latest/reference.html#gdbus-signal-de-pengutronix-rauc-installer-completed
				if len(sig.Body) == 1 {
					if exitCode, ok := sig.Body[0].(int32); ok {
						return int(exitCode)
					}
				}

				// In case the signal is malformed.
				return 1

			case dbusutil.PropertiesInterface + "." + dbusutil.PropertiesChangedSignal:
				// TODO: Write a generic extractor for PropertiesChanged.
				if iface, ok := sig.Body[0].(string); ok && iface == rauc.Interface {
					// Progress value has signature (isi) and encodes the tuple (percentage, message, level).
					// https://rauc.readthedocs.io/en/latest/using.html#sec-processing-progress
					if updated, ok := sig.Body[1].(map[string]dbus.Variant); ok {
						if prog, ok := updated[rauc.ProgressProperty]; ok {
							if fields, ok := prog.Value().([]interface{}); ok {
								if len(fields) == 3 {
									if percentage, ok := fields[0].(int32); ok {
										op.SetProgress(opspec.Progress(percentage))
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

func (hs *HostedService) compatibleArch() netota.Architecture {
	if arch := hs.cg.Config().CompatibleArch; arch != "" {
		return netota.Architecture(arch)
	}

	// See output from "go tool dist list" on Go 1.17 or newer for reference.
	switch runtime.GOARCH {
	case "386":
		return netota.X86
	case "amd64":
		return netota.X86_64
	case "arm":
		return netota.Arm
	case "arm64":
		return netota.Aarch64
	case "risc":
		return netota.RiscV32
	case "riscv64":
		return netota.RiscV64
	}

	panic("unsupported architecture")
}

func (hs *HostedService) compatibleSubArch() string {
	// TODO: explore this for 32bit Arm.
	return hs.cg.Config().CompatibleSubArch
}

func (hs *HostedService) compatibleMachine() string {
	// XXX: perhaps add a built-in default fallback?
	return hs.cg.Config().CompatibleMachine
}

// pickArchive picks a compatible archive.
//
// Archives are ruled out on incompatible archive type, archive format, lack of
// checksums, incompatible content type, content format, incompatible
// architecture, sub-architecture or machine, missing or unsupported download
// offers.
//
// In case there are multiple compatible archives a single archive must be
// marked as default, otherwise the choice cannot be resolved and no archive is
// selected.
func (hs *HostedService) pickArchive(ars []*netota.Archive) (*netota.Archive, error) {
	arch := hs.compatibleArch()
	subArch := hs.compatibleSubArch()
	mach := hs.compatibleMachine()

	var viable []*netota.Archive

	for _, ar := range ars {
		if ar.ArchiveSize == 0 {
			continue
		}

		if ar.ArchiveType != netota.RaucBundle {
			continue
		}

		if ar.ArchiveFormat != netota.RaucPlainBundle && ar.ArchiveFormat != netota.RaucVerityBundle {
			continue
		}

		// TODO: generalize this once other checksums are defined.
		if ar.ArchiveChecksums[netota.Sha256] == "" {
			continue
		}

		if ar.ContentType != netota.SysOTASystemImage {
			continue
		}

		if ar.ContentFormat != netota.SquashfsV4CompXz {
			continue
		}

		if ar.CompatibleArch != arch {
			continue
		}

		if ar.CompatibleSubArch != "" && ar.CompatibleSubArch != subArch {
			continue
		}

		if ar.CompatibleMachine != "" && ar.CompatibleMachine != mach {
			continue
		}

		if len(ar.DownloadOffers) == 0 {
			continue
		}

		plausibleDownloadOffer := false

		for i := range ar.DownloadOffers {
			u, _ := url.Parse(ar.DownloadOffers[i].URL)
			if u == nil {
				continue
			}

			if u.Scheme == httpScheme || u.Scheme == httpsScheme {
				plausibleDownloadOffer = true
				break
			}
		}

		if !plausibleDownloadOffer {
			continue
		}

		viable = append(viable, ar)
	}

	switch len(viable) {
	case 0:
		return nil, fmt.Errorf("no viable archive found")
	case 1:
		return viable[0], nil
	default:
		for _, ar := range viable {
			if ar.CompatibleDefault {
				return ar, nil
			}
		}

		return nil, fmt.Errorf("no default archive available")
	}
}

// pickDownloadOffer picks the best download offer.
//
// Offers with URLs that cannot be parsed are ignored. Offers using URL schemes
// other than http or https are also ignored. Remaining offers are sorted by
// metric, the lowest metric wins.
func (hs *HostedService) pickDownloadOffer(ar *netota.Archive) (netota.DownloadOffer, error) {
	var viable []netota.DownloadOffer

	for i := range ar.DownloadOffers {
		u, _ := url.Parse(ar.DownloadOffers[i].URL)
		if u == nil {
			continue
		}

		if u.Scheme != httpScheme && u.Scheme != httpsScheme {
			continue
		}

		viable = append(viable, ar.DownloadOffers[i])
	}

	sort.Slice(viable, func(i, j int) bool {
		return viable[i].Metric < viable[j].Metric
	})

	if len(viable) == 0 {
		return netota.DownloadOffer{}, fmt.Errorf("no download offer available")
	}

	return viable[0], nil
}

func (hs *HostedService) client() (*netota.Client, error) {
	serverURL, err := hs.serverURL()
	if err != nil {
		return nil, err
	}

	if serverURL == "" {
		return nil, dbus.MakeFailedError(fmt.Errorf("ServerURL property is not set"))
	}

	return &netota.Client{Addr: serverURL}, nil
}

func (hs *HostedService) serverURL() (string, error) {
	// NOTE: dbusErr is of type *dbus.Error. A nil value converts to non-nil
	// error, because error is an internal interface. Avoid undesired conversion
	// yielding non-nil errors pointing to nil values of specific pointer type.
	p, dbusErr := hs.props.Get(svcspec.ServerURLProperty)
	if dbusErr != nil {
		return "", dbusErr
	}

	// XXX: Which is it?
	switch v := p.Value().(type) {
	case string:
		return v, nil
	case *string:
		return *v, nil
	default:
		panic("ServerURL is not a string")
	}
}

func (hs *HostedService) systemImagePackageName() (string, error) {
	// NOTE: dbusErr is of type *dbus.Error. A nil value converts to non-nil
	// error, because error is an internal interface. Avoid undesired conversion
	// yielding non-nil errors pointing to nil values of specific pointer type.
	p, dbusErr := hs.props.Get(svcspec.SystemImagePackageProperty)
	if dbusErr != nil {
		return "", dbusErr
	}

	// XXX: Figure out why go-dbus has this weird behavior regarding properties
	// to simple types. It seems to be related to how the property is set.
	switch v := p.Value().(type) {
	case string:
		return v, nil
	case *string:
		return *v, nil
	default:
		panic("Package is not a string")
	}
}

func (hs *HostedService) nonEmptySystemImagePackageName() (string, error) {
	name, err := hs.systemImagePackageName()
	if err != nil {
		return "", err
	}

	if name == "" {
		return "", errors.New("SystemImagePackage property is not set")
	}

	return name, nil
}

func (hs *HostedService) streamName() (string, error) {
	// NOTE: dbusErr is of type *dbus.Error. A nil value converts to non-nil
	// error, because error is an internal interface. Avoid undesired conversion
	// yielding non-nil errors pointing to nil values of specific pointer type.
	p, dbusErr := hs.props.Get(svcspec.StreamProperty)
	if dbusErr != nil {
		return "", dbusErr
	}

	// XXX: Figure out why go-dbus has this weird behavior regarding properties
	// to simple types. It seems to be related to how the property is set.
	switch v := p.Value().(type) {
	case string:
		return v, nil
	case *string:
		return *v, nil
	default:
		panic("Stream is not a string")
	}
}

func (hs *HostedService) nonEmptyStreamName() (string, error) {
	name, err := hs.streamName()
	if err != nil {
		return "", err
	}

	if name == "" {
		return "", dbus.MakeFailedError(fmt.Errorf("the Stream property is not set"))
	}

	return name, nil
}

func (hs *HostedService) makeOp() (*oper.Operation, error) {
	hs.lastOperationID++

	op, err := oper.NewOperation(hs.conn, hs.lastOperationID)
	if err != nil {
		return nil, err
	}

	// NOTE: dbusErr is of type *dbus.Error. A nil value converts to non-nil
	// error, because error is an internal interface. Avoid undesired conversion
	// yielding non-nil errors pointing to nil values of specific pointer type.
	operProps, dbusErr := op.GetAll(opspec.Interface)
	if dbusErr != nil {
		// *dbus.Error converted to error
		return nil, dbusErr
	}

	path := op.ObjectPath()
	ifacesProps := map[string]map[string]dbus.Variant{opspec.Interface: operProps}

	if err := hs.conn.Emit(path, objmgr.InterfacesAddedSignal, path, ifacesProps); err != nil {
		return nil, err
	}

	return op, nil
}

// GetManagedObjects implements org.freedesktop.DBus.ObjectManager.GetManagedObjects.
func (hs *HostedService) GetManagedObjects() (map[dbus.ObjectPath]map[string]map[string]dbus.Variant, *dbus.Error) {
	hs.m.RLock()
	defer hs.m.RUnlock()

	objsIfacesProps := make(map[dbus.ObjectPath]map[string]map[string]dbus.Variant, len(hs.ops))

	for _, op := range hs.ops {
		props, dbusErr := op.GetAll(opspec.Interface)
		if dbusErr != nil {
			return nil, dbusErr
		}

		objsIfacesProps[op.ObjectPath()] = map[string]map[string]dbus.Variant{
			opspec.Interface: props,
		}
	}

	return objsIfacesProps, nil
}
