// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
package sysotad

import (
	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/ota"
)

// SetTestBootProtocol allows ota.TestBoot to be used in ota.Config.BootLoaderType.
//
// The provided boot protocol is registered into the boot provider map.
// Each test using the test boot loader type should call this method with the
// desired instance as the provided boot protocol object is repeated returned
// by configuredBootProtocol.
func SetTestBootProtocol(proto boot.Protocol) {
	bootProviders[ota.TestBoot] = func(*ota.Config) (boot.Protocol, error) {
		return proto, nil
	}
}
