// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
package sysotad_test

import (
	"os"
	"path/filepath"

	"gitlab.com/zygoon/sysota/cmd/sysotad"
	"gitlab.com/zygoon/sysota/ota"

	. "gopkg.in/check.v1"
)

type configFilesSuite struct {
	cf sysotad.ConfigFiles
}

var _ = Suite(&configFilesSuite{})

func (s *configFilesSuite) SetUpTest(c *C) {
	d := c.MkDir()
	r1 := filepath.Join(d, "read-1.conf")
	r2 := filepath.Join(d, "read-2.conf")
	w := filepath.Join(d, "write.conf")

	err := os.WriteFile(r1, []byte("[OTA]\nBootLoaderType=inert"), 0600)
	c.Assert(err, IsNil)

	err = os.WriteFile(r1, []byte("[OTA]\nBootLoaderType=pi-boot"), 0600)
	c.Assert(err, IsNil)

	s.cf = sysotad.ConfigFiles{ReadFiles: []string{r1, r2}, WriteFile: w}
}

func (s *configFilesSuite) TestLoad(c *C) {
	cfg, err := s.cf.LoadConfig()
	c.Assert(err, IsNil)
	c.Check(cfg, DeepEquals, &ota.Config{BootLoaderType: ota.PiBoot})
}

func (s *configFilesSuite) TestSave(c *C) {
	err := s.cf.SaveConfig(&ota.Config{UpdateStream: "potato"})
	c.Assert(err, IsNil)

	data, err := os.ReadFile(s.cf.WriteFile)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "[Update]\nStream=potato\n")
}
