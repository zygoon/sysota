// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
package sysotad_test

import (
	"context"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/zygoon/sysota/cmd/sysotad"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
	"gitlab.com/zygoon/sysota/pkg/testutil"

	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type sysotadSuite struct {
	dbustest.Suite
}

var _ = Suite(&sysotadSuite{})

func (s *sysotadSuite) TestStartupAndIdleShutdown(c *C) {
	opts := sysotad.DefaultOptions()
	opts.IdleDuration = time.Second
	opts.StateFile = filepath.Join(c.MkDir(), "non-existent/state.ini")

	cmd := sysotad.Cmd{Opts: opts}
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := cmd.Run(ctx, []string{})

	c.Assert(err, IsNil)
	c.Check(stdout.String(), Equals, ""+
		"Listening ...\n"+
		"Exiting due to inactivity\n")
	c.Check(stderr.String(), Equals, "")
}
