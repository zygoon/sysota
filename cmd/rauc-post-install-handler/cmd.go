// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package raucpostinstallhandler implements the rauc-post-install-handler program.
//
// The program is compiled to sysota-mux executable. The entry point of the
// program is the Cmd.Run method. It exits as RAUC requires a specific
// post-install handler program to interact with installed bundles.
//
// The program is a thin D-Bus bridge to the sysotad service, where a compatible
// interface is exposed by the RAUC boot adapter hosted service. The logic of
// adapting RAUC interfaces to SysOTA boot.Protocol interface is in the package
// ota/raucadapter.
package raucpostinstallhandler

import (
	"context"

	"gitlab.com/zygoon/sysota/rauc/installhandler"
	"gitlab.com/zygoon/sysota/rauc/installhandler/installdbus"
)

// Name is the name of this command inside the sysota-mux executable.
const Name = "rauc-post-install-handler"

// Cmd implements rauc-post-install-handler.
type Cmd struct{}

// OneLiner returns a help message when used inside Router.
func (cmd Cmd) OneLiner() string {
	return "Act as a RAUC post-install handler"
}

// Run constructs handler context and invokes the post-install method over D-Bus.
func (cmd Cmd) Run(_ context.Context, _ []string) error {
	return installdbus.Do(func(client *installdbus.Client, env *installhandler.Environment) error {
		return client.PostInstall(env)
	})
}
