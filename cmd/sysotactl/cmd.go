// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package sysotactl implements the sysotactl command line tool.
package sysotactl

import (
	"context"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
)

// Name is the name of the sysotactl command in the sysota-mux executable.
const Name = "sysotactl"

// Cmd is the sysotactl command router.
type Cmd struct{}

// OneLiner returns a help message when used inside Router.
func (Cmd) OneLiner() string {
	return "SysOTA control utility"
}

// Run parses arguments and routes execution to the appropriate sub-command.
func (Cmd) Run(ctx context.Context, args []string) error {
	r := router.Router{
		Name:     Name,
		Implicit: StatusCmd{},
		Commands: map[string]cmdr.Cmd{
			"status": StatusCmd{},

			"set-server":  SetServerCmd{},
			"set-package": SetPackageCmd{},
			"set-stream":  SetStreamCmd{},

			"streams": StreamsCmd{},
			"update":  UpdateCmd{},
		}}

	return r.Run(ctx, args)
}
