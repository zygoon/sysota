// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package testutil contains utilities for writing tests.
package testutil

import (
	"bufio"
	"os"
	"path/filepath"
	"strings"

	"gopkg.in/check.v1"
)

// CommandSuite is a test suite which makes it easy to intercept execution of external programs.
type CommandSuite struct {
	mockBinDir string
	oldPath    string
}

const envPath = "PATH"

// SetUpSuite creates a directory for mock binaries and sets PATH to prefer finding executables there.
func (s *CommandSuite) SetUpSuite(c *check.C) {
	s.mockBinDir = filepath.Join(c.MkDir(), "mock-bin")
	err := os.Mkdir(s.mockBinDir, 0755)
	c.Assert(err, check.IsNil)

	s.oldPath = os.Getenv(envPath)
	err = os.Setenv(envPath, strings.Join([]string{s.mockBinDir, s.oldPath}, string(os.PathListSeparator)))
	c.Assert(err, check.IsNil)
}

// TearDownSuite restores original PATH.
func (s *CommandSuite) TearDownSuite(c *check.C) {
	err := os.Setenv(envPath, s.oldPath)
	c.Assert(err, check.IsNil)
}

const stubScript = `#!/bin/bash
set -e
{
	# Basename is unrealistic but helps with temporary directory name
	# which contains this script during testing.
	printf '%q' "$(basename "$0")"
	for arg in "$@"; do
		printf ' %q' "$arg"
	done
	printf '\n'
} >> "$0.log"
`

// MockCommand puts a stub executable in a directory for mock binaries.
//
// The executable, when executed, records all arguments and returns
// successfully. The returned inspect function returns the list of arguments,
// one for each time the command was invoked. Inspect resets the execution log
// each time.
func (s *CommandSuite) MockCommand(c *check.C, name string) (restore func(), inspect func(c *check.C) []string) {
	fullname := filepath.Join(s.mockBinDir, name)
	err := os.WriteFile(fullname, []byte(stubScript), 0o755)
	c.Assert(err, check.IsNil)

	restore = func() {
		c.Assert(os.Remove(fullname), check.IsNil)
	}
	inspect = func(c *check.C) (lines []string) {
		logName := fullname + ".log"

		f, err := os.Open(logName)
		if err != nil && os.IsNotExist(err) {
			return nil
		}

		c.Assert(err, check.IsNil)

		defer func() {
			c.Assert(f.Close(), check.IsNil)
		}()

		scan := bufio.NewScanner(f)
		for scan.Scan() {
			lines = append(lines, scan.Text())
		}
		c.Assert(scan.Err(), check.IsNil)
		c.Assert(os.Remove(logName), check.IsNil)

		return lines
	}

	return restore, inspect
}
