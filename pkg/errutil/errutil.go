// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package errutil contains error utility code.
package errutil

import (
	"fmt"
	"io"
	"os"
)

// ErrorWriter is used to write errors which could not be forwarded.
var ErrorWriter io.Writer = os.Stderr

// Forward remembers the first error, logging remaining problems.
//
// Forward can be used to collect a number of possible errors and save *one* to
// the desired location. Remaining errors are logged to standard error.
func Forward(to *error, errs ...error) {
	for _, err := range errs {
		if err != nil {
			if *to == nil {
				*to = err
			} else {
				_, _ = fmt.Fprintf(ErrorWriter, "unable to forward error: %v\n", err)
			}
		}
	}
}
