// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package dbusutil contains D-Bus utility code not present in godbus.
package dbusutil

import (
	"github.com/godbus/dbus/v5"
)

const (
	// PropertiesInterface is the name of the standard Properties interface.
	PropertiesInterface = "org.freedesktop.DBus.Properties"
	// IntrospectableInterface is the name of the standard Introspectable interface.
	IntrospectableInterface = "org.freedesktop.DBus.Introspectable"

	// PropertiesChangedSignal is the name of the PropertiesChanged signal.
	PropertiesChangedSignal = "PropertiesChanged"

	// PropertyEmitsChangedSignalAnnotation is a standard annotation for individual properties.
	PropertyEmitsChangedSignalAnnotation = "org.freedesktop.DBus.Property.EmitsChangedSignal"

	// PropertyGetMethod is the name of the Get method of the standard Properties interface.
	PropertyGetMethod = "Get"
	// PropertyGetAllMethod is the name of the GetAll method of the standard Properties interface.
	PropertyGetAllMethod = "GetAll"
	// PropertySetMethod is the name of the Set method of the standard Properties interface.
	PropertySetMethod = "Set"
)

const (
	// In denotes incoming argument in the D-Bus introspection data.
	In = "in"
	// Out denotes outgoing argument in the D-Bus introspection data.
	Out = "out"
)

// SystemBus is like dbus.SystemBus but the returned connection can be closed.
func SystemBus(opts ...dbus.ConnOption) (conn *dbus.Conn, err error) {
	conn, err = dbus.SystemBusPrivate(opts...)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil && conn != nil {
			_ = conn.Close()
		}
	}()

	if err := conn.Auth(nil); err != nil {
		return nil, err
	}

	if err := conn.Hello(); err != nil {
		return nil, err
	}

	return conn, nil
}
