// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package objmgr helps implementing D-Bus object manager interface.
//
// This standard interface is specified by
// https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces-objectmanager
package objmgr

import (
	"github.com/godbus/dbus/v5/introspect"

	"gitlab.com/zygoon/sysota/pkg/dbusutil"
)

const (
	// InterfaceName is the well-known name of the D-Bus Object Manager.
	InterfaceName = "org.freedesktop.DBus.ObjectManager"
	// InterfacesAddedSignal is the well-known name of the InterfacesAdded signal of the D-Bus Object Manager.
	InterfacesAddedSignal = "org.freedesktop.DBus.ObjectManager.InterfacesAdded"
	// InterfacesRemovedSignal is the well-known name of the InterfacesRemoved signal of the D-Bus Object Manager.
	InterfacesRemovedSignal = "org.freedesktop.DBus.ObjectManager.InterfacesRemoved"
)

// IntrospectData is the D-Bus introspection data for the ObjectManager interface.
var IntrospectData = introspect.Interface{
	Name: "org.freedesktop.DBus.ObjectManager",
	Methods: []introspect.Method{
		{
			Name: "GetManagedObjects",
			Args: []introspect.Arg{
				{
					Name:      "objpath_interfaces_and_properties",
					Type:      "a{oa{sa{sv}}}",
					Direction: dbusutil.Out,
				},
			},
		},
	},
	Signals: []introspect.Signal{
		{
			Name: "InterfacesAdded",
			Args: []introspect.Arg{
				{
					Name:      "object_path",
					Type:      "o",
					Direction: dbusutil.Out,
				},
				{
					Name:      "interfaces_and_properties",
					Type:      "a{sa{sv}}",
					Direction: dbusutil.Out,
				},
			},
		},
		{
			Name: "InterfacesRemoved",
			Args: []introspect.Arg{
				{
					Name:      "object_path",
					Type:      "o",
					Direction: dbusutil.Out,
				},
				{
					Name:      "interfaces",
					Type:      "as",
					Direction: dbusutil.Out,
				},
			},
		},
	},
}
