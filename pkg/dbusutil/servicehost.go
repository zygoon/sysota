// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package dbusutil

import (
	"sort"
	"sync"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	"github.com/godbus/dbus/v5/prop"
)

// ServiceRegistration is the interface offered by ServiceHost to HostedService.
//
// ServiceRegistration allows a hosted service to register any number of objects
// as well as to obtain the D-Bus connection object, if required.
type ServiceRegistration interface {
	// Object begins registration of an object with one or more interfaces.
	//
	// Each D-Bus object is identified with a path-like string but can offer any
	// number of named interfaces. Each interface offers a set of methods,
	// signals and properties and optional introspection meta-data.
	//
	// To allow independent hosted services to register an interface at a given
	// path, registration is broken down into individual components. Starting
	// with the object path needed to register an object.
	//
	// The returned value allows calling Interface which in turn allows
	// registering methods, properties and introspection data.
	//
	// Object should be called from HostedService.JoinServiceHost.
	Object(path dbus.ObjectPath) ObjectRegistration

	// DBusConn returns the bus connection object.
	//
	// The returned connection may be stored inside the hosted service to allow
	// it to send D-Bus signals.
	//
	// DBusConn should be called from HostedService.JoinServiceHost.
	DBusConn() *dbus.Conn
}

// HostedService is an interface between hosted service and ServiceHost.
//
// The interface defines only one function, JoinServiceHost, which allows an
// object to register itself with the service host. The method is called by
// ServiceHost.Export.
//
// The hosted service can make any number of calls to
// ServiceRegistration.RegisterObject to register distinct objects, distinct
// interfaces or both. In addition, the hosted service may call
// ServiceRegistration.DBusConn to obtain remember the D-Bus connection necessary
// to send signals.
type HostedService interface {
	JoinServiceHost(reg ServiceRegistration) error
}

// ServiceHost provides the boilerplate to run a D-Bus service.
//
// ServiceHost handles exporting and un-exporting objects from D-Bus. It is
// suitable for exposing one or more objects and interfaces to the bus,
// arranging to clean up everything on shutdown. ServiceHost does not handle
// bus name registration. That is left as a responsibility of the application.
//
// ServiceHost is useful for creating test services, where the amount of code
// needed for a successful service is minimal compared to a direct approach.
type ServiceHost struct {
	m    sync.RWMutex
	conn *dbus.Conn

	services          []HostedService
	objectsInterfaces map[dbus.ObjectPath]map[string]bool
	objectsProperties map[dbus.ObjectPath]*prop.Properties
}

// ObjectRegistration assists in registering D-Bus interfaces for a specific D-Bus object.
type ObjectRegistration interface {
	// Interface continues object registration by scoping registration to a
	// specific interface name. The return value can be used to register
	// methods, properties and introspection data.
	Interface(name string) InterfaceRegistration
}

// InterfaceRegistration assists in registering D-Bus methods, properties and
// introspection for a specific D-Bus interface.
//
// Export operations are deferred until ServiceHost.Export is called.
type InterfaceRegistration interface {
	SetMethods(map[string]interface{})
	SetProperties(map[string]*prop.Prop) *InterfaceProperties
	SetIntrospection(*introspect.Interface)
}

// InterfaceProperties expose properties of a particular object and interface.
//
// The interface is similar to that of prop.Properties, with the interface name
// being implicitly provided. This type can be used by hosted services which
// wish to interact with their properties after registration.
type InterfaceProperties struct {
	path  dbus.ObjectPath
	iface string
	sh    *ServiceHost
}

// Get exposes org.freedesktop.DBus.Properties.Get for a known interface value.
func (ip *InterfaceProperties) Get(property string) (dbus.Variant, *dbus.Error) {
	return ip.sh.objectsProperties[ip.path].Get(ip.iface, property)
}

// GetAll exposes org.freedesktop.DBus.Properties.GetAll for a known interface value.
func (ip *InterfaceProperties) GetAll() (map[string]dbus.Variant, *dbus.Error) {
	return ip.sh.objectsProperties[ip.path].GetAll(ip.iface)
}

// GetMust returns the value of the given property and panics if the property name is invalid.
func (ip *InterfaceProperties) GetMust(property string) interface{} {
	return ip.sh.objectsProperties[ip.path].GetMust(ip.iface, property)
}

// Set exposes org.freedesktop.DBus.Properties.Set for a known interface value.
func (ip *InterfaceProperties) Set(property string, v dbus.Variant) *dbus.Error {
	return ip.sh.objectsProperties[ip.path].Set(ip.iface, property, v)
}

// SetMust sets the value of the given property and panics if the property name is invalid.
func (ip *InterfaceProperties) SetMust(property string, v interface{}) {
	ip.sh.objectsProperties[ip.path].SetMust(ip.iface, property, v)
}

// NewServiceHost returns a new service host.
//
// The connection is not closed by the service host. It is the responsibility of
// the caller.
func NewServiceHost(conn *dbus.Conn) *ServiceHost {
	return &ServiceHost{conn: conn}
}

// Init initializes an existing service host with a given connection.
//
// Init should be used when the service host is embedded inside another type.
func (sh *ServiceHost) Init(conn *dbus.Conn) {
	sh.conn = conn
}

// AddHostedService adds a given hosted service to the service host.
//
// Any number of hosted services may be added before calling Export. After
// calling Export adding additional services panics. Hosted services cannot be
// removed.
func (sh *ServiceHost) AddHostedService(svc HostedService) {
	sh.m.Lock()
	defer sh.m.Unlock()

	if sh.objectsInterfaces != nil {
		panic("AddHostedService cannot be used after calling Export")
	}

	sh.services = append(sh.services, svc)
}

// svcRegistration implements ServiceRegistration.
type svcRegistration struct {
	*ServiceHost
	objs  map[dbus.ObjectPath]*objRegistration
	order []dbus.ObjectPath
}

// DBusConn returns the bus connection object.
func (reg *svcRegistration) DBusConn() *dbus.Conn {
	var sh = reg.ServiceHost

	return sh.conn
}

// Object remembers path of the D-Bus object to export.
func (reg *svcRegistration) Object(path dbus.ObjectPath) ObjectRegistration {
	if reg.objs == nil {
		reg.objs = make(map[dbus.ObjectPath]*objRegistration)
	}

	r, ok := reg.objs[path]
	if !ok {
		r = &objRegistration{svcRegistration: reg, path: path}
		reg.objs[path] = r
		reg.order = append(reg.order, path)
	}

	return r
}

// Export exports all the hosted services to the bus.
//
// Each hosted service added with AddHostedService is asked to provide the set
// of object paths, interface names, methods and introspection data by calling
// ServiceRegistration.RegisterObject on the object provided as an argument to
// HostedService.JoinServiceHost.
//
// Introspection data is aggregated by object path and exported as well, using
// the standard introspection interface.
//
// In case any of the operation fails, the objects exported thus far are
// unexported. Any error encountered during the unexport process is ignored.
//
// Export panics if called when there are any objects already exported.
func (sh *ServiceHost) Export() (err error) {
	sh.m.Lock()
	defer sh.m.Unlock()

	if sh.objectsInterfaces != nil {
		panic("Export already called")
	}

	sh.objectsInterfaces = make(map[dbus.ObjectPath]map[string]bool)
	sh.objectsProperties = make(map[dbus.ObjectPath]*prop.Properties)

	defer func() {
		// introspection is only non-nil until the end of Export.
		if err != nil {
			_ = sh.unexportUnlocked()
		}
	}()

	// Collect exported objects, interfaces, methods, properties and
	// introspection data from all the hosted services.
	svcReg := svcRegistration{ServiceHost: sh}

	for _, hs := range sh.services {
		if err := hs.JoinServiceHost(&svcReg); err != nil {
			return err
		}
	}

	// We now have a complete picture of what to export.
	//
	// Iterating through all the objects and their interfaces export methods,
	// properties and introspection meta-data. Note that exporting properties or
	// introspection meta-data also exports additional interfaces and methods to
	// retrieve them.
	for _, objPath := range svcReg.order {
		objReg := svcReg.objs[objPath]

		// Slice of all the introspection data of the object being worked on.
		// This allows us to export introspection data across all the interfaces
		// in one go, even though individual parts may have come from distinct
		// hosted services.
		var allIfacesMeta []introspect.Interface

		// Map of all the properties of the object being worked on.
		var allProps map[string]map[string]*prop.Prop

		// Remember the set of exported interfaces. This data is kept for
		// Unexport to reverse the operation.
		sh.objectsInterfaces[objReg.path] = make(map[string]bool)

		exportedIfaces := sh.objectsInterfaces[objReg.path]

		// For each interface of the object being exported, do:
		for _, ifaceName := range objReg.order {
			ifaceReg := objReg.ifaces[ifaceName]

			// Export interface methods if any are provided.
			if len(ifaceReg.methods) > 0 {
				if err := sh.conn.ExportMethodTable(ifaceReg.methods, objReg.path, ifaceReg.name); err != nil {
					return err
				}

				exportedIfaces[ifaceReg.name] = true
			}

			// Aggregate properties across all the interfaces of this object.
			for propName, p := range ifaceReg.props {
				if allProps == nil {
					allProps = make(map[string]map[string]*prop.Prop)
				}

				if allProps[ifaceReg.name] == nil {
					allProps[ifaceReg.name] = make(map[string]*prop.Prop)
				}

				allProps[ifaceReg.name][propName] = p
			}

			// Aggregate introspection data cross all the interfaces of this object.
			if ifaceReg.meta != nil {
				allIfacesMeta = append(allIfacesMeta, *ifaceReg.meta)
			}
		}

		// Export properties if any were defined.
		// Internally this exports the standard properties interface.
		if len(allProps) > 0 {
			props, err := prop.Export(sh.conn, objReg.path, allProps)
			if err != nil {
				return err
			}

			// Remember we have exported the standard properties interface.
			exportedIfaces[PropertiesInterface] = true

			// Remember the properties per-object. This can come in handy later.
			sh.objectsProperties[objReg.path] = props

			// Augment existing introspection data with auto-generated property
			// introspection unless properties are explicitly annotated by hand.
			for ifaceName := range allProps {
				for i := range allIfacesMeta {
					if allIfacesMeta[i].Name == ifaceName && len(allIfacesMeta[i].Properties) == 0 {
						propsMeta := props.Introspection(ifaceName)
						sort.Slice(propsMeta, func(i, j int) bool {
							return propsMeta[i].Name < propsMeta[j].Name
						})

						allIfacesMeta[i].Properties = propsMeta
					}
				}
			}

			// Properties interface has its own introspection data.
			allIfacesMeta = append(allIfacesMeta, prop.IntrospectData)
		}

		// Export introspection data if any was defined.
		// This exports the standard introspectable interface.
		if len(allIfacesMeta) > 0 {
			allIfacesMeta = append(allIfacesMeta, introspect.IntrospectData)

			introspectable := introspect.NewIntrospectable(&introspect.Node{
				Name:       string(objReg.path),
				Interfaces: allIfacesMeta,
			})

			if err := sh.conn.Export(introspectable, objReg.path, IntrospectableInterface); err != nil {
				return err
			}

			// Remember we have exported the standard introspectable interface.
			exportedIfaces[IntrospectableInterface] = true
		}
	}

	return nil
}

type objRegistration struct {
	*svcRegistration
	path   dbus.ObjectPath
	ifaces map[string]*ifaceRegistration
	order  []string
}

// Interface remembers name of the D-Bus interface to export.
func (reg *objRegistration) Interface(name string) InterfaceRegistration {
	if reg.ifaces == nil {
		reg.ifaces = make(map[string]*ifaceRegistration)
	}

	r, ok := reg.ifaces[name]
	if !ok {
		r = &ifaceRegistration{objRegistration: reg, name: name}
		reg.ifaces[name] = r
		reg.order = append(reg.order, name)
	}

	return r
}

type ifaceRegistration struct {
	*objRegistration
	name    string
	props   map[string]*prop.Prop
	methods map[string]interface{}
	meta    *introspect.Interface
}

// SetMethods remembers the method map to export.
func (reg *ifaceRegistration) SetMethods(methods map[string]interface{}) {
	reg.methods = methods
}

// SetProperties remembers the property map to export.
func (reg *ifaceRegistration) SetProperties(props map[string]*prop.Prop) *InterfaceProperties {
	reg.props = props

	return &InterfaceProperties{
		path:  reg.objRegistration.path,
		iface: reg.name,
		sh:    reg.objRegistration.svcRegistration.ServiceHost,
	}
}

// SetIntrospection remembers the introspection meta-data to export.
func (reg *ifaceRegistration) SetIntrospection(meta *introspect.Interface) {
	reg.meta = meta
}

// Unexport undoes the operations done by Export
//
// For each object path and interface name, the unexport process remembers
// progress, making sure not to unexport the same pair twice. The process stops
// on the first encountered error.
//
// Note that closing the connection with the bus automatically unexports all the
// objects and is a suitable approach to error recovery.
func (sh *ServiceHost) Unexport() error {
	sh.m.Lock()
	defer sh.m.Unlock()

	return sh.unexportUnlocked()
}

func (sh *ServiceHost) unexportUnlocked() error {
	for path, ifaces := range sh.objectsInterfaces {
		for iface, ok := range ifaces {
			if ok {
				if err := sh.conn.Export(nil, path, iface); err != nil {
					return err
				}

				sh.objectsInterfaces[path][iface] = false
			}
		}
	}

	sh.objectsInterfaces = nil

	return nil
}
