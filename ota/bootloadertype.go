// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ota

import (
	"bytes"
	"fmt"
)

// BootLoaderType designates the type of the boot loader.
type BootLoaderType int

const (
	// InertBootLoader indicates that the OTA system will not interact with the boot loader.
	InertBootLoader BootLoaderType = iota
	// TestBoot can only be used in unit tests, it cannot be marshalled. To use
	// it, call service.SetTestBootProtocol and pass &ota.Config{BootLoaderType:
	// TestBoot} to service.New.
	TestBoot
	// PiBoot indicates that the OTA system will configure the Raspberry Pi boot loader.
	PiBoot
	// GRUB indicates that the OTA system will configure a GRUB bootloader.
	GRUB
)

const (
	inert    = "inert"
	testBoot = "test"
	piBoot   = "pi-boot"
	grub     = "GRUB"
)

// String returns the type of the boot loader as text.
func (t BootLoaderType) String() string {
	switch t {
	case InertBootLoader:
		return inert
	case TestBoot:
		return testBoot
	case PiBoot:
		return piBoot
	case GRUB:
		return grub
	default:
		return fmt.Sprintf("BootLoaderType(%d)", int(t))
	}
}

// MarshalText implements TextMarshaler.
func (t BootLoaderType) MarshalText() ([]byte, error) {
	switch t {
	case InertBootLoader:
		return []byte(inert), nil
	case TestBoot:
		return nil, fmt.Errorf("test boot loader type cannot be marshaled")
	case PiBoot:
		return []byte(piBoot), nil
	case GRUB:
		return []byte(grub), nil
	default:
		return nil, fmt.Errorf("invalid boot loader type: %d", int(t))
	}
}

// UnmarshalText implements TextUnmarshaler.
func (t *BootLoaderType) UnmarshalText(data []byte) error {
	if bytes.Equal(data, []byte(inert)) {
		*t = InertBootLoader
		return nil
	} else if bytes.Equal(data, []byte(piBoot)) {
		*t = PiBoot
		return nil
	} else if bytes.Equal(data, []byte(grub)) {
		*t = GRUB
		return nil
	} else {
		return fmt.Errorf("cannot unmarshal boot loader type from %q", string(data))
	}
}
