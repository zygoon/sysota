# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

# Make sure ZMK.BugFixes is defined. It is available since zmk 0.5.1 but older
# versions do not offer it, this avoids a warning about using an undefined
# variable.
ZMK.BugFixes ?=

# Hotfix for zmk-issue-80: https://github.com/zyga/zmk/issues/80
ifeq ($(filter zmk-issue-80,$(ZMK.BugFixes)),)
$(eval $(call ZMK.Import,Symlink))
define Symlink.Template
$1.InstallDir ?= $$(error define $1.InstallDir - the destination directory, or noinst to skip installation)
$1.SymlinkTarget ?= $$(error define $1.SymlinkTarget - the target of the symbolic link)
$1.InstallName ?= $$(notdir $1)

# Create the directory where the symbolic link is built in.
$$(eval $$(call ZMK.Expand,Directory,$$(dir $1)))
# Create the symbolic link in the build directory.
$1: | $$(patsubst %/,%,$$(dir $1))
	$$(call Silent.Say,SYMLINK,$$@)
	$$(Silent.Command)$$(strip ln -sf $$($1.SymlinkTarget) $$@)
# React to "all" and "clean" targets.
$$(eval $$(call ZMK.Expand,AllClean,$1))

# Unless we don't want to install the file, look below.
ifneq ($$($1.InstallDir),noinst)

# Create the directory where the symbolic link is installed to.
$$(eval $$(call ZMK.Expand,Directory,$$($1.InstallDir)))
# Create the symbolic link in the install directory.
$$(DESTDIR)$$($1.InstallDir)/$$($1.InstallName):| $$(DESTDIR)$$($1.InstallDir)
	$$(call Silent.Say,SYMLINK,$$@)
	$$(Silent.Command)$$(strip ln -sf $$($1.SymlinkTarget) $$@)
# React to "install" and "uninstall" targets.
install:: $$(DESTDIR)$$($1.InstallDir)/$$($1.InstallName)
uninstall::
	$$(call Silent.Say,RM,$$($1.InstallDir)/$$($1.InstallName))
	$$(Silent.Command)rm -f $$(DESTDIR)$$($1.InstallDir)/$$($1.InstallName)
endif # !noinst
endef
endif # hotfix for zmk-issue-80
