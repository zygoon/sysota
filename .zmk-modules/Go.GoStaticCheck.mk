# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

$(eval $(call ZMK.Import,Go))

Go.GoStaticCheck.Cmd ?= $(strip $(shell PATH="(PATH):$(Go.Path)/bin" command -v staticcheck 2>/dev/null))
Go.GoStaticCheck.Options ?=

.PHONY: static-check-go-staticcheck
static-check-go-staticcheck:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(strip $(Go.GoStaticCheck.Cmd) $(Go.GoStaticCheck.Options) $(Go.ImportPath)/...)

# Run "staticcheck" when running static checks, but only when installed.
# You can install this tool by following instructions on https://staticcheck.io/docs/install
ifneq (,$(Go.GoStaticCheck.Cmd))
static-check:: static-check-go-staticcheck
else
$(if $(filter check static-check,$(MAKECMDGOALS)),$(warning The staticcheck program is not available, static-check target will not depend on static-check-go-staticcheck))
endif
