# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

PkgConfig.Cmd ?= $(shell command -v pkg-config 2>/dev/null)
PkgConfig.atLeastPkgConfigVersion = 0.29

ifeq (,$(PkgConfig.Cmd))
$(error pkg-config is not available in PATH, install it or pass PkgConfig.Cmd= to make)
endif

ifeq (,$(shell $(PkgConfig.Cmd) --atleast-pkgconfig-version=$(PkgConfig.atLeastPkgConfigVersion) 2>/dev/null && echo ok))
$(error $(PkgConfig.Cmd) is too old, version $(PkgConfig.atLeastPkgConfigVersion) is required, you have $(shell $(PkgConfig.Cmd) --version))
endif

define PkgConfig.moduleStatusScript
if $(PkgConfig.Cmd) --atleast-version=$2 $1; then
	echo "sufficient";
else
	if $(PkgConfig.Cmd) --exists $1; then
		echo "outdated";
	else
		echo "missing";
	fi;
fi;
endef

PkgConfig.Variables = AtLeastVersion Variables
define PkgConfig.Template
$1.AtLeastVersion ?= $$(error define $1.AtLeastVersion - the required version of the module)
$1.Variables ?=

PkgConfig.Modules.$1.Status ?= $$(shell $$(call PkgConfig.moduleStatusScript,$1,$$($1.AtLeastVersion)))
ifneq (missing,$$(PkgConfig.Modules.$1.Status))
PkgConfig.Modules.$1.Version ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --modversion))

PkgConfig.Modules.$1.Cflags ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --cflags))
PkgConfig.Modules.$1.CflagsOnlyI ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --cflags-only-I))
PkgConfig.Modules.$1.CflagsOnlyOther ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --cflags-only-other))

PkgConfig.Modules.$1.Libs ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --libs))
PkgConfig.Modules.$1.LibsOnlyLowerL ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --libs-only-l))
PkgConfig.Modules.$1.LibsOnlyL ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --libs-only-L))
PkgConfig.Modules.$1.LibsOnlyOther ?= $$(shell $$(PkgConfig.Cmd) $1 --silence-errors --libs-only-other))

$$(foreach v,$$($1.Variables),$$(eval PkgConfig.Modules.$1.Variables.$$(v) ?= $$(shell $$(PkgConfig.Cmd) $1 --variable $$(v) 2>/dev/null)))
endif
endef
