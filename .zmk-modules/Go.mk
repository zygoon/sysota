# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

Go.Cmd ?= go
Go.TestOptions ?= -race
Go.Sources ?= $(patsubst $(ZMK.SrcDir)/%,%,$(shell find $(ZMK.SrcDir) -name '*.go' -o -name go.mod -o -name go.sum))
Go.ImportPath ?= $(error Define Go.ImportPath to the import path of the project)
Go.Path ?= $(shell $(Go.Cmd) env GOPATH)

# Run "go test" when running checks.
check:: check-go-test
.PHONY: check-go-test
check-go-test:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(Go.Cmd) test $(Go.TestOptions) $(Go.ImportPath)/...

# Run "go vet" when running static checks.
static-check:: static-check-go-vet
.PHONY: static-check-go-vet
static-check-go-vet:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(Go.Cmd) vet $(Go.ImportPath)/...
