# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

Reuse.Cmd ?= $(strip $(shell command -v reuse 2>/dev/null))
Reuse.Sources ?= $(patsubst $(ZMK.SrcDir)/%,%,$(shell find $(ZMK.SrcDir) -name '*.license' -o -path './LICENSES/*'))

ifneq (,$(Reuse.Cmd))
static-check:: static-check-reuse
else
$(if $(filter static-check check,$(MAKECMDGOALS)),$(warning The reuse program is not available, static-check target will not depend on static-check-reuse))
endif

.PHONY: static-check-reuse
static-check-reuse:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(Reuse.Cmd) lint
