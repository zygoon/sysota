# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

$(eval $(call ZMK.Import,Go))

Go.GoIneffAssign.Cmd ?= $(strip $(shell PATH="$(PATH):$(Go.Path)/bin" command -v ineffassign 2>/dev/null))
Go.GoIneffAssign.Options ?=

.PHONY: static-check-go-ineffassign
static-check-go-ineffassign:
	$(if $(ZMK.IsOutOfTreeBuild),cd $(ZMK.SrcDir) && )$(strip $(Go.GoIneffAssign.Cmd) $(Go.GoIneffAssign.Options) $(Go.ImportPath)/...)

# Run "ineffassign" when running static checks, but only when installed.
# You can install this tool by following instructions on https://github.com/gordonklaus/ineffassign
ifneq (,$(Go.GoIneffAssign.Cmd))
static-check:: static-check-go-ineffassign
else
$(if $(filter check static-check,$(MAKECMDGOALS)),$(warning The ineffassign program is not available, static-check target will not depend on static-check-go-ineffassign))
endif

