// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boot_test

import (
	"time"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type rebootSuite struct {
	testutil.CommandSuite
}

var _ = Suite(&rebootSuite{})

func (s *rebootSuite) TestReboot(c *C) {
	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	err := boot.RebootSystem()
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})

	err = boot.RebootSystem("potato")
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot potato`})

	err = boot.RebootSystem("0 tryboot")
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot 0\ tryboot`})
}

type delayedRebootSuite struct {
	testutil.CommandSuite
}

var _ = Suite(&delayedRebootSuite{})

func (s *delayedRebootSuite) TestImmediateReboot(c *C) {
	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	var d boot.DelayedReboot

	c.Check(d.RebootPending(), Equals, false)
	c.Check(d.RebootSystem(), IsNil)
	c.Check(d.RebootPending(), Equals, false)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})
}

func (s *delayedRebootSuite) TestDelayedReboot(c *C) {
	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	var d boot.DelayedReboot

	c.Check(d.RebootPending(), Equals, false)
	d.SetRebootDelay(time.Minute)
	c.Check(d.RebootPending(), Equals, false)
	d.SetRebootDelay(time.Second)
	c.Check(d.RebootSystem(), IsNil)
	c.Check(d.RebootPending(), Equals, true)
	time.Sleep(time.Second * 3)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})
}
