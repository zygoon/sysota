// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package piboot_test

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"gitlab.com/zygoon/go-raspi/pieeprom"
	"gitlab.com/zygoon/go-raspi/pimodel"
	"gitlab.com/zygoon/go-squashfstools/mksquashfs"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/boot/piboot"
	"gitlab.com/zygoon/sysota/dirs"
	"gitlab.com/zygoon/sysota/pkg/testutil"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
)

func Test(t *testing.T) { TestingT(t) }

type bootTest struct {
	testutil.CommandSuite
	bootDir   string
	procDir   string
	revCode   pimodel.RevisionCode
	serialNum pimodel.SerialNumber
	pi        *piboot.PiBoot
}

var _ = Suite(&bootTest{
	// Arbitrary choice for as long a valid revision is used.
	revCode:   Pi4BRevCode,
	serialNum: sampleSerial,
})

func (s *bootTest) SetUpTest(c *C) {
	s.bootDir = c.MkDir()
	s.procDir = c.MkDir()

	pi, err := piboot.New(s.bootDir, s.procDir, s.revCode, s.serialNum)
	c.Assert(err, IsNil)

	s.pi = pi
}

func (s *bootTest) MockBootFile(c *C, path, content string) {
	name := filepath.Join(s.bootDir, path)

	err := os.MkdirAll(filepath.Dir(name), 0o755)
	c.Assert(err, IsNil)

	err = os.WriteFile(name, []byte(content), 0o644)
	c.Assert(err, IsNil)
}

func (s *bootTest) MockConfigTxt(c *C, content string) {
	s.MockBootFile(c, "config.txt", content)
}

func (s *bootTest) MockTrybootTxt(c *C, content string) {
	s.MockBootFile(c, "tryboot.txt", content)
}

func (s *bootTest) TestNew(c *C) {
	pi, err := piboot.New(s.bootDir, s.procDir, s.revCode, s.serialNum)
	c.Assert(err, IsNil)

	c.Check(pi.BootMountPoint(), Equals, s.bootDir)
	c.Check(pi.RevisionCode(), Equals, s.revCode)
	c.Check(pi.SerialNumber(), Equals, s.serialNum)
}

func (s *bootTest) TestSupportsDelayedReboot(c *C) {
	pi, err := piboot.New(s.bootDir, s.procDir, s.revCode, s.serialNum)
	c.Assert(err, IsNil)

	var delayer boot.RebootDelayer = pi

	c.Check(delayer, NotNil)
}

func (s *bootTest) TestQueryActive(c *C) {
	slot, err := s.pi.QueryActive()
	c.Assert(err, ErrorMatches, "cannot determine active slot: compatibility error: os_prefix= is not set")
	c.Assert(errors.Is(err, boot.ErrPristineBootConfig), Equals, true)
	c.Check(slot, Equals, boot.InvalidSlot)

	s.MockConfigTxt(c, "os_prefix=slot-a/\n")

	slot, err = s.pi.QueryActive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotA)

	s.MockConfigTxt(c, "os_prefix=slot-b/\n")

	slot, err = s.pi.QueryActive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotB)

	s.MockConfigTxt(c, "os_prefix=potato/")

	slot, err = s.pi.QueryActive()
	c.Assert(err, ErrorMatches, "cannot determine active slot: compatibility error: os_prefix= evades slot directory")
	c.Assert(errors.Is(err, boot.ErrPristineBootConfig), Equals, false)
	c.Check(slot, Equals, boot.InvalidSlot)
}

func (s *bootTest) TestQueryInactive(c *C) {
	slot, err := s.pi.QueryInactive()
	c.Assert(err, ErrorMatches, "cannot determine inactive slot: compatibility error: os_prefix= is not set")
	c.Check(slot, Equals, boot.InvalidSlot)

	s.MockConfigTxt(c, "os_prefix=slot-a/")

	slot, err = s.pi.QueryInactive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotB)

	s.MockConfigTxt(c, "os_prefix=slot-b/")

	slot, err = s.pi.QueryInactive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotA)

	s.MockConfigTxt(c, "os_prefix=potato/")

	slot, err = s.pi.QueryInactive()
	c.Assert(err, ErrorMatches, "cannot determine inactive slot: compatibility error: os_prefix= evades slot directory")
	c.Check(slot, Equals, boot.InvalidSlot)
}

func (s *bootTest) TestTrySwitchAtoB(c *C) {
	s.MockConfigTxt(c, "# pristine")
	s.MockBootFile(c, "slot-b/config.txt", ""+
		"kernel=vmlinuz\n"+
		"cmdline=cmdline.txt\n"+
		"initramfs initrd.img followkernel\n")
	s.MockBootFile(c, "slot-b/cmdline.txt", "... root=/dev/mmcblk0p4 rauc.slot=B")

	err := s.pi.TrySwitch(boot.SlotB)
	c.Assert(err, IsNil)

	// The config.txt file is not touched.
	data, err := os.ReadFile(filepath.Join(s.bootDir, "config.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "# pristine")

	// The tryboot.txt is slot-b/config.txt with os_prefix= set.
	data, err = os.ReadFile(filepath.Join(s.bootDir, "tryboot.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, ""+
		"kernel=vmlinuz\n"+
		"cmdline=cmdline.txt\n"+
		"initramfs initrd.img followkernel\n"+
		"os_prefix=slot-b/\n")

	// The cmdline.txt file is not modified.
	data, err = os.ReadFile(filepath.Join(s.bootDir, "slot-b", "cmdline.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "... root=/dev/mmcblk0p4 rauc.slot=B")
}

func (s *bootTest) TestTrySwitchBtoA(c *C) {
	s.MockConfigTxt(c, "# pristine")
	s.MockBootFile(c, "slot-a/config.txt", ""+
		"kernel=vmlinuz\n"+
		"cmdline=cmdline.txt\n"+
		"initramfs initrd.img followkernel\n")
	s.MockBootFile(c, "slot-a/cmdline.txt", "... root=/dev/mmcblk0p3 rauc.slot=A")

	err := s.pi.TrySwitch(boot.SlotA)
	c.Assert(err, IsNil)

	// The config.txt file is not touched.
	data, err := os.ReadFile(filepath.Join(s.bootDir, "config.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "# pristine")

	// The tryboot.txt is slot-a/config.txt with os_prefix= set.
	data, err = os.ReadFile(filepath.Join(s.bootDir, "tryboot.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, ""+
		"kernel=vmlinuz\n"+
		"cmdline=cmdline.txt\n"+
		"initramfs initrd.img followkernel\n"+
		"os_prefix=slot-a/\n")

	// The cmdline.txt file is not modified.
	data, err = os.ReadFile(filepath.Join(s.bootDir, "slot-a", "cmdline.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "... root=/dev/mmcblk0p3 rauc.slot=A")
}

func (s *bootTest) TestReboot(c *C) {
	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	err := s.pi.Reboot(0)
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})

	err = s.pi.Reboot(boot.RebootTryBoot)
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot 0\ tryboot`})
}

func (s *bootTest) TestCommitSwitch(c *C) {
	s.MockConfigTxt(c, "os_prefix=slot-a/")
	s.MockTrybootTxt(c, "os_prefix=slot-b/")

	err := s.pi.CommitSwitch()
	c.Assert(err, IsNil)

	slot, err := s.pi.QueryActive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotB)

	_, err = os.Stat(filepath.Join(s.bootDir, "tryboot.txt"))
	c.Assert(os.IsNotExist(err), Equals, true)
}

func (s *bootTest) TestCancelSwitch(c *C) {
	s.MockConfigTxt(c, "os_prefix=slot-a/")
	s.MockTrybootTxt(c, "os_prefix=slot-b/")

	err := s.pi.CancelSwitch()
	c.Assert(err, IsNil)

	slot, err := s.pi.QueryActive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotA)

	_, err = os.Stat(filepath.Join(s.pi.BootMountPoint(), "tryboot.txt"))
	c.Assert(os.IsNotExist(err), Equals, true)

	// Idempotent
	err = s.pi.CancelSwitch()
	c.Assert(err, IsNil)
}

func (s *bootTest) TestPostInstall(c *C) {
	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	env, err := installhandler.NewEnvironment([]string{
		"RAUC_SLOTS=1",
		"RAUC_TARGET_SLOTS=1",
		"RAUC_SLOT_CLASS_1=potato",
	})
	c.Check(err, IsNil)
	err = s.pi.PostInstall(env)
	c.Check(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string(nil))

	env, err = installhandler.NewEnvironment([]string{
		"RAUC_SLOTS=1",
		"RAUC_TARGET_SLOTS=1",
		"RAUC_SLOT_CLASS_1=system",
	})
	c.Assert(err, IsNil)
	err = s.pi.PostInstall(env)
	c.Check(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot 0\ tryboot`})
}

type preInstallSuite struct {
	bootDir   string
	procDir   string
	bundleDir string
	systemDir string

	revCode   pimodel.RevisionCode
	serialNum pimodel.SerialNumber

	pi  *piboot.PiBoot
	env *installhandler.Environment

	// Boot system configuration and RAUC handler information.
	// Set those at a suite level to create test variations.
	configTxt          string
	expectedTargetSlot boot.Slot
}

var _ = Suite(&preInstallSuite{
	// Arbitrary choice for as long a valid revision is used.
	revCode:   Pi4BRevCode,
	serialNum: sampleSerial,
	// Pristine boot configuration. The system will update to slot B.
	configTxt:          "",
	expectedTargetSlot: boot.SlotB,
})

var _ = Suite(&preInstallSuite{
	revCode:   Pi4BRevCode,
	serialNum: sampleSerial,
	// State after first update, system will update to slot A.
	configTxt:          "os_prefix=slot-b/",
	expectedTargetSlot: boot.SlotA,
})

var _ = Suite(&preInstallSuite{
	// Arbitrary choice for as long a valid revision is used.
	revCode:   Pi4BRevCode,
	serialNum: sampleSerial,
	// State after second update, system will update to slot B, again.
	configTxt:          "os_prefix=slot-a/",
	expectedTargetSlot: boot.SlotB,
})

func (s *preInstallSuite) raucTargetSlotIndex() string {
	switch s.expectedTargetSlot {
	case boot.SlotA:
		// This seems backwards but matches the data passed to
		// NewEnvironment in SetUpTest below. The ordering may seem
		// wrong but mirrors RAUC behavior which is documented to
		// provide no relationship to slot boot name and slot index.
		return "2"
	case boot.SlotB:
		return "1"
	default:
		panic("raucTargetSlotIndex is only defined for slots A and B")
	}
}

func (s *preInstallSuite) raucTargetSlotDevice() string {
	switch s.expectedTargetSlot {
	case boot.SlotA:
		return "/dev/mmcblk0p3"
	case boot.SlotB:
		return "/dev/mmcblk0p4"
	default:
		panic("raucTargetSlotDevice is only defined for slots A and B")
	}
}

func (s *preInstallSuite) SetUpTest(c *C) {
	var err error

	_, err = exec.LookPath("mksquashfs")
	if err != nil {
		c.Skip("pre-install handler suite requires mksquashfs")
	}

	s.bootDir = c.MkDir()
	s.procDir = c.MkDir()
	s.bundleDir = c.MkDir()
	s.systemDir = c.MkDir()

	s.pi, err = piboot.New(s.bootDir, s.procDir, s.revCode, s.serialNum)
	c.Assert(err, IsNil)

	// Prepare minimum configuration of the current boot system.
	// This is needed to detect updates which would clobber the current system.
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"), []byte(s.configTxt), 0o600), IsNil)

	// Prepare a fake system image.
	c.Assert(os.MkdirAll(filepath.Join(s.systemDir, "usr/lib/sysota/boot-assets"), 0o700), IsNil)
	c.Assert(os.WriteFile(filepath.Join(s.systemDir, "usr/lib/sysota/boot-assets/vmlinuz-5.10"), []byte("kernel"), 0o600), IsNil)
	c.Assert(os.WriteFile(filepath.Join(s.systemDir, "usr/lib/sysota/boot-assets/initrd-5.10.img"), []byte("initrd"), 0o600), IsNil)
	c.Assert(os.WriteFile(filepath.Join(s.systemDir, "usr/lib/sysota/boot-assets/config.txt"), []byte(""+
		"kernel=vmlinuz-5.10\n"+
		"initramfs initrd-5.10.img followkernel\n"), 0o600), IsNil)
	c.Assert(os.WriteFile(filepath.Join(s.systemDir, "usr/lib/sysota/boot-assets/cmdline.txt"), []byte("root=hardcoded"), 0o600), IsNil)
	c.Assert(mksquashfs.Run(filepath.Join(s.bundleDir, "system.img"), nil, s.systemDir), IsNil)

	// Prepare the handler context
	s.env, err = installhandler.NewEnvironment([]string{
		fmt.Sprintf("RAUC_BUNDLE_MOUNT_POINT=%s", s.bundleDir),
		"RAUC_IMAGE_CLASS_1=system",
		"RAUC_IMAGE_NAME_1=system.img",
		"RAUC_SLOT_BOOTNAME_1=B",
		"RAUC_SLOT_BOOTNAME_2=A",
		"RAUC_SLOT_CLASS_1=system",
		"RAUC_SLOT_CLASS_2=system",
		"RAUC_SLOT_DEVICE_1=/dev/mmcblk0p4",
		"RAUC_SLOT_DEVICE_2=/dev/mmcblk0p3",
		"RAUC_SLOTS=1 2 ",
		fmt.Sprintf("RAUC_TARGET_SLOTS=%s ", s.raucTargetSlotIndex()),
	})
	c.Assert(err, IsNil)
}

func (s *preInstallSuite) TestPreInstall(c *C) {
	// Run the pre-install handler.
	err := s.pi.PreInstall(s.env)
	c.Assert(err, IsNil)

	slotDir := piboot.SlotPrefix(s.expectedTargetSlot)

	data, err := os.ReadFile(filepath.Join(s.bootDir, slotDir, "config.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, ""+
		"kernel=vmlinuz-5.10\n"+
		"initramfs initrd-5.10.img followkernel\n")

	data, err = os.ReadFile(filepath.Join(s.bootDir, slotDir, "cmdline.txt"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, fmt.Sprintf("root=%s rauc.slot=%s",
		s.raucTargetSlotDevice(), s.expectedTargetSlot))

	data, err = os.ReadFile(filepath.Join(s.bootDir, slotDir, "vmlinuz-5.10"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "kernel")

	data, err = os.ReadFile(filepath.Join(s.bootDir, slotDir, "initrd-5.10.img"))
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "initrd")
}

func (s *preInstallSuite) TestPreInstallSanityCheck(c *C) {
	c.Assert(os.WriteFile(filepath.Join(s.bootDir, "config.txt"),
		[]byte(fmt.Sprintf("os_prefix=%s", piboot.SlotPrefix(s.expectedTargetSlot))), 0o600), IsNil)

	err := s.pi.PreInstall(s.env)
	c.Assert(err, ErrorMatches, `pre-install hook would clobber active slot`)
}

func (s *preInstallSuite) TestPreInstallOldEEPROM(c *C) {
	relTsPath, err := filepath.Rel(dirs.Proc, pieeprom.ProcfsBootloaderBuildTimestamp)
	c.Assert(err, IsNil)

	tsPath := filepath.Join(s.procDir, relTsPath)

	err = os.MkdirAll(filepath.Dir(tsPath), 0700)
	c.Assert(err, IsNil)

	// 2020-09-03 12:11:43
	err = os.WriteFile(tsPath, []byte{0x5f, 0x50, 0xdd, 0x7f}, 0o600)
	c.Assert(err, IsNil)

	defer func() {
		_ = os.Remove(tsPath)
	}()

	err = s.pi.PreInstall(s.env)
	c.Assert(err, NotNil)
	c.Check(errors.Is(err, piboot.ErrTryBootUnsupported), Equals, true)
}
