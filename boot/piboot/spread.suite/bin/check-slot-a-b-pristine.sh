#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

set -e
set -x

# Check that the slot-specific boot directories originally copied by the
# pre-install handler are pristine. This data is consistent with what is
# prepared by spread.yaml for the test suite boot/piboot/spread.suite.

test -e "$SYSOTA_BOOT_DIR"/slot-a/config.txt
test -e "$SYSOTA_BOOT_DIR"/slot-a/cmdline.txt

test -e "$SYSOTA_BOOT_DIR"/slot-b/config.txt
test -e "$SYSOTA_BOOT_DIR"/slot-b/cmdline.txt

# OS prefix is not set in either config file.
NOMATCH 'os_prefix=' "$SYSOTA_BOOT_DIR"/slot-a/config.txt
NOMATCH 'os_prefix=' "$SYSOTA_BOOT_DIR"/slot-b/config.txt

# cmdline= is set to the data provided by RAUC.
MATCH 'root=/var/tmp/rauc-fake-system/slot-a' "$SYSOTA_BOOT_DIR"/slot-a/cmdline.txt
MATCH 'root=/var/tmp/rauc-fake-system/slot-b' "$SYSOTA_BOOT_DIR"/slot-b/cmdline.txt
