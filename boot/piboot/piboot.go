// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package piboot implements the boot protocol for the Raspberry Pi.
//
// This implementation assumes the following: The raspberry pi boot firmware is
// at least the release from 2020-10-28. All boot assets selected by the
// os_prefix= mechanism are stored in a subdirectory named either slot-a/ or
// slot-b/, one for each slot.
//
// Each kernel command line file points root= argument to block device matching
// each slot. The kernel image supports the RPI_FIRMWARE_SET_REBOOT_FLAGS
// feature, which is used to perform "tryboot" reboots before committing the new
// configuration.
//
// References:
//   - https://github.com/raspberrypi/linux/commit/757666748ebf69dc161a262faa3717a14d68e5aa
//   - https://github.com/raspberrypi/rpi-eeprom/blob/master/firmware/release-notes.md
//   - https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/README.md
package piboot

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/zygoon/go-raspi/pieeprom"
	"gitlab.com/zygoon/go-raspi/pimodel"
	"gitlab.com/zygoon/go-squashfstools/unsquashfs"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/dirs"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
)

const (
	// configTxtName is the name of the firmware configuration file read during
	// the boot process.
	configTxtName = "config.txt"
	// trybootTxtName is the name of the firmware configuration file read during
	// the try-once boot process. It is further described at
	// https://github.com/raspberrypi/linux/commit/757666748ebf69dc161a262faa3717a14d68e5aa
	trybootTxtName = "tryboot.txt"
	// argRaucSlot is a RAUC-specific kernel command line argument encoding current slot's name.
	argRaucSlot = "rauc.slot="
	// argRoot is a kernel command line argument designating the device with the
	// root file system.
	argRoot = "root="
)

func slotPrefix(slot boot.Slot) string {
	return fmt.Sprintf("slot-%s/", strings.ToLower(slot.String()))
}

// PiBoot implements boot.Protocol for the Raspberry Pi bootloader.
type PiBoot struct {
	boot.DelayedReboot

	bootMountPoint string
	procMountPoint string
	revCode        pimodel.RevisionCode
	serialNum      pimodel.SerialNumber
}

// New returns a new PiBoot with the given boot mount point and CPU properties.
func New(bootMountPoint, procMountPoint string, revCode pimodel.RevisionCode, serialNum pimodel.SerialNumber) (*PiBoot, error) {
	// TODO(zyga): drop the error return path if it is not useful later on.
	return &PiBoot{
		bootMountPoint: bootMountPoint,
		procMountPoint: procMountPoint,
		revCode:        revCode,
		serialNum:      serialNum,
	}, nil
}

// BootMountPoint returns the corresponding value passed to the New.
func (pi *PiBoot) BootMountPoint() string {
	return pi.bootMountPoint
}

// RevisionCode returns the corresponding value passed to the New.
func (pi *PiBoot) RevisionCode() pimodel.RevisionCode {
	return pi.revCode
}

// SerialNumber returns the corresponding value passed to the New.
func (pi *PiBoot) SerialNumber() pimodel.SerialNumber {
	return pi.serialNum
}

// activeBootConfig loads the boot config corresponding to the active slot.
func (pi *PiBoot) activeBootConfig() (*BootConfig, error) {
	return NewBootConfig(pi.bootMountPoint, pi.revCode, pi.serialNum)
}

var (
	// errOsPrefixUnset records boot configuration not using the os_prefix= variable.
	errOsPrefixUnset = errors.New("os_prefix= is not set")

	// errOsPrefixEvadesSlot records boot configuration where os_prefix= evades the slot mechanism.
	errOsPrefixEvadesSlot = errors.New("os_prefix= evades slot directory")
)

// osPrefixError records problems with osPrefix
type osPrefixError struct {
	Err error
}

// Error implements the error interface.
func (e *osPrefixError) Error() string {
	return fmt.Sprintf("compatibility error: %s", e.Err)
}

// Is implements the errors.Is interface extension.
//
// CompatibilityError wrapping ErrOsPrefixUnset reports as being
// boot.ErrPristineBootConfig to avoid polluting RAUC adapter package
// with piboot-specific imports
func (e *osPrefixError) Is(target error) bool {
	return target == boot.ErrPristineBootConfig && e.Err == errOsPrefixUnset
}

// Unwrap returns the error wrapped inside the incompatible configuration error.
func (e *osPrefixError) Unwrap() error {
	return e.Err
}

// QueryActive returns the slot that is used for booting.
//
// Active slot is determined by the value of os_prefix= parameter.
// If there is an error, it will be of type *UnknownActiveSlotError.
func (pi *PiBoot) QueryActive() (slot boot.Slot, err error) {
	bootCfg, err := pi.activeBootConfig()
	if err != nil {
		return boot.InvalidSlot, &boot.UnknownActiveSlotError{Err: err}
	}

	osPrefix, _, err := bootCfg.ComputedOSPrefix()
	if err != nil {
		return boot.InvalidSlot, &boot.UnknownActiveSlotError{Err: err}
	}

	switch osPrefix {
	case slotPrefix(boot.SlotA):
		return boot.SlotA, nil
	case slotPrefix(boot.SlotB):
		return boot.SlotB, nil
	case "":
		return boot.InvalidSlot, &boot.UnknownActiveSlotError{Err: &osPrefixError{Err: errOsPrefixUnset}}
	default:
		return boot.InvalidSlot, &boot.UnknownActiveSlotError{Err: &osPrefixError{Err: errOsPrefixEvadesSlot}}
	}
}

// QueryInactive returns the slot that is not used for booting.
//
// If there is an error, it will be of type *UnknownInactiveSlotError.
func (pi *PiBoot) QueryInactive() (boot.Slot, error) {
	// Find the active slot and return the other slot.
	slot, err := pi.QueryActive()
	if err != nil {
		// Unwrap UnknownActiveSlotError to avoid confusing error messages.
		var realErr = err

		var e *boot.UnknownActiveSlotError

		if errors.As(err, &e) {
			realErr = e.Unwrap()
		}

		return boot.InvalidSlot, &boot.UnknownInactiveSlotError{Err: realErr}
	}

	return boot.SynthesizeInactiveSlot(slot), nil
}

// TrySwitch configures the bootloader for a one-off boot using the given slot.
func (pi *PiBoot) TrySwitch(targetSlot boot.Slot) (err error) {
	slottedBootDir := slotPrefix(targetSlot)

	// Load boot configuration pretending that slot subdirectory of the boot
	// partition is the real boot partition. This is the state we get
	// from the pre-install handler.
	slotBootCfg, err := NewBootConfig(
		filepath.Join(pi.bootMountPoint, slottedBootDir), pi.revCode, pi.serialNum)
	if err != nil {
		return err
	}

	// Set the os_prefix= to slot-$inactive/ and add a rauc.slot={A,B} flag to
	// kernel command line (informative only).
	if err := slotBootCfg.SetOSPrefix(slottedBootDir); err != nil {
		return err
	}

	// Save the configuration as "tryboot.txt" in the real boot mount point.
	slotBootCfg.BootMountPoint = pi.bootMountPoint
	slotBootCfg.ConfigFileName = trybootTxtName

	return slotBootCfg.Save()
}

// CommitSwitch re-configures a try-mode slot for continuous use.
func (pi *PiBoot) CommitSwitch() error {
	return os.Rename(filepath.Join(pi.bootMountPoint, trybootTxtName), filepath.Join(pi.bootMountPoint, configTxtName))
}

// Reboot gracefully reboots the system, passing any platform-specific information to the bootloader if necessary.
func (pi *PiBoot) Reboot(flags boot.RebootFlags) error {
	if flags&boot.RebootTryBoot != 0 {
		return pi.DelayedReboot.RebootSystem("0 tryboot")
	}

	return pi.DelayedReboot.RebootSystem()
}

// CancelSwitch removes the try-mode boot configuration and cmdline file.
func (pi *PiBoot) CancelSwitch() error {
	if err := os.Remove(filepath.Join(pi.bootMountPoint, trybootTxtName)); err != nil && !os.IsNotExist(err) {
		return err
	}

	// TODO(zyga): perhaps remove the slot subdirectory as well?
	return nil
}

// systemClass is the class of the RAUC image with a SystemOTA root file system + boot partition + kernel image.
const systemClass = "system"

// ErrWouldClobber reports cases where the pre-install hook would clobber active slot.
var ErrWouldClobber = errors.New("pre-install hook would clobber active slot")

// ErrTryBootUnsupported reports that the boot firmware is too old to support the tryboot.txt mechanism.
var ErrTryBootUnsupported = errors.New("boot firmware does not support tryboot.txt")

// PreInstall copies boot assets out of system.img and into the boot partition.
func (pi *PiBoot) PreInstall(env *installhandler.Environment) (err error) {
	// If the handler is invoked but doesn't find a bundle and a target slot
	// with "system" class then do nothing without reporting an error. This will
	// allow systems to use other slots and classes without SystemOTA
	// interfering with that.
	systemImgInfo := env.ImageWithClass(systemClass)
	targetSlotInfo := env.TargetSlotWithClass(systemClass)

	if systemImgInfo == nil || targetSlotInfo == nil {
		return nil
	}

	// Which slot are we targeting?
	var targetSlot boot.Slot
	if err := targetSlot.UnmarshalText([]byte(targetSlotInfo.BootName)); err != nil {
		return err
	}

	// Sanity check, ensure that we never clobber the active slot. This should
	// never happen, but it's just easy to check so let's check and have fewer
	// things to worry about.
	activeSlot, err := pi.QueryActive()
	if err != nil {
		if !errors.Is(err, boot.ErrPristineBootConfig) {
			return err
		}

		activeSlot = boot.SlotA
	}

	if activeSlot == targetSlot {
		return ErrWouldClobber
	}

	// Sanity check: the boot firmware supports "tryboot" functionality. The
	// return may be ENOENT if this is invoked on a device without bootloader
	// in EEPROM. The extra indirection allows tests to define a different
	// /proc directory without having to mock variables.
	relTsPath, err := filepath.Rel(dirs.Proc, pieeprom.ProcfsBootloaderBuildTimestamp)
	if err != nil {
		return err
	}

	tsPath := filepath.Join(pi.procMountPoint, relTsPath)

	firmwareBuildTs, err := pieeprom.BuildTimestamp(tsPath)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return err
	}

	if !firmwareBuildTs.IsZero() && firmwareBuildTs.Before(pieeprom.TryBootFeatureSince) {
		return ErrTryBootUnsupported
	}

	slottedBootDir := slotPrefix(targetSlot)

	// Remove $boot/squashfs-root/ and $boot/slot-$inactive to make space for
	// the unpack/move we are about to do below.
	if err := os.RemoveAll(filepath.Join(pi.bootMountPoint, slottedBootDir)); err != nil {
		return err
	}

	const squashfsRootDir = "squashfs-root"
	if err := os.RemoveAll(filepath.Join(pi.bootMountPoint, squashfsRootDir)); err != nil {
		return err
	}

	// Extract the $(system.img)/usr/lib/sysota/boot-assets directory from the
	// system.img squashfs into $boot/squashfs-root/.
	const bootAssetsDir = "usr/lib/sysota/boot-assets"

	unsquashfsFrom := filepath.Join(env.BundleMountPoint, systemImgInfo.Name)
	unsquashfsTo := filepath.Join(pi.bootMountPoint, squashfsRootDir)
	unsquashfsWhat := bootAssetsDir

	if env.InitialHandlerPID != 0 {
		// Note that the rauc-pre-pre-handler is invoked by RAUC inside a separate
		// mount namespace where the bundle file is mounted. Look at the mounted
		// file through the magic symlink in /proc/[pid]/root.
		unsquashfsFrom = filepath.Join(dirs.Proc, strconv.Itoa(env.InitialHandlerPID), "root", unsquashfsFrom)
	}

	if err := unsquashfs.Run(unsquashfsFrom, unsquashfsTo, &unsquashfs.Options{Force: true}, unsquashfsWhat); err != nil {
		return err
	}

	// Rename the extracted boot directory to $boot/slot-$inactive.
	renameFrom := filepath.Join(pi.bootMountPoint, squashfsRootDir, unsquashfsWhat)
	renameTo := filepath.Join(pi.bootMountPoint, slottedBootDir)

	if err := os.Rename(renameFrom, renameTo); err != nil {
		return err
	}

	// Remove the now-empty squashfs directory.
	if err := os.RemoveAll(filepath.Join(pi.bootMountPoint, squashfsRootDir)); err != nil {
		return err
	}

	// Load the boot configuration from the slot directory and modify root=.
	bootCfgDir := filepath.Join(pi.bootMountPoint, slotPrefix(targetSlot))

	slotBootCfg, err := NewBootConfig(bootCfgDir, pi.revCode, pi.serialNum)
	if err != nil {
		return err
	}

	// Set the root= argument to the block device matching the slot.
	slotBootCfg.CmdLine.SetArg(argRoot, targetSlotInfo.DevicePath)
	// Set the rauc.slot={A,B} kernel command line (informative only).
	slotBootCfg.CmdLine.SetArg(argRaucSlot, targetSlot.String())

	// TODO(zyga): validate the final configuration.

	// Save the configuration back to the per-slot directory.
	return slotBootCfg.Save()
}

// PostInstall reboots the Raspberry Pi if necessary.
//
// Reboots are required after writing to RAUC slot of type "system".
// Reboot is graceful, shutting down services normally.
func (pi *PiBoot) PostInstall(env *installhandler.Environment) error {
	if !shouldReboot(env) {
		return nil
	}

	return pi.Reboot(boot.RebootTryBoot)
}

// shouldReboot returns true if a slot with "system" class is being modified.
func shouldReboot(env *installhandler.Environment) bool {
	for _, slotID := range env.TargetSlotIDs {
		if env.Slots[slotID].Class == systemClass {
			return true
		}
	}

	return false
}
