// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package piboot

import (
	"fmt"
	"path/filepath"
	"strings"

	"gitlab.com/zygoon/go-raspi/picfg"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

// defaultKernel returns the default value of the "kernel=" parameter.
//
// The default value depends on the model and on the value of the arm_64bit=
// parameter. The return value is one of "kernel.img" for ARMv6 devices,
// "kernel7.img" for ARMv7 devices and either "kernel7l.img" or "kernel8.img"
// when arm_64bit=1 is used.
func defaultKernel(code pimodel.RevisionCode, arm64Bit bool) (string, error) {
	soc, err := code.Processor()
	if err != nil {
		return "", err
	}

	if arm64Bit {
		return "kernel8.img", nil
	}

	switch soc.BcmSiliconFamilyCode() {
	case 2708:
		return "kernel.img", nil
	case 2709, 2710:
		return "kernel7.img", nil
	case 2711:
		return "kernel7l.img", nil // "l" stands for LPAE.
	default:
		return "", fmt.Errorf("unhandled default kernel for %s", code)
	}
}

// defaultDeviceTree returns the list of device tree files for a given revision code.
//
// The returned list expresses preference implemented in the firmware.
func defaultDeviceTree(code pimodel.RevisionCode, upstreamKernel bool) (files []string, err error) {
	// We need to know the board type.
	model, err := code.BoardType()
	if err != nil {
		return nil, err
	}

	// We need to know the SoC family code.
	soc, err := code.Processor()
	if err != nil {
		return nil, err
	}

	// The models variable holds the name of the board for the purpose of the device tree file.
	models := make([]string, 0, 2)

	switch model {
	case pimodel.Pi1A:
		models = append(models, "a")
		fallthrough // In general all the "A" models use device tree from the matching "B" model.
	case pimodel.Pi1B:
		// FIXME(zyga): it's unclear what selects b-rev1 that exists in the wild.
		models = append(models, "b")
	case pimodel.Pi1APlus:
		models = append(models, "a-plus")
		fallthrough
	case pimodel.Pi1BPlus:
		models = append(models, "b-plus")
	case pimodel.Cm1:
		models = append(models, "cm")
	case pimodel.Pi0:
		models = append(models, "zero")
	case pimodel.Pi0W:
		// Pi0W will look for Pi1B DBT as well.
		models = append(models, "zero-w")
	case pimodel.Pi2B:
		models = append(models, "2-b")
	case pimodel.Pi3B:
		models = append(models, "3-b")
	case pimodel.Pi3APlus:
		models = append(models, "3-a-plus")
		fallthrough
	case pimodel.Pi3BPlus:
		models = append(models, "3-b-plus")
	case pimodel.Cm3, pimodel.Cm3Plus:
		models = append(models, "cm3")
	case pimodel.Pi400:
		models = append(models, "400")
	case pimodel.Pi4B:
		models = append(models, "4-b")
	case pimodel.Cm4:
		models = append(models, "cm4")
	default:
		return nil, fmt.Errorf("unknown device tree for revision code %s", code)
	}

	if upstreamKernel {
		files = make([]string, 0, len(models)*2)
	} else {
		files = make([]string, 0, len(models))
	}

	const dtbFmt = "bcm%d-rpi-%s.dtb"

	for _, model := range models {
		if upstreamKernel && soc.BcmPackageCode() != soc.BcmSiliconFamilyCode() {
			// Upstream kernel device trees are named after the package, not the
			// silicon family. Since BCM2711 does not have a special package
			// name, refrain from emitting duplicate entries.
			// https://github.com/raspberrypi/linux/issues/3237#issue-494626152
			files = append(files, fmt.Sprintf(dtbFmt, soc.BcmPackageCode(), model))
		}

		// The downstream variant is always looked up as fallback.
		files = append(files, fmt.Sprintf(dtbFmt, soc.BcmSiliconFamilyCode(), model))
	}

	return files, nil
}

// joinOsPrefix joins OS prefix and a path.
//
// The prefix and path are concatenated unless the path is absolute.
// https://github.com/raspberrypi/linux/issues/3237#issuecomment-547475392
func joinOsPrefix(osPrefix, path string) string {
	if filepath.IsAbs(path) {
		// Absolute paths ignore osPrefix.
		// All paths become relative to the boot partition.
		return strings.TrimLeft(path, "/")
	}

	return strings.TrimLeft(osPrefix+path, "/")
}

// pickParam returns the non-nil parameter with the largest sequence number.
//
// Any or all of the parameters may be nil.
func pickParam(params ...*picfg.Parameter) (last *picfg.Parameter) {
	for _, param := range params {
		if param == nil {
			continue
		}

		if last == nil {
			last = param
		}

		if param.Location.SeqNum > last.Location.SeqNum {
			last = param
		}
	}

	return last
}
