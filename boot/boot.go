// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package boot implements the interface between the OTA system and the boot loader.
package boot

import (
	"errors"
	"fmt"
	"time"
)

// Protocol encapsulates the interaction between the System OTA service and the boot loader.
type Protocol interface {
	// QueryActive returns the slot that is used for booting
	QueryActive() (Slot, error)
	// QueryInactive returns the slot that is not used for booting
	QueryInactive() (Slot, error)

	// TrySwitch configures the boot loader for a one-off boot using the given
	// slot On the next boot invoked with RebootTryMode, the system will boot
	// into the try slot.
	TrySwitch(Slot) error
	// Reboot gracefully reboots the system
	Reboot(RebootFlags) error
	// CommitSwitch commits a try-boot slot for continuous use
	CommitSwitch() error
	// CancelSwitch reverts any changes made by TrySwitch. While any such
	// changes are not effective unless CommitSwitch is called, calling this
	// function in the upgrade-rollback path may remove unnecessary files,
	// making the system easier to analyze.
	CancelSwitch() error
}

// RebootDelayer allows boot protocol to indicate support for postponing the reboot process.
type RebootDelayer interface {
	SetRebootDelay(time.Duration)
	RebootPending() bool
}

// invalidString is the used by String to represent the invalid state of enumerations.
const invalidString = "invalid"

// ErrInvalidSlot records slot name other than A or B.
var ErrInvalidSlot = errors.New("invalid boot slot")

// Slot describes a bootable system partition in an A/B scheme.
type Slot uint

const (
	slotNameA = "A"
	slotNameB = "B"
)

const (
	// InvalidSlot represents an invalid slot, it is also the zero value.
	InvalidSlot Slot = iota
	// SlotA represents the "A" system image slot.
	SlotA
	// SlotB represents the "B" system image slot.
	SlotB
)

// String implements Stringer.
func (slot Slot) String() string {
	switch slot {
	case InvalidSlot:
		return invalidString
	case SlotA:
		return slotNameA
	case SlotB:
		return slotNameB
	}

	panic(ErrInvalidSlot)
}

// MarshalText implements text marshaler.
func (slot Slot) MarshalText() ([]byte, error) {
	switch slot {
	case SlotA:
		return []byte(slotNameA), nil
	case SlotB:
		return []byte(slotNameB), nil
	default:
		return nil, ErrInvalidSlot
	}
}

// UnmarshalText unmarshals slot name from given text.
func (slot *Slot) UnmarshalText(text []byte) error {
	switch string(text) {
	case slotNameA:
		*slot = SlotA
		return nil
	case slotNameB:
		*slot = SlotB
		return nil
	default:
		return ErrInvalidSlot
	}
}

// RebootFlags influence the reboot process.
type RebootFlags uint

const (
	// RebootTryBoot indicates that the system should use one-time boot configuration.
	RebootTryBoot RebootFlags = 1 << iota
)

// SlotState conveys information about the boot state of a RAUC slot.
//
// A boot slot is either in good or bad state. Slots in bad state cannot be
// booted. Slots in good state can. The currently booted slot may be in an
// indeterminate state until it is assessed.
//
// As a SystemOTA specific behavior, the active slot is always good and the
// inactive slot is always bad, except when the try-mode configuration indicates
// it should be booted into.
type SlotState int

const (
	// InvalidSlotState indicates indeterminate boot state of a slot, neither good nor bad.
	// InvalidSlotState cannot be marshaled.
	InvalidSlotState SlotState = iota
	// BadSlot indicates that a slot cannot be used for booting.
	BadSlot
	// GoodSlot indicates that a slot can be used for booting.
	GoodSlot
)

const (
	stateNameBad  = "bad"
	stateNameGood = "good"
)

// ErrInvalidSlotState records slot state other than good or bad.
var ErrInvalidSlotState = errors.New("invalid slot state")

// String returns the RAUC-compatible representation of the boot state.
//
// The return value is always either "good" or "bad".
func (state SlotState) String() string {
	switch state {
	case InvalidSlotState:
		return invalidString
	case BadSlot:
		return stateNameBad
	case GoodSlot:
		return stateNameGood
	default:
		panic(ErrInvalidSlotState)
	}
}

// MarshalText marshal boot state into a slice of bytes.
func (state SlotState) MarshalText() ([]byte, error) {
	switch state {
	case BadSlot:
		return []byte(stateNameBad), nil
	case GoodSlot:
		return []byte(stateNameGood), nil
	default:
		return nil, ErrInvalidSlotState
	}
}

// UnmarshalText unmarshals boot state from a slice of bytes.
func (state *SlotState) UnmarshalText(text []byte) error {
	switch string(text) {
	case stateNameBad:
		*state = BadSlot
	case stateNameGood:
		*state = GoodSlot
	default:
		return ErrInvalidSlotState
	}

	return nil
}

const (
	modeNameNormal = "normal"
	modeNameTry    = "try"
)

// ErrInvalidBootMode records boot mode other than normal or try.
var ErrInvalidBootMode = errors.New("invalid boot mode")

// Mode designates the mode in which System OTA expects the system to boot.
type Mode int

const (
	// Normal indicates that the system boots using the active slot.
	Normal Mode = iota
	// Try indicates that the system boots using the inactive slot,
	// in one-off try mode.
	Try
	// InvalidBootMode indicates that the boot mode is neither good nor bad.
	InvalidBootMode
)

// String returns the boot mode as text.
func (m Mode) String() string {
	switch m {
	case InvalidBootMode:
		return invalidString
	case Normal:
		return modeNameNormal
	case Try:
		return modeNameTry
	default:
		panic(ErrInvalidBootMode)
	}
}

// MarshalText implements TextMarshaler.
func (m Mode) MarshalText() ([]byte, error) {
	switch m {
	case Normal:
		return []byte(modeNameNormal), nil
	case Try:
		return []byte(modeNameTry), nil
	default:
		return nil, ErrInvalidBootMode
	}
}

// UnmarshalText implements TextUnmarshaler.
func (m *Mode) UnmarshalText(text []byte) error {
	switch string(text) {
	case modeNameNormal:
		*m = Normal
	case modeNameTry:
		*m = Try
	default:
		return ErrInvalidBootMode
	}

	return nil
}

// SynthesizeInactiveSlot returns the inactive counterpart of the given active slot.
func SynthesizeInactiveSlot(active Slot) Slot {
	switch active {
	case SlotA:
		return SlotB
	case SlotB:
		return SlotA
	default:
		return InvalidSlot
	}
}

// ErrPristineBootConfig records discovery of a pristine boot configuration
// which is not yet adapted to the slot system. This may be the state coming
// fresh out of the image build system.
var ErrPristineBootConfig = errors.New("pristine boot configuration error")

// UnknownActiveSlotError records problems with determining active slot.
type UnknownActiveSlotError struct {
	Err error
}

// Unwrap returns the error wrapped inside the unknown active slot error.
func (e *UnknownActiveSlotError) Unwrap() error {
	return e.Err
}

// Error implements the error interface.
func (e *UnknownActiveSlotError) Error() string {
	return fmt.Sprintf("cannot determine active slot: %v", e.Err)
}

// UnknownInactiveSlotError records problems with determining inactive slot.
type UnknownInactiveSlotError struct {
	Err error
}

// Unwrap returns the error wrapped inside the unknown inactive slot error.
func (e *UnknownInactiveSlotError) Unwrap() error {
	return e.Err
}

// Error implements the error interface.
func (e *UnknownInactiveSlotError) Error() string {
	return fmt.Sprintf("cannot determine inactive slot: %v", e.Err)
}
