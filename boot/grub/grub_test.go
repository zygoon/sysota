// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package grub_test

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	gogrub "gitlab.com/zygoon/go-grub"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/boot/grub"
	"gitlab.com/zygoon/sysota/pkg/testutil"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
)

func Test(t *testing.T) { TestingT(t) }

type grubSuite struct {
	testutil.CommandSuite
}

var _ = Suite(&grubSuite{})

func (*grubSuite) TestSupportsDelayedReboot(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "grubenv")
	b := grub.New(name)

	var delayer boot.RebootDelayer = b

	c.Check(delayer, NotNil)
}

// TestMissingGrubenv shows what happens when grubenv is entirely missing.
func (*grubSuite) TestMissingGrubenv(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "grubenv")
	b := grub.New(name)

	_, err := b.QueryActive()
	c.Assert(errors.Is(err, os.ErrNotExist), Equals, true, Commentf("err: %v", err))

	_, err = b.QueryInactive()
	c.Assert(errors.Is(err, os.ErrNotExist), Equals, true, Commentf("err: %v", err))

	err = b.TrySwitch(boot.SlotB)
	c.Assert(errors.Is(err, os.ErrNotExist), Equals, true, Commentf("err: %v", err))

	err = b.CommitSwitch()
	c.Assert(errors.Is(err, os.ErrNotExist), Equals, true, Commentf("err: %v", err))

	err = b.CancelSwitch()
	c.Assert(errors.Is(err, os.ErrNotExist), Equals, true, Commentf("err: %v", err))
}

// TestGrubEnvWithoutSysotaVariables shows what happens when grubenv does not provide SysOTA variables.
func (*grubSuite) TestGrubEnvWithoutSysotaVariables(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "grubenv")

	c.Assert(gogrub.NewEnv().Save(name), IsNil)

	b := grub.New(name)

	_, err := b.QueryActive()
	c.Assert(err, ErrorMatches, "cannot determine active slot: SYSOTA_BOOT_ACTIVE is not set")

	_, err = b.QueryInactive()
	c.Assert(err, ErrorMatches, "cannot determine inactive slot: SYSOTA_BOOT_ACTIVE is not set")

	// TrySwitch doesn't care about existing variables. This may be a little bit weird.
	err = b.TrySwitch(boot.SlotB)
	c.Assert(err, IsNil)

	err = b.CommitSwitch()
	c.Assert(err, ErrorMatches, "cannot determine inactive slot: SYSOTA_BOOT_TRIED is not set")

	// Cancel does not care about any existing variables.
	err = b.CancelSwitch()
	c.Assert(err, IsNil)
}

func (*grubSuite) TestSmoke(c *C) {
	d := c.MkDir()
	name := filepath.Join(d, "grubenv")

	// Pretend A is active.
	env := gogrub.NewEnv()
	c.Assert(env.Set("SYSOTA_BOOT_ACTIVE", "A"), IsNil)
	c.Assert(env.Save(name), IsNil)

	b := grub.New(name)

	// Check active and inactive slot.
	slot, err := b.QueryActive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotA)

	slot, err = b.QueryInactive()
	c.Assert(err, IsNil)
	c.Check(slot, Equals, boot.SlotB)

	// Try switching from to B
	c.Assert(b.TrySwitch(boot.SlotB), IsNil)

	env, err = gogrub.LoadEnv(name)
	c.Assert(err, IsNil)
	c.Check(env.Get("SYSOTA_BOOT_ACTIVE"), Equals, "A")
	c.Check(env.Get("SYSOTA_BOOT_TRY"), Equals, "B")

	// Pretend that B was boot tested.
	c.Check(env.Set("SYSOTA_BOOT_TRIED", "B"), IsNil)
	env.Delete("SYSOTA_BOOT_TRY")
	c.Assert(env.Save(name), IsNil)

	// Commit the switch.
	c.Assert(b.CommitSwitch(), IsNil)

	env, err = gogrub.LoadEnv(name)
	c.Assert(err, IsNil)
	c.Check(env.Get("SYSOTA_BOOT_ACTIVE"), Equals, "B")
	_, ok := env.Lookup("SYSOTA_BOOT_TRY")
	c.Check(ok, Equals, false)
	_, ok = env.Lookup("SYSOTA_BOOT_TRIED")
	c.Check(ok, Equals, false)

	// Try switching to A
	c.Assert(b.TrySwitch(boot.SlotB), IsNil)

	// Cancel the switch.
	c.Assert(b.CancelSwitch(), IsNil)

	env, err = gogrub.LoadEnv(name)
	c.Assert(err, IsNil)

	_, ok = env.Lookup("SYSOTA_BOOT_TRY")
	c.Check(ok, Equals, false)
	_, ok = env.Lookup("SYSOTA_BOOT_TRIED")
	c.Check(ok, Equals, false)
}

func (s *grubSuite) TestInstallHandlers(c *C) {
	// Prepare the handler context
	raucEnv, err := installhandler.NewEnvironment([]string{
		fmt.Sprintf("RAUC_BUNDLE_MOUNT_POINT=%s", c.MkDir()),
		"RAUC_IMAGE_CLASS_1=system",
		"RAUC_IMAGE_NAME_1=system.img",
		"RAUC_SLOT_BOOTNAME_1=B",
		"RAUC_SLOT_BOOTNAME_2=A",
		"RAUC_SLOT_CLASS_1=system",
		"RAUC_SLOT_CLASS_2=system",
		"RAUC_SLOT_DEVICE_1=/dev/sda3",
		"RAUC_SLOT_DEVICE_2=/dev/sda4",
		"RAUC_SLOTS=1 2 ",
		"RAUC_TARGET_SLOTS=1 ",
	})
	c.Assert(err, IsNil)

	b := grub.New("")

	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	// Run the pre-install handler.
	c.Assert(b.PreInstall(raucEnv), IsNil)

	// Run the post-install handler.
	c.Assert(b.PostInstall(raucEnv), IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})
}

func (s *grubSuite) TestReboot(c *C) {
	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	b := grub.New("")

	// Run the post-install handler.
	c.Assert(b.Reboot(0), IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})

	// TryBoot flag is ignored.
	c.Assert(b.Reboot(boot.RebootTryBoot), IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})
}
